import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get2gether/util/AppColors.dart';
import 'package:get2gether/util/AppDimensions.dart';
import 'package:get2gether/util/Strings.dart';
import 'package:webview_flutter/webview_flutter.dart';

class WebViewContainer extends StatefulWidget {
  final String url;
  WebViewContainer({this.url});

  @override
  createState() => _WebViewContainerState();
}
class _WebViewContainerState extends State<WebViewContainer> {
  final _key = UniqueKey();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: new AppBar(
          centerTitle: true, // this is all you need
          backgroundColor: Colors.white,
          title: Text('Content', textAlign: TextAlign.center, style: TextStyle(fontFamily: 'Avenir', color: AppColors.TEXT_BLACK, fontSize: AppDimensions.appBarTitleTextSize),),
          leading: new IconButton(
            icon: new Icon(Icons.arrow_back, color: AppColors.TEXT_BLACK,),
            onPressed: () => Navigator.pop(context, true),
          ),

        ),
        body: Column(
          children: [
            Expanded(
                child: WebView(
                    key: _key,
                    javascriptMode: JavascriptMode.unrestricted,
                    initialUrl: widget.url))
          ],
        ));
  }
}