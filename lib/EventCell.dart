import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get2gether/UIComponents/AppButton.dart';
import 'package:get2gether/UIComponents/OrangeBorderTransparentButton.dart';
import 'package:get2gether/bloc/updateInviteStatus/updateInviteBloc.dart';
import 'package:get2gether/bloc/updateInviteStatus/updateinvite_event.dart';
import 'package:get2gether/bloc/updateInviteStatus/updateinvite_state.dart';
import 'package:get2gether/models/requests/UpdateInviteRequest.dart';
import 'package:get2gether/resources/AppData.dart';
import 'package:get2gether/util/AppColors.dart';
import 'package:get2gether/util/AppDimensions.dart';
import 'package:get2gether/util/Constants.dart';
import 'package:intl/intl.dart';
import 'package:timeago/timeago.dart' as timeago;

import 'models/Event.dart';

class EventsCell extends StatefulWidget {
  Event eventModel;
  Function deleteCallback;
  @override
  _EventsCellState createState() => _EventsCellState();

  EventsCell(this.eventModel, {this.deleteCallback});

}

class _EventsCellState extends State<EventsCell>{

  UpdateInviteBloc _updateInviteBloc;

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  Widget build(BuildContext context) {
    return BlocProvider<UpdateInviteBloc>(
        create: (_) {
          _updateInviteBloc = UpdateInviteBloc();
          return _updateInviteBloc;
        },
        child: BlocListener<UpdateInviteBloc, UpdateInviteBlocState>(
            listener: (context, state) {
              if (state is UpdateInviteSuccess) {
                widget.eventModel = state.response.event;
              } else if(state is RefreshEventCell){
                widget.eventModel = state.event;
              } else if(state is NewModelFetched) {
                widget.eventModel = state.eventDetailModel.event;
                AppData.updateUserEvent(widget.eventModel);
              }
            },
            child: BlocBuilder<UpdateInviteBloc, UpdateInviteBlocState>(
                bloc: _updateInviteBloc,
                builder: (context, state) {
                  return Stack(
                    children: <Widget>[
                      Card(
                          color: Colors.white,
                          margin: EdgeInsets.all(15.0),
                          child:  Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Padding(
                                  padding: EdgeInsets.all(15.0),
                                  child: Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: <Widget>[
                                      GestureDetector(
                                        child: Text(widget.eventModel.title, style: TextStyle(color: Colors.black, fontSize: 23.0, fontFamily: 'Avenir', )),
                                        onTap: () {
                                          Navigator.pushNamed(context, '/EventDetail', arguments: widget.eventModel);
                                        }
                                      ),
                                      SizedBox(height: 10,),
                                      Text(widget.eventModel.shortDescription, style: TextStyle(color: AppColors.TEXT_GREY_BLUISH, fontSize: 17.0)),
                                      SizedBox(height: 10,),
                                      Row(
                                        mainAxisAlignment: MainAxisAlignment.center,
                                        children: <Widget>[
                                          Image.asset('assets/images/clock.png', width: 18, height: 18,),
                                          SizedBox(width: 5,),
                                          widget.eventModel.dateTimeList.length == 1 ?
                                            Text("${getFormattedTime(widget.eventModel.dateTimeList[0].time)} in", style: TextStyle(color: AppColors.TEXT_GREY_BLUISH, fontSize: 16.0),) :
                                            Text("Multiple in", style: TextStyle(color: AppColors.TEXT_GREY_BLUISH, fontSize: 16.0),),
                                          SizedBox(width: 10,),
                                          Image.asset('assets/images/location.png', width: 18, height: 18,),
                                          SizedBox(width: 5,),
                                          Expanded(
                                            child: Text(widget.eventModel.address,
                                                style: TextStyle(color: AppColors.TEXT_GREY_BLUISH, fontSize: 16.0),
                                                overflow: TextOverflow.ellipsis),
                                          )
                                        ],
                                      ),
                                      SizedBox(height: 10,),
                                      Row(
                                        mainAxisAlignment: MainAxisAlignment.start,
                                        children: <Widget>[
                                          Image.asset('assets/images/calendar.png', width: 18, height: 18,),
                                          SizedBox(width: 5,),
                                          widget.eventModel.dateTimeList.length == 1 ?
                                            Text("${widget.eventModel.dateTimeList[0].date}", style: TextStyle(color: AppColors.TEXT_GREY_BLUISH, fontSize: 16.0),) :
                                            Text("Multiple", style: TextStyle(color: AppColors.TEXT_GREY_BLUISH, fontSize: 16.0),),
                                          SizedBox(width: 10,),
                                          Image.asset('assets/images/clock.png', width: 18, height: 18,),
                                          SizedBox(width: 5,),
                                          Text("created ${getTimeAgo()} ", style: TextStyle(color: AppColors.TEXT_GREY_BLUISH, fontSize: 16.0),),
                                        ],
                                      ),
                                      SizedBox(height: 15,),
                                      getInvitedView()
                                    ],
                                  ),
                                ),
                                _getFooterWidget(state)

                              ]
                          )
                      ),
                      widget.eventModel.adminId == AppData.currentUser.id?
                      Positioned(
                          right: 25.0,
                          top: 20.0,
                          child: GestureDetector(
                            child: Image.asset("assets/images/menu.png", height: 18.0, width: 18.0,),
                            onTap: (){
                              _showOptionsBottomSheet(context);
                            },
                          )

                      ) : Container()

                    ],
                  ) ;
                }
            )
        )
    );

  }
  _getFooterWidget(UpdateInviteBlocState state) {
    var result = isInviteAccepted();
    if(result == null)
      return widget.eventModel.adminId != AppData.currentUser.id ? widget.eventModel.paymentRequired == 1 ?_paymentWidget(state) : _nonPaymentWidget(state) : Container();
    else if(result) {
      return Padding(
          padding: EdgeInsets.all(15.0),
          child: Text("You have accepted this invite.", style: TextStyle(color: AppColors.ORANGE, fontSize: 17.0, fontFamily: 'Avenir'),)
      );
    } else {
      return Padding(
          padding: EdgeInsets.all(15.0),
          child: Text("You rejected this invite.", style: TextStyle(color: AppColors.ORANGE, fontSize: 17.0, fontFamily: 'Avenir'),)
      );
    }
  }
  void _showOptionsBottomSheet(context){
    showModalBottomSheet(
        context: context,
        builder: (BuildContext bc){
          return Container(
            child: new Wrap(
              children: <Widget>[
                new ListTile(
                  leading: Image.asset("assets/images/edit.png", height: 20.0, width: 20.0, fit: BoxFit.fill,),
                  title: new Text('Edit'),
                  onTap: () async {
                    Navigator.pop(context);
                    if(isAcceptedByAnyUser())
                      _showActionBlockedDialog("Edit");
                    else {
                      _updateInviteBloc.add(new RestEvent());
                      var eventModel = await Navigator.pushNamed(
                          context, '/CreateEvent',
                          arguments: widget.eventModel);
                      _refreshCellLocally(eventModel);
                    }

                  },
                ),
                new ListTile(
                    leading: Image.asset("assets/images/delete.png", height: 20.0, width: 20.0, fit: BoxFit.fill,),
                    title: new Text('Delete'),
                    onTap: () {
                      Navigator.pop(context);
                      if(isAcceptedByAnyUser())
                        _showActionBlockedDialog("Delete");
                      else
                        if(widget.deleteCallback != null) widget.deleteCallback();
                    }
                ),

              ],
            ),
          );
        }
    );
  }

  bool isAcceptedByAnyUser() {
    var inviations = widget.eventModel.invitations;
    for(int i = 0 ; i < inviations.length; i++) {
      var model = inviations[i];
      if(model.status == "accepted" || model.status == "paid")
        return true;

    }
    return false;
  }

  _showActionBlockedDialog(String action) {
    showDialog(
      context: context,
      builder: (context) => AlertDialog(
        content: ListTile(
          title: Text("Action Blocked"),
          subtitle: Text("You can't $action this event as one or more participants have accepted this event."),
        ),
        actions: <Widget>[
          FlatButton(
              child: Text('Ok'),
              onPressed: () {
                Navigator.of(context).pop();
              }
          ),
        ],
      ),
    );
  }
  _refreshCellLocally(Event eventModel) {
    AppData.updateUserEvent(eventModel);
    _updateInviteBloc.add(new UpdateEventCell(eventModel)); //refreshing cell using bloc event.
  }

  _refreshCell() {
    _updateInviteBloc.add(new EventDetailEvent(widget.eventModel.id.toString())); //refreshing cell using bloc event.
  }
  Widget _paymentWidget(UpdateInviteBlocState state) {
    return Column (
      children: <Widget>[
        Container(
          height: 0.3,
          color: AppColors.TEXT_GREY,
        ),
        Container(
            margin: EdgeInsets.all(15.0),
            decoration: BoxDecoration(
              color: AppColors.ORANGE,
              borderRadius: BorderRadius.all(Radius.circular(5)),
            ),
            width: MediaQuery.of(context).size.width,
            height: AppDimensions.buttonHeight(),
            child: GestureDetector(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text("PAY NOW WITH", style: TextStyle(color: Colors.white, fontSize: 17.0, fontFamily: 'Avenir'),),
                  SizedBox(width: 10,),
                  Image.asset('assets/images/paypal.png', width: 100, height: 30, fit: BoxFit.fill,),
                ],
              ),
              onTap: _acceptEventWithPayment,
            )
        ),
        Padding(
            padding: EdgeInsets.only(left: 15.0, right: 15.0, bottom: 15.0, ),
            child: OrangeBorderTransparentButton("Not Interested", _rejectEvent, colorText: AppColors.ORANGE, loading: state is UpdateInviteRejectInProgress)
        )

      ],
    );
  }

  Widget _nonPaymentWidget(UpdateInviteBlocState state) {
    return Column (
      children: <Widget>[
        Container(
          height: 0.3,
          color: AppColors.TEXT_GREY,
        ),
        Padding(
            padding: EdgeInsets.all(15.0),
            child: AppButton("Will Join, Accept Invite", () => _acceptEvent(widget.eventModel), loading: state is UpdateInviteAcceptInProgress,)
        ),
        Padding(
            padding: EdgeInsets.only(left: 15.0, right: 15.0, bottom: 15.0, ),
            child: OrangeBorderTransparentButton("Not Interested", _rejectEvent, colorText: AppColors.ORANGE, loading: state is UpdateInviteRejectInProgress)
        )

      ],
    );
  }

  _rejectEvent() {
    _updateInviteBloc.add(new RejectInvite(new UpdateInviteRequest(widget.eventModel.id, AppData.currentUser.id, "rejected", '', '')));
  }

  _acceptEvent(Event event) {
    if (event.dateTimeList.length == 1) {
      _updateInviteBloc.add(new AcceptInvite(new UpdateInviteRequest(
          widget.eventModel.id, AppData.currentUser.id, "accepted", event.dateTimeList[0].date, event.dateTimeList[0].time)));
    } else {
      Navigator.pushNamed(context, '/EventDates', arguments: event);
    }
  }

  _acceptEventWithPayment() {/*
      String url = WebConstants.BASE_URL_FOR_PAGES+"paywithpaypal/${widget.eventModel.id}/${AppData.currentUser.id}";
      var flutterWebView = new FlutterWebView();
      flutterWebView.launch(
          url,
          headers: {
          },
          javaScriptEnabled: true,
          toolbarActions: [
            new ToolbarAction("Dismiss", 1),
          ],
          barColor: AppColors.CYAN,
          tintColor: Colors.white);
      flutterWebView.onToolbarAction.listen((identifier) {
        switch (identifier) {
          case 1:
            flutterWebView.dismiss();
            break;
        }
      });

      flutterWebView.onWebViewDidStartLoading.listen((url) {
        _isTransactionSuccessful(url, flutterWebView);
      });
      flutterWebView.onWebViewDidLoad.listen((url) {
        _isTransactionSuccessful(url, flutterWebView);
      });
      flutterWebView.onRedirect.listen((url) {
        _isTransactionSuccessful(url, flutterWebView);
      });
  */}

  _isTransactionSuccessful(String url, var flutterWebView) {
    print(url);
    if(url.contains(WebConstants.BASE_URL_FOR_PAGES.toString()) && url.contains("invitation_id=") && url.contains("token=") && url.contains("paymentId=")) {
      flutterWebView.dismiss();
      _refreshCell();
    }

  }
  bool isInviteAccepted() {
    var invitations = widget.eventModel.invitations;
    for(int i = 0 ; i < invitations.length; i++) {
      var model = invitations[i];
      if(AppData.currentUser.id == model.id) {
        if(model.status == "accepted" || model.status == "paid")
          return true;
        else if(model.status == "rejected")
          return false;
      }
    }
    return null;

  }

  String getTimeAgo() {
    var date1 = DateTime.parse(widget.eventModel.createAt+" Z");
    return timeago.format(date1);
  }

  Widget getInvitedView() {
    double _imageWidth = AppDimensions.imageWidth(MediaQuery.of(context).size);
    var invitations = widget.eventModel.invitations;
    if(invitations != null && invitations.length > 0) {
      List<Widget> invites = new List();
      for(int i = 0; i < invitations.length; i++) {
        var invitedUser = invitations[i];
        if(i > 5) {
          invites.add(Container(
            height: 60,
            width: _imageWidth-10,
            decoration: BoxDecoration(
                shape: BoxShape.circle,
                color: AppColors.CYAN
            ),
            child: Center(
              child: Text("+${invitations.length-i}",
                style: TextStyle(color: Colors.white, fontSize: 18.0),
              ),
            ),
          ));
          break;
        }
        invites.add(ClipOval(
            child: CachedNetworkImage(
              width: _imageWidth,
              height: _imageWidth,
              fit: BoxFit.fill,
              imageUrl: invitedUser.image ?? "",
              placeholder: (context, url) => SizedBox(
                height: _imageWidth,
                width: _imageWidth,
                child: CircularProgressIndicator(strokeWidth: 1,),
              ),
              errorWidget: (context, url, error) => Icon(Icons.supervised_user_circle, color: AppColors.TEXT_GREY_BLUISH, size: _imageWidth,),
            ))
        );


      }
      return GestureDetector(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text("Invited People", style: TextStyle(color: AppColors.TEXT_GREY_BLUISH, fontSize: 18.0, fontFamily: 'Avenir'),),
            SizedBox(height: 5,),
            Row(
                children : invites
            )
          ],
        ),
        onTap: () {
          Navigator.pushNamed(context, '/AllFriends', arguments: widget.eventModel);
        },
      );

    } else {
      return Container();
    }

  }
}

String getFormattedTime(String time) {
  var date = DateTime.parse("1996-07-20 $time");
  var a = DateFormat('h:mm a').format(date);
  return a;
}