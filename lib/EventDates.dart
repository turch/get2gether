import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get2gether/EventCell.dart';
import 'package:get2gether/EventTimeSlotCell.dart';
import 'package:get2gether/UIComponents/AppButton.dart';
import 'package:get2gether/bloc/eventSlotsByFilter/EventSlotsByFilterBloc.dart';
import 'package:get2gether/bloc/eventSlotsByFilter/eventslotsbyfilter_event.dart';
import 'package:get2gether/bloc/eventSlotsByFilter/eventslotsbyfilter_state.dart';
import 'package:get2gether/models/Event.dart';
import 'package:get2gether/models/AvailabilitySlot.dart';
import 'package:get2gether/models/requests/EventsRequestByFilter.dart';
import 'package:get2gether/util/AppColors.dart';
import 'package:intl/intl.dart';
import 'package:table_calendar/table_calendar.dart';


class EventDates extends StatefulWidget {

  @override
  _EventsDatesState createState() => _EventsDatesState();
}

class _EventsDatesState extends State<EventDates>{
  Event _event;
  DateTime _currentDate;
  EventSlotsByFilterBloc _eventDatesBloc;
  CalendarController _calendarController;
  Map<DateTime, List<String>> _availabilitySlotsForCalendarIndicators = new Map();
  List<String> _currentDayEventSlots = new List();
  bool isCalenderMonthly = true;

  @override
  void initState() {
    _eventDatesBloc = new EventSlotsByFilterBloc();
    _currentDate = new DateTime.now();
    _calendarController = CalendarController();
    super.initState();
  }

  @override
  void dispose() {
    _calendarController.dispose();
    super.dispose();
  }

  void _onVisibleDaysChanged(DateTime first, DateTime last, CalendarFormat format) {
    if(format == CalendarFormat.month) {
      isCalenderMonthly = true;
    } else {
      isCalenderMonthly = false;
    }
  }

  Widget _buildCalendarMarker(DateTime date, dynamic event) {
    return Container(
      width: 10,
      height: 10,
      decoration: BoxDecoration(
        shape: BoxShape.circle,
        color: Colors.red,
      ),
    );
  }

  Widget build(BuildContext context) {
    final arguments = ModalRoute.of(context).settings.arguments;

    if (arguments is Event) {
      _event = arguments;
      _availabilitySlotsForCalendarIndicators = new Map();

      _event.dateTimeList.forEach((dateTimeSlot) {
        var date = DateTime.parse(dateTimeSlot.date);
        if (_availabilitySlotsForCalendarIndicators[date] == null) {
          _availabilitySlotsForCalendarIndicators[date] = new List();
        }

        _availabilitySlotsForCalendarIndicators[date].add(dateTimeSlot.time);
      });
    }

    return Scaffold(
        backgroundColor: AppColors.MAIN_BG_GREY,
        body: Stack(
          children: <Widget>[
            Container(
              padding: EdgeInsets.only(top: 30, bottom: 10),
              decoration: new BoxDecoration(
                  color: AppColors.CYAN,
                  borderRadius: new BorderRadius.only(
                      bottomLeft: const Radius.circular(30.0),
                      bottomRight: const Radius.circular(30.0)
                  )
              ),
              child: TableCalendar(
                startingDayOfWeek: StartingDayOfWeek.monday,
                calendarController: _calendarController,
                initialSelectedDay: _currentDate,
                initialCalendarFormat: CalendarFormat.month,
                events: _availabilitySlotsForCalendarIndicators,
                availableCalendarFormats: const {
                  CalendarFormat.month: 'Month',
                  CalendarFormat.week: 'Week',
                },
                onVisibleDaysChanged: _onVisibleDaysChanged,
                onDaySelected: (date, events) {
                  _currentDate = date;
                  _calendarController.setCalendarFormat(CalendarFormat.week);

                  var formatter = new DateFormat('yyyy-MM-dd');
                  var stringDate = formatter.format(_currentDate);

                  List<String> eventSlots = new List();

                  _event.dateTimeList.forEach((slot) {
                    if (slot.date == stringDate) {
                      eventSlots.add(slot.time);
                    }
                  });

                  setState(() {
                    _currentDayEventSlots = eventSlots;
                  });
                },

                headerStyle: HeaderStyle(
                  titleTextBuilder: (date, locale) => DateFormat.yMMM(locale).format(date),
                  titleTextStyle: TextStyle(fontFamily: 'Lato', color: Colors.white, fontSize: 20.0),
                  formatButtonTextStyle: TextStyle().copyWith(color: Colors.white, fontFamily: 'Lato'),
                  formatButtonDecoration: BoxDecoration(
                    color: AppColors.ORANGE,
                    borderRadius: BorderRadius.circular(16.0),
                  ),
                  leftChevronIcon: Icon(Icons.chevron_left, color: Colors.white),
                  rightChevronIcon: Icon(Icons.chevron_right, color: Colors.white),
                ),
                daysOfWeekStyle:  DaysOfWeekStyle(
                    weekdayStyle: TextStyle(color: Colors.white, fontWeight: FontWeight.w600),
                    weekendStyle: TextStyle(color: Colors.white, fontWeight: FontWeight.w600)
                ),
                calendarStyle: CalendarStyle(
                  selectedColor: AppColors.ORANGE,
                  todayColor:  AppColors.TEXT_GREY_BLUISH,
                  weekdayStyle: TextStyle(color: Colors.white),
                  weekendStyle: TextStyle(color: Colors.white),
                  outsideStyle: TextStyle(color: Colors.white),
                  outsideWeekendStyle: TextStyle(color: Colors.white),
                  outsideHolidayStyle: TextStyle(color: Colors.white),
                ),
                builders: CalendarBuilders(
                  markersBuilder: (context, date, events, holidays) {
                    final children = <Widget>[];
                    final dots = <Widget>[];

                    events.forEach((element) {
                      dots.add(_buildCalendarMarker(date, element));
                    });

                    children.add(
                        Row(
                          children: dots,
                          mainAxisAlignment: MainAxisAlignment.center,
                        )
                    );

                    return children;
                  },
                ),
              ),
            ),
            Container(
                height:  isCalenderMonthly ? 0 : null,
                margin: EdgeInsets.only(top: 170.0),
                child: _getChildViewByState()
            )
          ],
        )
    );

  }

  Widget _getChildViewByState() {
    var children = List<Widget>();

    if (_currentDayEventSlots.length == 0) {
      children.add(SizedBox(height: 10.0,));
      children.add(Text("This event does not occur on this date."));
    }

    var formatter = new DateFormat('yyyy-MM-dd');

    _currentDayEventSlots.forEach((timeSlot) {
      var slotCell = EventTimeSlotCell(timeSlot,formatter.format(_currentDate) , _event);

      children.add(
          slotCell
      );
    });

    children.add(SizedBox(height: 40.0,));

    return ListView(
        children: children
    );
  }

  _populateAvailabilitySlotsFromResponse(List<AvailabilitySlot> availabilitySlots) {
    /*_availabilitySlotsForCalendarIndicators.clear();

    availabilitySlots.forEach((slot) {
      DateTime date = DateTime.parse(slot.date);

      if (_availabilitySlotsForCalendarIndicators[date] == null) {
        _availabilitySlotsForCalendarIndicators[date] = List();
      }

      _availabilitySlotsForCalendarIndicators[DateTime.parse(slot.date)].add(slot);
    });*/
  }
}