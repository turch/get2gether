import 'package:dio/dio.dart';
import 'package:get2gether/models/GeneralResponse.dart';
import 'package:get2gether/models/requests/ForgotPasswordRequest.dart';
import 'package:get2gether/util/Constants.dart';

class ForgotPasswordAPIProvider {

  Future<GeneralResponse> forgotPassword(ForgotPasswordRequest data) async {
    try {
      Response response;
      Dio dio = new Dio();
      response = await dio.post(WebConstants.BASE_URL + WebConstants.FORGOT_PASSWORD,
          data: {"email": data.email
          });
      if(response != null) {
        print("Signup Api Response: "+ response.data['message'].toString());
        if(response.statusCode == 200) {
          return GeneralResponse.fromJson(response.data);
        }
        else{
          throw Exception("Request Failed");
        }
      }
    }catch (e) {
      print(e);
    }
  }
}