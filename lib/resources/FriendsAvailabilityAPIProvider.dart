import 'package:dio/dio.dart';
import 'package:get2gether/models/requests/FriendsAvailabilityByFilterRequest.dart';
import 'package:get2gether/resources/AppData.dart';
import 'package:get2gether/util/Constants.dart';

import 'package:get2gether/mock/FriendsAvailabilityByFilterMock.dart';

class FriendsAvailabilityByFilterAPIProvider {

  Future<FriendsAvailabilityByFilterResponse> fetchFriendsAvailability(FriendsAvailabilityByFilterRequest data) async {
    try {
      Response response;
      Dio dio = new Dio();
      dio.options.headers.addAll(
          {'Authorization' : "Bearer ${AppData.currentUser.token}",
            "Content-Type" : "application/x-www-form-urlencoded",
            "Accept" : "application/json",
            "Cookie" : "XDEBUG_SESSION=XDEBUG_ECLIPSE",
          }
      );

      //FriendsAvailabilityByFilterMock mock = FriendsAvailabilityByFilterMock(data.type, data.friendId);
      //dio.httpClientAdapter = mock.dioAdapterMock;

      String url = WebConstants.BASE_URL + WebConstants.GET_FRIENDS_AVAILABILITY;

      response = await dio.post(url,
          data: {
            "type" : data.type,
            "value": data.value,
            "year": data.year,
            "friends": data.friends,
          });
      if (response != null) {
        print("Api Response: "+ response.data['message'].toString());
        if(response.statusCode == 200) {
          return FriendsAvailabilityByFilterResponse.fromJson(response.data);
        }
        else {
          throw Exception("Request Failed");
        }
      }
    } catch (e) {
      print(e);
    }
  }
}