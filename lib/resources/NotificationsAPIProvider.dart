import 'package:dio/dio.dart';
import 'package:get2gether/models/GeneralResponse.dart';
import 'package:get2gether/models/requests/NotificationsRequest.dart';
import 'package:get2gether/resources/AppData.dart';
import 'package:get2gether/util/Constants.dart';

class NotificationsAPIProvider {

  Future<NotificationsResponse> fetNotifications(NotificationsRequest request) async {
    try {
      Response response;
      Dio dio = new Dio();
      dio.options.headers.addAll(
          {'Authorization' : "Bearer ${AppData.currentUser.token}",
            "Content-Type" : "application/x-www-form-urlencoded",
            "Accept" : "application/json"
          }
      );
      response = await dio.post(WebConstants.BASE_URL + WebConstants.GET_ALL_NOTIFICATIONS,
          data: {
            "user_id": AppData.currentUser.id,
            "page_size" : request.pageSize,
            "page_no": request.pageNumber
          });
      if(response != null) {
        print("Api Response: "+ response.data['message'].toString());
        if(response.statusCode == 200) {
          return NotificationsResponse.fromJson(response.data);
        }
        else {
          throw Exception("Request Failed");
        }
      }
    }catch (e) {
      print(e);
    }
  }
}