import 'package:dio/dio.dart';
import 'package:get2gether/models/GeneralResponse.dart';
import 'package:get2gether/models/requests/ResendCodeRequest.dart';
import 'package:get2gether/util/Constants.dart';

class ResendCodeAPIProvider {

  Future<GeneralResponse> resendCode(ResendCodeRequest data) async {
    try {
      Response response;
      Dio dio = new Dio();
      response = await dio.post(WebConstants.BASE_URL + WebConstants.RESEND_CODE,
          data: {
            "country_code" : data.countryCode,
            "phone": data.phoneNumber,
          });
      if(response != null) {
        print("Signup Api Response: "+ response.data['message'].toString());
        if(response.statusCode == 200) {
          return GeneralResponse.fromJson(response.data);
        }
        else{
          throw Exception("Request Failed");
        }
      }
    } catch (e) {
      print(e);
    }
  }
}