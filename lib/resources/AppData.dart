import 'package:contacts_service/contacts_service.dart';
import 'package:get2gether/models/AvailabilitySlot.dart';
import 'package:get2gether/models/Event.dart';
import 'package:get2gether/models/repositories/AvailabilitySlotRepository.dart';
import 'package:get2gether/models/Profile.dart';

class AppData {
  static Profile currentUser;
  static bool notificationSwitch = true;
  static AvailabilitySlotRepository currentUserAvailabilitySlots = new AvailabilitySlotRepository();
  static List<Event> currentUserEvents;
  static List<Event> currentUserInvitedEvents;
  static List<AvailabilitySlot> currentUserAvailability;
  static Iterable<Contact> contacts;

  static updateUserEvent(Event eventModel) {
    if(currentUserEvents != null) {
      for(int i = 0; i < currentUserEvents.length; i++) {
        var event = currentUserEvents[i];
        if(event.id == eventModel.id) {
          currentUserEvents[i] = eventModel;
        }
      }
    }
  }

}