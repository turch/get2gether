import 'package:dio/dio.dart';
import 'package:get2gether/models/requests/SigninRequest.dart';
import 'package:get2gether/util/Constants.dart';

class SigninAPIProvider {

  Future<SigninResponse> signin(SigninRequest data) async {
    try {
      Response response;
      Dio dio = new Dio();
      dio.options.headers.addAll({
            "Cookie" : "XDEBUG_SESSION=XDEBUG_ECLIPSE",
          }
      );

      response = await dio.post(WebConstants.BASE_URL + WebConstants.SIGN_IN,
          data: {"email": data.email ,
            "password": data.password,
            "device_id": data.deviceId,
            "device_type": data.deviceType,
            "fcm_token" : data.fcmToken
          });
      if(response != null) {
        print("Signin Api Response: "+ response.data['message'].toString());
        if(response.statusCode == 200) {
          return SigninResponse.fromJson(response.data);
        }
        else {
          throw Exception("SignIn Failed");
        }
      }
    } catch (e) {
      print(e);
    }
  }
}