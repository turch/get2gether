import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:get2gether/models/requests/EditAvailabilitySlotRequest.dart';
import 'package:get2gether/resources/AppData.dart';
import 'package:get2gether/util/Constants.dart';

class EditAvailabilitySlotAPIProvider {

  Future<EditAvailabilitySlotResponse> editAvailabilitySlot(EditAvailabilitySlotRequest data) async {
    try {
      Response response;
      Dio dio = new Dio();
      dio.options.headers.addAll(
          {'Authorization' : "Bearer ${AppData.currentUser.token}",
            "Content-Type" : "application/x-www-form-urlencoded",
            "Accept" : "application/json"
          }
      );
      response = await dio.post(WebConstants.BASE_URL + WebConstants.EDIT_AVAILABILITY_SLOT,
          data: json.encode(data.toMap()));
      if(response != null) {
        print("Api Response: "+ response.data['message'].toString());
        if(response.statusCode == 200) {
          return EditAvailabilitySlotResponse.fromJson(response.data);
        }
        else {
          throw Exception("Request Failed");
        }
      }
    }catch (e) {
      print(e);
    }
  }
}