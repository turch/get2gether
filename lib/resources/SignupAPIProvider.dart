import 'package:dio/dio.dart';
import 'package:get2gether/models/requests/SigninRequest.dart';
import 'package:get2gether/models/requests/SignupRequest.dart';
import 'package:get2gether/util/Constants.dart';

class SignupAPIProvider {

  Future<SigninResponse> signup(SignupRequest data) async {
    try {
      Response response;
      Dio dio = new Dio();
      response = await dio.post(WebConstants.BASE_URL + WebConstants.SIGN_UP,
          data: {"email": data.email ,
            "password": data.password,
            "first_name": data.firstName,
            "last_name": data.lastName,
            "country_code" : data.countryCode,
            "phone": data.phoneNumber
          });
      if(response != null) {
        print("Signup Api Response: "+ response.data['message'].toString());
        if(response.statusCode == 200) {
          return SigninResponse.fromJson(response.data);
        }
        else{
          throw Exception("SignIn Failed");
        }
      }
    }catch (e) {
      print(e);
    }
  }
}