import 'package:dio/dio.dart';
import 'package:get2gether/models/GeneralResponse.dart';
import 'package:get2gether/models/requests/SigninRequest.dart';
import 'package:get2gether/models/requests/VerifyCodeRequest.dart';
import 'package:get2gether/util/Constants.dart';

class VerifyCodeAPIProvider {

  Future<SigninResponse> verifyCode(VerifyCodeRequest data) async {
    try {
      Response response;
      Dio dio = new Dio();
      response = await dio.post(WebConstants.BASE_URL + WebConstants.VERIFY_CODE,
          data: {"device_id": data.deviceId,
            "device_type": data.deviceType,
            "fcm_token" : data.fcmToken,
            "country_code" : data.countryCode,
            "phone": data.phoneNumber,
            "verification_code": data.verificationCode
          });
      if(response != null) {
        print("Signup Api Response: "+ response.data['message'].toString());
        if(response.statusCode == 200) {
          return SigninResponse.fromJson(response.data);
        }
        else{
          throw Exception("Request Failed");
        }
      }
    }catch (e) {
      print(e);
    }
  }
}