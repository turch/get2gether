import 'package:dio/dio.dart';
import 'package:get2gether/models/requests/AvailabilitySlotsRequestByFilter.dart';
import 'package:get2gether/resources/AppData.dart';
import 'package:get2gether/util/Constants.dart';

class AvailabilitySlotsByFilterAPIProvider {

  Future<AvailabilitySlotsResponseByFilter> fetchAvailabilitySlotsByFilter(AvailabilitySlotsRequestByFilter data) async {
    try {
      Response response;
      Dio dio = new Dio();
      dio.options.headers.addAll(
          {'Authorization' : "Bearer ${AppData.currentUser.token}",
            "Content-Type" : "application/x-www-form-urlencoded",
            "Accept" : "application/json",
            "Cookie" : "XDEBUG_SESSION=XDEBUG_ECLIPSE",
          }
      );

      String url = WebConstants.BASE_URL + WebConstants.AVAILABILITY_SLOTS_BY_FILTER;

      response = await dio.post(url,
          data: {
            "type" : data.type,
            "value": data.value,
            "year": data.year,
          });
      if (response != null) {
        print("Api Response: "+ response.data['message'].toString());
        if(response.statusCode == 200) {
          return AvailabilitySlotsResponseByFilter.fromJson(response.data);
        }
        else {
          throw Exception("Request Failed");
        }
      }
    }catch (e) {
      print(e);
    }
  }
}