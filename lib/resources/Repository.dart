import 'package:get2gether/bloc/editUserProfile/edituserprofile_event.dart';
import 'package:get2gether/models/GeneralResponse.dart';
import 'package:get2gether/models/requests/CreateEventRequest.dart';
import 'package:get2gether/models/requests/CreateAvailabilitySlotRequest.dart';
import 'package:get2gether/models/requests/EditEventRequest.dart';
import 'package:get2gether/models/requests/EditAvailabilitySlotRequest.dart';
import 'package:get2gether/models/requests/EditUserProfileRequest.dart';
import 'package:get2gether/models/requests/ForgotPasswordRequest.dart';
import 'package:get2gether/models/requests/FriendsAvailabilityByFilterRequest.dart';
import 'package:get2gether/models/requests/EventSlotsByFilterRequest.dart';
import 'package:get2gether/models/requests/InviteFriendRequest.dart';
import 'package:get2gether/models/requests/NotificationsRequest.dart';
import 'package:get2gether/models/requests/RemoveFriendRequest.dart';
import 'package:get2gether/models/requests/ResendCodeRequest.dart';
import 'package:get2gether/models/requests/SigninRequest.dart';
import 'package:get2gether/models/requests/SignupRequest.dart';
import 'package:get2gether/models/requests/EventsRequestByFilter.dart';
import 'package:get2gether/models/requests/AvailabilitySlotsRequestByFilter.dart';
import 'package:get2gether/models/requests/UpdateInviteRequest.dart';
import 'package:get2gether/models/requests/UserEventsRequest.dart';
import 'package:get2gether/models/requests/VerifyCodeRequest.dart';
import 'package:get2gether/resources/CreateEventAPIProvider.dart';
import 'package:get2gether/resources/CreateAvailabilitySlotAPIProvider.dart';
import 'package:get2gether/resources/DeleteEventAPIProvider.dart';
import 'package:get2gether/resources/EditEventAPIProvider.dart';
import 'package:get2gether/resources/EditAvailabilitySlotAPIProvider.dart';
import 'package:get2gether/resources/EditUserProfileAPIProvider.dart';
import 'package:get2gether/resources/EventDetailAPIProvider.dart';
import 'package:get2gether/resources/InviteFriendAPIProvider.dart';
import 'package:get2gether/resources/RemoveFriendAPIProvider.dart';
import 'package:get2gether/resources/SigninAPIProvider.dart';
import 'package:get2gether/resources/SignupAPIProvider.dart';
import 'package:get2gether/resources/UpdateInviteAPIProvider.dart';
import 'package:get2gether/resources/UserEventsAPIProvider.dart';
import 'package:get2gether/resources/EventsByFilterAPIProvider.dart';
import 'package:get2gether/resources/AvailabilitySlotsByFilterAPIProvider.dart';
import 'package:get2gether/resources/FriendsAvailabilityAPIProvider.dart';
import 'package:get2gether/resources/EventSlotsByFilterAPIProvider.dart';

import 'ForgotPasswordAPIProvider.dart';
import 'NotificationsAPIProvider.dart';
import 'ResendCodeAPIProvider.dart';
import 'VerifyCodeAPIProvider.dart';


class Repository {


  final _signinApiProvider = SigninAPIProvider();
  Future<SigninResponse> signinUser(SigninRequest data) => _signinApiProvider.signin(data);

  final _signupApiProvider = SignupAPIProvider();
  Future<SigninResponse> signupUser(SignupRequest data) => _signupApiProvider.signup(data);

  final _verifyCodeApiProvider = VerifyCodeAPIProvider();
  Future<SigninResponse> verifyCode(VerifyCodeRequest data) => _verifyCodeApiProvider.verifyCode(data);

  final _resendCodeApiProvider = ResendCodeAPIProvider();
  Future<GeneralResponse> resendCode(ResendCodeRequest data) => _resendCodeApiProvider.resendCode(data);

  final _forgotPasswordApiProvider = ForgotPasswordAPIProvider();
  Future<GeneralResponse> forgotPassword(ForgotPasswordRequest data) => _forgotPasswordApiProvider.forgotPassword(data);

  final _createEventApiProvider = CreateEventAPIProvider();
  Future<GeneralResponse> createEvent(CreateEventRequest data) => _createEventApiProvider.createEvent(data);

  final _createAvailabilitySlotApiProvider = CreateAvailabilitySlotAPIProvider();
  Future<GeneralResponse> createAvailabilitySlot(CreateAvailabilitySlotRequest data) => _createAvailabilitySlotApiProvider.createAvailabilitySlot(data);

  final _eventsByFilterApiProvider = EventsByFilterAPIProvider();
  Future<EventsResponseByFilter> fetchEventsByFilter(EventsRequestByFilter data) => _eventsByFilterApiProvider.fetchEventsByFilter(data);

  final _availabilitySlotsByFilterApiProvider = AvailabilitySlotsByFilterAPIProvider();
  Future<AvailabilitySlotsResponseByFilter> fetchAvailabilitySlotsByFilter(AvailabilitySlotsRequestByFilter data) => _availabilitySlotsByFilterApiProvider.fetchAvailabilitySlotsByFilter(data);

  final _userEventsApiProvider = UserEventsAPIProvider();
  Future<UserEventsResponse> fetchUserEvents(UserEventsRequest data) => _userEventsApiProvider.fetchUserEvents(data);

  final _deleteEventApiProvider = DeleteEventAPIProvider();
  Future<GeneralResponse> deleteEvent(int eventId) => _deleteEventApiProvider.deleteEvent(eventId);

  final _notificationsApiProvider = NotificationsAPIProvider();
  Future<NotificationsResponse> fetNotifications(NotificationsRequest request) => _notificationsApiProvider.fetNotifications(request);

  final _editProfileApiProvider = EditUserProfileAPIProvider();
  Future<EditUserProfileResponse> editProfile(EditUserProfileRequest request, UpdateRequest requestType) => _editProfileApiProvider.editUserProfile(request, requestType);

  final _editEventApiProvider = EditEventAPIProvider();
  Future<EditEventResponse> editEvent(EditEventRequest data) => _editEventApiProvider.editEvent(data);

  final _editAvailabilitySlotApiProvider = EditAvailabilitySlotAPIProvider();
  Future<EditAvailabilitySlotResponse> editAvailabilitySlot(EditAvailabilitySlotRequest data) => _editAvailabilitySlotApiProvider.editAvailabilitySlot(data);

  final _updateEventApiProvider = UpdateInviteAPIProvider();
  Future<EditEventResponse> updateInvite(UpdateInviteRequest data) => _updateEventApiProvider.updateInvite(data);

  final _inviteFriendApiProvider = InviteFriendAPIProvider();
  Future<EditEventResponse> inviteFriend(InviteFriendRequest data) => _inviteFriendApiProvider.inviteFriend(data);

  final _removeFriendApiProvider = RemoveFriendAPIProvider();
  Future<EditEventResponse> removeFriend(RemoveFriendRequest data) => _removeFriendApiProvider.removeFriend(data);

  final _eventDetailAPIProvider = EventDetailAPIProvider();
  Future<EditEventResponse> viewEventDetail(String id) => _eventDetailAPIProvider.createEventDetail(id);

  final _friendsAvailabilityByFilterApiProvider = FriendsAvailabilityByFilterAPIProvider();
  Future<FriendsAvailabilityByFilterResponse> fetchFriendsAvailabilityByFilter(FriendsAvailabilityByFilterRequest data) => _friendsAvailabilityByFilterApiProvider.fetchFriendsAvailability(data);

  final _eventSlotsByFilterApiProvider = EventSlotsByFilterAPIProvider();
  Future<EventSlotsByFilterResponse> fetchEventSlotsByFilter(EventSlotsByFilterRequest data) => _eventSlotsByFilterApiProvider.fetchEventSlots(data);
}