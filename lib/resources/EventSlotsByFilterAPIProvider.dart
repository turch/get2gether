import 'package:dio/dio.dart';
import 'package:get2gether/mock/EventSlotsByFilterMock.dart';
import 'package:get2gether/models/requests/EventSlotsByFilterRequest.dart';
import 'package:get2gether/resources/AppData.dart';
import 'package:get2gether/util/Constants.dart';

import 'package:get2gether/mock/EventsByFilterMock.dart';

class EventSlotsByFilterAPIProvider {

  Future<EventSlotsByFilterResponse> fetchEventSlots(EventSlotsByFilterRequest data) async {
    try {
      Response response;
      Dio dio = new Dio();
      dio.options.headers.addAll(
          {'Authorization' : "Bearer ${AppData.currentUser.token}",
            "Content-Type" : "application/x-www-form-urlencoded",
            "Accept" : "application/json"
          }
      );

      EventSlotsByFilterMock mock = EventSlotsByFilterMock(data.type);
      dio.httpClientAdapter = mock.dioAdapterMock;

      String url = WebConstants.BASE_URL + WebConstants.GET_EVENT_SLOTS;

      response = await dio.post(url,
          data: {
            "type" : data.type,
            "value": data.value,
            "year": data.year,
          });
      if (response != null) {
        print("Api Response: "+ response.data['message'].toString());
        if(response.statusCode == 200) {
          return EventSlotsByFilterResponse.fromJson(response.data);
        }
        else {
          throw Exception("Request Failed");
        }
      }
    } catch (e) {
      print(e);
    }
  }
}