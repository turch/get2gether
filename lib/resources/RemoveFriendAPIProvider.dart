import 'package:dio/dio.dart';
import 'package:get2gether/models/GeneralResponse.dart';
import 'package:get2gether/models/requests/EditEventRequest.dart';
import 'package:get2gether/models/requests/RemoveFriendRequest.dart';
import 'package:get2gether/resources/AppData.dart';
import 'package:get2gether/util/Constants.dart';

class RemoveFriendAPIProvider {

  Future<EditEventResponse> removeFriend(RemoveFriendRequest data) async {
    try {
      Response response;
      Dio dio = new Dio();
      dio.options.headers.addAll(
          {'Authorization' : "Bearer ${AppData.currentUser.token}",
            "Content-Type" : "application/x-www-form-urlencoded",
            "Accept" : "application/json"
          }
      );
      response = await dio.post(WebConstants.BASE_URL + WebConstants.REMOVE_FRIEND,
          data: {
            "event_id": data.eventId,
            "user_id" : data.userId
          });
      if(response != null) {
        print("Api Response: "+ response.data['message'].toString());
        if(response.statusCode == 200) {
          return EditEventResponse.fromJson(response.data);
        }
        else {
          throw Exception("Request Failed");
        }
      }
    }catch (e) {
      print(e);
    }
  }
}