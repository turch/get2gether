import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:get2gether/models/requests/EditEventRequest.dart';
import 'package:get2gether/models/requests/UpdateInviteRequest.dart';
import 'package:get2gether/resources/AppData.dart';
import 'package:get2gether/util/Constants.dart';

class UpdateInviteAPIProvider {

  Future<EditEventResponse> updateInvite(UpdateInviteRequest data) async {
    try {
      Response response;
      Dio dio = new Dio();
      dio.options.headers.addAll(
          {'Authorization' : "Bearer ${AppData.currentUser.token}",
            "Content-Type" : "application/x-www-form-urlencoded",
            "Accept" : "application/json",
            "Cookie" : "XDEBUG_SESSION=XDEBUG_ECLIPSE",
          }
      );
      response = await dio.post(WebConstants.BASE_URL + WebConstants.UPDATE_INVITATION_STATUS,
          data: {"event_id": data.eventId ,
            "user_id": data.userId,
            "status": data.status,
            "selected_date": data.selectedDate,
            "selected_time": data.selectedTime,
          });
      if(response != null) {
        print("Api Response: "+ response.data['message'].toString());
        if(response.statusCode == 200) {
          return EditEventResponse.fromJson(response.data);
        }
        else {
          throw Exception("Request Failed");
        }
      }
    }catch (e) {
      print(e);
    }
  }
}