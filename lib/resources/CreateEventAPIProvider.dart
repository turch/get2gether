import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:get2gether/models/requests/CreateEventRequest.dart';
import 'package:get2gether/resources/AppData.dart';
import 'package:get2gether/util/Constants.dart';

class CreateEventAPIProvider {

  Future<CreateEventResponse> createEvent(CreateEventRequest data) async {
    try {
      Response response;
      Dio dio = new Dio();
      dio.options.headers.addAll(
          {'Authorization' : "Bearer ${AppData.currentUser.token}",
            "Content-Type" : "application/x-www-form-urlencoded",
            "Accept" : "application/json",
            "Cookie" : "XDEBUG_SESSION=XDEBUG_ECLIPSE",
          }
      );
      var a = data.toMap();
      var b = json.encode(a);
      response = await dio.post(WebConstants.BASE_URL + WebConstants.CREATE_EVENT,
          data: b);
      if(response != null) {
        print("Api Response: "+ response.data['message'].toString());
        if(response.statusCode == 200) {
          return CreateEventResponse.fromJson(response.data);
        }
        else {
          throw Exception("Request Failed");
        }
      }
    }catch (e) {
      print(e);
    }
  }
}