import 'dart:io';

import 'package:dio/dio.dart';
import 'package:get2gether/bloc/editUserProfile/edituserprofile_event.dart';
import 'package:get2gether/models/GeneralResponse.dart';
import 'package:get2gether/models/requests/EditUserProfileRequest.dart';
import 'package:get2gether/resources/AppData.dart';
import 'package:get2gether/util/Constants.dart';

class EditUserProfileAPIProvider {

  Future<EditUserProfileResponse> editUserProfile(EditUserProfileRequest request, UpdateRequest requestType) async {
    try {
      Response response;
      Dio dio = new Dio();
      dio.options.headers.addAll(
          {'Authorization' : "Bearer ${AppData.currentUser.token}",
            "Content-Type" : "application/x-www-form-urlencoded",
            "Accept" : "application/json"
          }
      );
      var data = {};
      /*if(requestType == UpdateRequest.firstName) {
        data = new FormData.from({
          "user_id" : request.userId,
          "first_name" : request.firstName
        });
      } else if(requestType == UpdateRequest.lastName) {
        data = new FormData.from({
          "user_id" : request.userId,
          "last_name" : request.lastName
        });
      } else if(requestType == UpdateRequest.image) {
        data = new FormData.from({
          "user_id" : request.userId,
          "image" : new UploadFileInfo(new File(request.image.path), request.image.path, contentType: ContentType('image', 'jpeg')),
        });
      }*/
      response = await dio.post(WebConstants.BASE_URL + WebConstants.EDIT_PROFILE,
          data: data);
      if(response != null) {
        print("Api Response: "+ response.data['message'].toString());
        if(response.statusCode == 200) {
          return EditUserProfileResponse.fromJson(response.data);
        }
        else {
          throw Exception("Request Failed");
        }
      }
    }catch (e) {
      print(e);
    }
  }
}