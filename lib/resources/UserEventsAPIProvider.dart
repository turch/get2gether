import 'package:dio/dio.dart';
import 'package:get2gether/models/requests/UserEventsRequest.dart';
import 'package:get2gether/resources/AppData.dart';
import 'package:get2gether/util/Constants.dart';

class UserEventsAPIProvider {

  Future<UserEventsResponse> fetchUserEvents(UserEventsRequest data) async {
    try {
      Response response;
      Dio dio = new Dio();
      dio.options.headers.addAll(
          {'Authorization' : "Bearer ${AppData.currentUser.token}",
            "Content-Type" : "application/x-www-form-urlencoded",
            "Accept" : "application/json",
            "Cookie" : "XDEBUG_SESSION=XDEBUG_ECLIPSE",
          }
      );

      response = await dio.post(WebConstants.BASE_URL + WebConstants.USER_EVENTS,
          data: {
            "user_id" : data.userId,
          });
      if(response != null) {
        print("Api Response: "+ response.data['message'].toString());
        if(response.statusCode == 200) {
          return UserEventsResponse.fromJson(response.data);
        }
        else {
          throw Exception("Request Failed");
        }
      }
    }catch (e) {
      print(e);
    }
  }
}