import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get2gether/UIComponents/AppButton.dart';
import 'package:get2gether/bloc/userEvents/userEventsBloc.dart';
import 'package:get2gether/bloc/userEvents/userevents_event.dart';
import 'package:get2gether/bloc/userEvents/userevents_state.dart';
import 'package:get2gether/resources/AppData.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

import 'EventCell.dart';
import 'models/Event.dart';
import 'models/requests/UserEventsRequest.dart';

class InvitedEvents extends StatefulWidget {

  @override
  _InvitedEventsState createState() => _InvitedEventsState();
}

class _InvitedEventsState extends State<InvitedEvents>{

  UserEventsBloc _userEventsBloc;
  @override
  void initState() {
    _userEventsBloc = BlocProvider.of<UserEventsBloc>(context);
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }


  Widget build(BuildContext context) {
    return BlocBuilder<UserEventsBloc, UserEventsBlocState>(
        bloc: _userEventsBloc,
        builder: (context, state) {
          if (state is UserEventsFetched || state is EventDeleted) {
            if (AppData.currentUserInvitedEvents != null &&
                AppData.currentUserInvitedEvents.length > 0) {
              return Container(
                  padding: EdgeInsets.only(top: 10.0),
                  child: ListView.builder(
                      itemCount: AppData.currentUserInvitedEvents.length,
                      itemBuilder: (BuildContext context, int index) {
                        var eventModel = AppData
                            .currentUserInvitedEvents[index];
                        return EventsCell(eventModel, deleteCallback: () {
                          _userEventsBloc.add(new DeleteEvent(eventModel.id));
                        },);
                      }
                  )
              );
            } else {
              return Center(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text("You haven’t been invited to any events yet."),
                      SizedBox(height: 10.0,),
                      AppButton('Create an event yourself', _createEvent)
                    ],
                  )
              );
            }
          } else if (state is UserEventsInProgress) {
            return Center(
              child: CircularProgressIndicator(),
            );
          } else {
            return Container();
          }
        }
    );
  }

  _createEvent() {
    Navigator.pushNamed(context, '/CreateEvent');
  }
}