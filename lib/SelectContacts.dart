import 'package:autocomplete_textfield/autocomplete_textfield.dart';
import 'package:contacts_service/contacts_service.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get2gether/UIComponents/AppButton.dart';
import 'package:get2gether/UIComponents/OrangeBorderTransparentButton.dart';
import 'package:get2gether/bloc/updateEventInvites/updateEventInvitesBloc.dart';
import 'package:get2gether/bloc/updateEventInvites/updateeventinvites_state.dart';
import 'package:get2gether/models/Event.dart';
import 'package:get2gether/models/requests/InviteFriendRequest.dart';
import 'package:get2gether/models/requests/RemoveFriendRequest.dart';
import 'package:get2gether/resources/AppData.dart';
import 'package:get2gether/util/AppColors.dart';
import 'package:get2gether/util/AppDimensions.dart';
import 'package:get2gether/util/GeneralUtil.dart';
import 'package:get2gether/util/Strings.dart';

import 'bloc/updateEventInvites/updateeventinvites_event.dart';

class SelectContacts extends StatefulWidget {

  List<Contact> selectedContacts;
  Event event;

  SelectContacts(this.selectedContacts, {this.event});

  @override
  _SelectContactsState createState() => _SelectContactsState();
}

class _SelectContactsState extends State<SelectContacts>{

  TextEditingController _friendNameController;
  TextEditingController _nameController;
  TextEditingController _emailController;
  TextEditingController _phoneController;
  bool _isPhoneFieldVisible;
  List<Contact> _selectedContacts;
  UpdateEventInvitesBloc _updateEventInvitesBloc;

  @override
  void initState() {
    print(AppData.contacts);
    if(AppData.contacts == null) {
      ContactsService.getContacts().then((value) {
        setState(() {
          AppData.contacts = value;
        });
      });
    }
    _selectedContacts = widget.selectedContacts;
    _isPhoneFieldVisible = true;
    _friendNameController = TextEditingController();
    _nameController = TextEditingController();
    _phoneController = TextEditingController();
    _emailController = TextEditingController();
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }
  Future<bool> _onWillPop() async {
    _popWithSelection();
    return false;
  }

  Widget build(BuildContext context) {
    return new WillPopScope(
        onWillPop: _onWillPop,
        child: BlocProvider<UpdateEventInvitesBloc>(
            create: (BuildContext context) {
              _updateEventInvitesBloc = UpdateEventInvitesBloc();
              return _updateEventInvitesBloc;
            },
            child:BlocListener<UpdateEventInvitesBloc, UpdateEventInvitesBlocState>(
                listener: (context, state) {
                  if(state is FriendRemoved) {
                    widget.event = state.response.event;
                  } else if(state is FriendInvited) {
                    widget.event = state.response.event;
                  }
                },
                child: BlocBuilder<UpdateEventInvitesBloc, UpdateEventInvitesBlocState>(
                    bloc: _updateEventInvitesBloc,
                    builder: (context, state) {
                      GlobalKey<AutoCompleteTextFieldState<Contact>> key = new GlobalKey();
                      return Scaffold(
                          backgroundColor: AppColors.MAIN_BG_GREY,
                          appBar: AppBar(
                              elevation: 1.0,
                              backgroundColor: AppColors.CYAN,
                              centerTitle: true,
                              title: Text(
                                "Select Friends", style: TextStyle(color: Colors.white),),
                              leading: IconButton(
                                icon: Icon(Icons.arrow_back, color: Colors.white,),
                                onPressed: _popWithSelection,
                              ),
                            actions: <Widget>[
                              Container(
                                margin: EdgeInsets.only(right: 15),
                                child: GestureDetector(
                                  child: Icon(Icons.group_add, color: Colors.white, size: 30.0,),
                                  onTap: (){
                                    showGeneralDialog(
                                      context: context,
                                      barrierDismissible: false,
                                      barrierLabel: MaterialLocalizations.of(context).modalBarrierDismissLabel,
                                      barrierColor: Colors.black45,
                                      transitionDuration: const Duration(milliseconds: 200),
                                      pageBuilder: (BuildContext buildContext, Animation animation, Animation secondaryAnimation) {
                                        return StatefulBuilder(
                                            builder: (BuildContext context, StateSetter setState) {
                                              return Center(
                                                child: Container(
                                                  width: MediaQuery.of(context).size.width - 20,
                                                  height: 280,
                                                  padding: EdgeInsets.all(10),
                                                  decoration: BoxDecoration(
                                                    borderRadius: BorderRadius.all(Radius.circular(12.0)),
                                                    color: AppColors.MAIN_BG_GREY,
                                                  ),
                                                  child: Column(
                                                    mainAxisAlignment: MainAxisAlignment.center,
                                                    children: <Widget>[
                                                      Container(
                                                        margin: EdgeInsets.only(right: 40),
                                                        child: CustomTextField("Type your friend's name", _nameController, TextInputType.text, (nameController){_nameController = nameController;}),
                                                      ),
                                                      SizedBox(height: 10,),
                                                      Row(
                                                        mainAxisAlignment: MainAxisAlignment.center,
                                                        children: <Widget>[
                                                          Expanded(
                                                            child:  _isPhoneFieldVisible ?
                                                            CustomTextField("Type your friend's phone number", _phoneController, TextInputType.phone, (phoneController){_phoneController = phoneController;}):
                                                            CustomTextField("Type your friend's email address", _emailController, TextInputType.emailAddress, (emailController){_emailController = emailController;}),
                                                          ),
                                                          SizedBox(width: 5,),
                                                          GestureDetector(
                                                            child: Container(
                                                              alignment: Alignment.center,
                                                              height: 40, width: 40,
                                                              decoration: BoxDecoration(
                                                                shape: BoxShape.circle,
                                                                color: AppColors.ORANGE,
                                                              ),
                                                              child: Icon(_isPhoneFieldVisible ? Icons.phone : Icons.email, color: Colors.white, size: 20,),
                                                            ),
                                                            onTap: (){
                                                              setState(() {
                                                                FocusScope.of(context).unfocus();
                                                                _isPhoneFieldVisible = !_isPhoneFieldVisible;
                                                              });
                                                            },
                                                          ),
                                                        ],
                                                      ),
                                                      SizedBox(height: 30,),
                                                      Container(
                                                        width: 150,
                                                        height: 40,
                                                        child: OrangeBorderTransparentButton("Add Contact" ,(){
                                                          var item = Contact(displayName: _nameController.text,
                                                              emails: _isPhoneFieldVisible ? null : [Item(label: "Email", value: _emailController.text)],
                                                              phones: _isPhoneFieldVisible ? [Item(label: "Phone", value: _phoneController.text)] : null);
                                                          Navigator.of(context).pop();
                                                          if(widget.event == null)
                                                            _addFriend(item);
                                                          else {
                                                            _getConfirmationAndAddFriend(item);
                                                          }
                                                          _nameController.text = ""; _emailController.text = ""; _phoneController.text = "";

                                                        }, colorText: AppColors.ORANGE,),
                                                      ),
                                                      SizedBox(height: 10,),
                                                      Container(
                                                        width: 150,
                                                        height: 40,
                                                        child: OrangeBorderTransparentButton(
                                                          "Cancel" ,(){
                                                            _nameController.text = ""; _emailController.text = ""; _phoneController.text = "";
                                                            Navigator.of(context).pop();
                                                          }, colorText: AppColors.ORANGE,),
                                                      )
                                                    ],
                                                  ),
                                                ),
                                              );
                                            });
                                      });
                                  },
                                ),
                              )
                            ],
                          ),
                          body: AppData.contacts != null && AppData.contacts.length > 0 ?
                          Stack(
                            children: <Widget>[
                              Container(
                                margin: EdgeInsets.all(10.0),
                                height: AppDimensions.buttonHeight(),
                                width: MediaQuery
                                    .of(context)
                                    .size
                                    .width,
                                child: new AutoCompleteTextField<Contact>(
                                  itemBuilder: (context, item) {
                                    return Row(
                                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                      children: <Widget>[
                                        Text(getStringToCompare(item),
                                          style: TextStyle(
                                              fontSize: 16.0
                                          ),),
                                        Padding(
                                          padding: EdgeInsets.all(15.0),
                                        ),

                                      ],
                                    );
                                  },
                                  itemFilter: (item, query) {
                                    return getStringToCompare(item).toLowerCase().startsWith(query.toLowerCase());
                                  },
                                  itemSorter: (a, b) {
                                    return getStringToCompare(a).compareTo(getStringToCompare(b));
                                  },
                                  itemSubmitted: (item) {
                                    if(widget.event == null)
                                      _addFriend(item);
                                    else
                                      _getConfirmationAndAddFriend(item);
                                    _friendNameController.text = "";
                                  },
                                  suggestions: AppData.contacts.toList(),
                                  style: TextStyle(fontSize: 16.0, color: AppColors.TEXT_GREY_BLUISH),
                                  decoration: new InputDecoration(
                                    contentPadding: EdgeInsets.fromLTRB(15.0, 15.0, 60.0, 15.0),
                                    focusedBorder: OutlineInputBorder(
                                      borderSide: BorderSide(color: AppColors.TEXT_GREY, width: 1.0),
                                    ),
                                    enabledBorder: OutlineInputBorder(
                                      borderSide: BorderSide(color: Colors.white, width: 1.0),
                                    ),
                                    filled: true,
                                    hintStyle: new TextStyle(color: AppColors.HINT_COLOR),
                                    hintText: "Type your friend's name here…",
                                    fillColor: Colors.white,

                                  ),
                                  controller: _friendNameController,
                                ),

                              ),
                              Container(
                                  margin: EdgeInsets.only(top: AppDimensions.buttonHeight() + 10),
                                  child:  Padding(
                                    padding: EdgeInsets.only(bottom: 70.0),
                                    child: ListView.builder(
                                      itemCount: _selectedContacts == null ? 0 : _selectedContacts.length,
                                      itemBuilder: (context, position) {
                                        return getFriendCell(_selectedContacts[position]);
                                      },
                                    ),
                                  )
                              ),
                              Align(
                                  alignment: AlignmentDirectional.bottomCenter,
                                  child: Padding(
                                    padding: EdgeInsets.only(bottom: 10.0),
                                    child: AppButton(Strings.confirm, _popWithSelection, loading: state is UpdateInviteInProgress,),
                                  )
                              )

                            ],
                          ): Center(
                              child: CircularProgressIndicator()
                          )
                      );
                    }
                )
            )
        )
    );

  }

  String getStringToCompare(item){
    String displayName = item.displayName;
    String familyName = item.familyName;
    String givenName = item.givenName;
    String stringToCompare = '';

    if(displayName != null && displayName.isNotEmpty) {
      stringToCompare = displayName;
    } else if(familyName != null && familyName.isNotEmpty) {
      stringToCompare = familyName;
    } else if(givenName != null && givenName.isNotEmpty) {
      stringToCompare = givenName;
    }
    return stringToCompare;
  }

  Widget getFriendCell(Contact contact) {
    return Card(
      color: Colors.white,
      child: Container(
          width: MediaQuery.of(context).size.width,
          margin: EdgeInsets.all(10.0),
          child: Column(
            children: <Widget>[
              Row(
                mainAxisAlignment : MainAxisAlignment.spaceBetween,
                children: <Widget>[
  //                Image.asset(widget.notification.friend.profileImage, width: 40.0, height: 40.0, fit: BoxFit.fill,),
                  Flexible(
                    child: Padding(
                      padding: EdgeInsets.only(left: 10.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Text(contact.displayName, style: TextStyle(color: AppColors.TEXT_BLACK, fontSize: 16.0, fontFamily: 'Avenir'),),
                          SizedBox(height: 5,),
                          Text(getPhoneOrEmail(contact), style: TextStyle(color: AppColors.TEXT_GREY_BLUISH, fontSize: 14.0, fontFamily: 'Avenir'), maxLines: 2,),
                        ],

                      ),
                    ),
                  ),
                  GestureDetector(
                    child: Icon(Icons.cancel, color: AppColors.TEXT_GREY_BLUISH,),
                    onTap: () {
                      if(widget.event != null)
                        _getConfirmationAndRemoveFriend(contact);
                      else
                        _removeFriend(contact);
                      },
                  )

                ]
              ),
              SizedBox(height: 15.0,),
              AppButton("View Availability", () => _friendsAvailability(contact.givenName + ' '  + contact.familyName), color: AppColors.CYAN),
            ]
          )
      ),

    );
  }

  String getPhoneOrEmail(Contact contact) {
    String text = "";
    if(contact.phones != null){
      if(contact.phones.length > 0)
        text = contact.phones.first.value;
    } else if(contact.emails != null) {
      if(contact.emails.length > 0)
        text = contact.emails.first.value;
    }
    return text.replaceAll(RegExp('-'), '');
  }

  _friendsAvailability(String name) {
    Navigator.pushNamed(context, '/FriendsAvailability', arguments: name);
  }

  _addFriend(Contact friend) {
    if(_selectedContacts == null)
      _selectedContacts = new List();
    setState(() {
      _selectedContacts.add(friend);
    });
  }

  _removeFriend(Contact friend) {
    setState(() {
      _selectedContacts.remove(friend);
    });
  }

  _getConfirmationAndRemoveFriend(Contact friend) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          title: new Text("Remove invite from ${widget.event.title}"),
          content: new Text("Are you sure you want to remove ${friend.displayName} from event?"),
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            new FlatButton(
              child: new Text("No"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            new FlatButton(
              child: new Text("Yes"),
              onPressed: () {
                _updateEventInvitesBloc.add(new RemoveFriend(new RemoveFriendRequest(widget.event.id , int.parse(friend.suffix))));
                _removeFriend(friend);
                Navigator.of(context).pop();
              },
            ),

          ],
        );
      },
    );


  }
  _popWithSelection() {
    if(widget.event != null) { //user is was here for edit
      Navigator.pop(context, widget.event);
    } else {
      Navigator.pop(context, _selectedContacts);
    }
  }

  void _getConfirmationAndAddFriend(Contact contact) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          title: new Text("Add new invite in ${widget.event.title}"),
          content: new Text("Are you sure you want to add ${contact.displayName} to this event?"),
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            new FlatButton(
              child: new Text("No"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            new FlatButton(
              child: new Text("Yes"),
              onPressed: () {
                InviteFriend inviteFriend;
                if(contact.phones != null && contact.phones.single != null) {
                  var list = GeneralUtil.getCountryCode(context, contact.phones.single.value);
                  inviteFriend = new InviteFriend(new InviteFriendRequest(contact.displayName, list[1], list[0], widget.event.id, null));
                } else {
                  inviteFriend = new InviteFriend(new InviteFriendRequest(contact.displayName, null, null, widget.event.id, contact.emails.single.value));
                }
                _updateEventInvitesBloc.add(inviteFriend);
                _addFriend(contact);
                Navigator.of(context).pop();
              },
            ),

          ],
        );
      },
    );
  }
}

class CustomTextField extends StatefulWidget{
  String _textFieldHint;
  TextEditingController textFieldController;
  TextInputType _textInputType;
  Function callback;
  CustomTextField(this._textFieldHint, this.textFieldController, this._textInputType, this.callback);
  @override
  _CustomTextFieldState createState() => _CustomTextFieldState();
}

class _CustomTextFieldState extends State<CustomTextField> {
  @override
  Widget build(BuildContext context) {
    return Card(
      child: TextField(
        onChanged: ((text) {
          widget.callback(widget.textFieldController);
          print("===== txt = ${text}");
        }),
        controller: widget.textFieldController,
        keyboardType: widget._textInputType,
        maxLines: 1,
        style: TextStyle(fontSize: 16.0, color: AppColors.TEXT_GREY_BLUISH),
        decoration: new InputDecoration(
          contentPadding: EdgeInsets.fromLTRB(15.0, 15.0, 60.0, 15.0),
          focusedBorder: OutlineInputBorder(
            borderSide: BorderSide(color: AppColors.TEXT_GREY, width: 1.0),
          ),
          enabledBorder: OutlineInputBorder(
            borderSide: BorderSide(color: Colors.white, width: 1.0),
          ),
          filled: true,
          hintStyle: new TextStyle(color: AppColors.HINT_COLOR, fontSize: 12),
          hintText: widget._textFieldHint,
          fillColor: Colors.white,
        ),
        textCapitalization: TextCapitalization.none,
      ),
    );
  }
}