import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get2gether/AllInvitedFriends.dart';
import 'package:get2gether/CreateEvent.dart';
import 'package:get2gether/CreateAvailabilitySlot.dart';
import 'package:get2gether/FriendsAvailability.dart';
import 'package:get2gether/EventDates.dart';
import 'package:get2gether/Home.dart';
import 'package:get2gether/WebviewContainer.dart';
import 'package:get2gether/bloc/userEvents/userEventsBloc.dart';
import 'package:get2gether/bloc/eventsByFilter/EventsByFilterBloc.dart';
import 'package:get2gether/bloc/availabilitySlotsByFilter/AvailabilitySlotsByFilterBloc.dart';
import 'package:get2gether/onboarding/LoginOptions.dart';
import 'package:get2gether/util/SharedPrefHelper.dart';

import 'EventDetail.dart';
import 'Home.dart';
import 'onboarding/OnboardingContainer.dart';


void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  Widget _defaultHome = new LoginOptions();

  // Get result of the login function.
  bool _result = await SharedPreferencesHelper.getValueByKey(SharedPreferenceKeys.USER_LOGGED_IN);
  if (_result) {
    _defaultHome = new Home();
  }

  runApp(MyApp(_defaultHome));
}

class MyApp extends StatelessWidget {

  Widget _defaultHome;
  MyApp(this._defaultHome);

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider<UserEventsBloc>(
          create: (BuildContext context) => UserEventsBloc(),
        ),
        BlocProvider<EventsByFilterBloc>(
          create: (BuildContext context) => EventsByFilterBloc(),
        ),
        BlocProvider<AvailabilitySlotsByFilterBloc>(
          create: (BuildContext context) => AvailabilitySlotsByFilterBloc(),
        ),
      ],
      child:  MaterialApp(
        title: 'Get2Gether',
        theme: ThemeData(
            primarySwatch: Colors.orange,
            fontFamily: 'Lato'
        ),
        routes: {
          '/Onboarding': (context) => OnboardingContainer(),
          '/Webview': (context) => WebViewContainer(),
          '/Home' : (context) => Home(),
          '/CreateEvent' : (context) => CreateEvent(),
          '/CreateAvailabilitySlot' : (context) => CreateAvailabilitySlot(),
          '/AllFriends' : (context) => AllInvitedFriends(),
          '/EventDetail' : (context) => EventDetail(),
          '/FriendsAvailability' : (context) => FriendsAvailability(),
          '/EventDates' : (context) => EventDates(),
        },
        home: _defaultHome,
      )
    );
  }
}


