import 'package:country_pickers/country.dart';
import 'package:country_pickers/country_picker_cupertino.dart';
import 'package:country_pickers/utils/utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get2gether/UIComponents/AppButton.dart';
import 'package:get2gether/UIComponents/FormTextField.dart';
import 'package:get2gether/bloc/signup/signupBloc.dart';
import 'package:get2gether/bloc/signup/signup_event.dart';
import 'package:get2gether/bloc/signup/signup_state.dart';
import 'package:get2gether/models/requests/SignupRequest.dart';
import 'package:get2gether/util/AppColors.dart';
import 'package:get2gether/util/AppConstants.dart';
import 'package:get2gether/util/AppDimensions.dart';
import 'package:get2gether/util/Strings.dart';

class SignUp extends StatefulWidget{
  @override
  State<StatefulWidget> createState() {
    return SignUpState();
  }

}

class SignUpState extends State<SignUp> {

  TextEditingController _fNameController = TextEditingController();
  TextEditingController _lNameController = TextEditingController();
  TextEditingController _phoneNumberController = TextEditingController();
  TextEditingController _emailController = TextEditingController();
  TextEditingController _passwordController = TextEditingController();
  SignupBloc _signupBloc;

  Country _selectedCountry;

  @override
  void initState() {
    super.initState();
    _selectedCountry = new Country(name: "United States",isoCode: "US",iso3Code: "USA",phoneCode: "1");
    _signupBloc = BlocProvider.of<SignupBloc>(context);
    _fNameController.addListener(_onFirstNameChanged);
    _lNameController.addListener(_onLastNameChanged);
    _phoneNumberController.addListener(_onPhoneNumberChanged);
    _emailController.addListener(_onEmailChanged);
    _passwordController.addListener(_onPasswordChanged);
  }


  @override
  Widget build(BuildContext context) {
    return BlocListener<SignupBloc, SignUpBlocState>(
        listener: (context, state) {
          if (state is SignupSuccess) {
            if (state.signupResponse.profile.isPhoneVerified == 1) {
              Navigator.of(context).pushNamedAndRemoveUntil(
                  '/Home', (Route<dynamic> route) => false);
            } else {
              Navigator.of(context).pushNamed(
                  '/Onboarding', arguments: ScreenType.PhoneVerification);
            }
          }
        },
        child: BlocBuilder<SignupBloc, SignUpBlocState>(
            bloc: _signupBloc,
            builder: (context, state) {
              return SingleChildScrollView(
                child: Padding(
                  padding: EdgeInsets.fromLTRB(20.0, 40.0, 20.0, 10.0),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Image.asset('assets/images/logo_white.png', width: AppDimensions.logoWidth,),
                      Text(Strings.welcomeMessage, textAlign: TextAlign.center, style: TextStyle(color: Colors.white),),
                      SizedBox(height: 40.0,),
                      Row(

                        children: <Widget>[
                          FormTextField(Strings.hintSignUpFirstName, (MediaQuery.of(context).size.width-50)/2, textController: _fNameController,),
                          SizedBox(width: 10.0,),
                          FormTextField(Strings.hintSignUpLastName, (MediaQuery.of(context).size.width-50)/2, textController: _lNameController,),
                        ],
                      ),
                      SizedBox(height: 15.0,),
                      FormTextField(Strings.hintSignInEmail, MediaQuery.of(context).size.width, textController: _emailController,),
                      SizedBox(height: 15.0,),
                      FormTextField(Strings.hintSignInPassword, MediaQuery.of(context).size.width, textController: _passwordController, obscureText: true,),
                      SizedBox(height: 15.0,),
                      Container(
                        decoration: BoxDecoration(
                          borderRadius: const BorderRadius.all(
                            const Radius.circular(5.0),
                          ),
                          color: Colors.white,
                        ),
                        height: AppDimensions.buttonHeight(),
                        child: Row(

                          children: <Widget>[
                            Expanded(
                              child: GestureDetector(
                                child: Container(
                                    child: Center(
                                      child: _selectedCountry != null? Row(
                                        mainAxisAlignment: MainAxisAlignment.center,
                                        children: <Widget>[
                                          CountryPickerUtils.getDefaultFlagImage(_selectedCountry),
                                          SizedBox(width: 15,),
                                          Text("+"+_selectedCountry.phoneCode,style: TextStyle(fontSize: 16.0, color: AppColors.TEXT_GREY_BLUISH),)
                                        ],
                                      ):Container(),
                                    )
                                ),
                                onTap: _openCountryPicker,
                              ),
                              flex: 2,
                            ),
                            Container(
                              color: AppColors.TEXT_GREY,
                              width: 2,
                              height: double.infinity,
                            ),
                            Expanded(
                              child: Container(
                                padding: EdgeInsets.symmetric(horizontal: 20),
                                child: TextField(
                                  controller: _phoneNumberController,
                                  keyboardType: TextInputType.phone,
                                  style: TextStyle(fontSize: 16.0, color: AppColors.TEXT_GREY_BLUISH),

                                  decoration: new InputDecoration.collapsed(
                                    hintText: "Enter phone number",
                                    hintStyle: new TextStyle(color: AppColors.HINT_COLOR),
                                  ),
                                ),
                              ),
                              flex: 3,
                            ),
                          ],
                        ),
                      ),
                      SizedBox(height: 20.0,),
                      GestureDetector(
                        onTap: () => Navigator.of(context).pushNamed('/Webview'),
                        child:
                        Text(Strings.termsAndConditions, textAlign: TextAlign.center, style: TextStyle(
                            color: Colors.white,
                            fontFamily: 'Avenir'

                        )),
                      ),
                      SizedBox(height: 20.0,),
                      AppButton(Strings.signUp, _onEnterPressed, loading: state is SignupInProgress),
                      state is SignupError ? Column(
                          children: <Widget>[
                            SizedBox(height: 20.0,),
                            Center(child:
                            Text(state.error, style: TextStyle(
                                color: Colors.red,
                                fontFamily: 'Avenir'
                            )
                            )
                            )
                          ])
                          : Container(),
                      SizedBox(height: 40.0,),
                      GestureDetector(
                        child: Center(
                          child:  Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Text(Strings.alreadyHaveAnAccount, style: TextStyle(
                                  color: Colors.white,
                                  fontFamily: 'Avenir'

                              )),
                              SizedBox(width: 5.0,),
                              Text(Strings.signInText, style: TextStyle(
                                  decoration: TextDecoration.underline,
                                  color: Colors.white,
                                  fontFamily: 'Avenir'
                              )),
                            ],
                          ),

                        ),
                        onTap: () => Navigator.of(context).pushNamed('/Onboarding', arguments: ScreenType.SignIn),
                      )

                    ],
                  ),
                ),
              );
            }
        )
    );
  }

  void _openCountryPicker() => showCupertinoModalPopup<void>(
      context: context,
      builder: (BuildContext context) {
        return CountryPickerCupertino(
            pickerSheetHeight: 300.0,
            onValuePicked: (Country country) =>
                setState(() => _selectedCountry = country)
        );
      });

  void _onEmailChanged() {
//    _signInBloc.add(new EmailChanged(email: _emailController.text));
  }

  void _onPasswordChanged() {
//    _signInBloc.add(new PasswordChanged(password: _passwordController.text));
  }

  void _onFirstNameChanged() {
//    _signInBloc.add(new EmailChanged(email: _emailController.text));
  }

  void _onLastNameChanged() {
//    _signInBloc.add(new PasswordChanged(password: _passwordController.text));
  }

  void _onPhoneNumberChanged() {
//    _signInBloc.add(new PasswordChanged(password: _passwordController.text));
  }

  void _onEnterPressed(){
    FocusScope.of(context).requestFocus(FocusNode());
    SignupRequest request = new SignupRequest(_emailController.text, _passwordController.text, _fNameController.text, _lNameController.text, "+"+_selectedCountry.phoneCode, _phoneNumberController.text);
    _signupBloc.add(new FormSubmitted(request));
  }


}