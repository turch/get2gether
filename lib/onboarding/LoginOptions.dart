import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get2gether/UIComponents/OrangeBorderTransparentButton.dart';
import 'package:get2gether/UIComponents/AppButton.dart';
import 'package:get2gether/util/AppColors.dart';
import 'package:get2gether/util/AppConstants.dart';
import 'package:get2gether/util/AppDimensions.dart';
import 'package:get2gether/util/Strings.dart';

class LoginOptions extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.red,

      body: Stack(
          children: <Widget>[
            Image.asset(
              "assets/images/splash.png",
              height: MediaQuery.of(context).size.height,
              width: MediaQuery.of(context).size.width,
              fit: BoxFit.cover,
            ),
            Align(
                alignment: AlignmentDirectional.topCenter,
                child: Column(children: <Widget>[
                  Padding(padding: EdgeInsets.fromLTRB(0.0, 80.0, 0.0, 20.0),
                    child: Image.asset('assets/images/artwork.png'),
                  ),
                  Image.asset('assets/images/logo_blue.png', width: AppDimensions.logoWidth,),
                  Text(Strings.welcomeMessage, textAlign: TextAlign.center, style: TextStyle(color: AppColors.TEXT_GREY),)
                ],)
            ),
            Align(
                alignment: AlignmentDirectional.bottomCenter,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    AppButton(Strings.signIn, () {
                      Navigator.of(context).pushNamed('/Onboarding', arguments: ScreenType.SignIn);
                    }),
                    SizedBox(height: 10.0,),
                    OrangeBorderTransparentButton("Sign Up", () {
                      Navigator.of(context).pushNamed('/Onboarding', arguments: ScreenType.SignUp);
                    }),
                    SizedBox(height: 10.0,),
                    Text(Strings.needHelp, style: TextStyle(color: Colors.white, fontSize: 14.0), textAlign: TextAlign.center,),
                    SizedBox(height: 20.0,),
                  ],)
            ),
          ]),
    );
  }
}