
import 'package:countdown/countdown.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get2gether/UIComponents/AppButton.dart';
import 'package:get2gether/UIComponents/FormTextField.dart';
import 'package:get2gether/bloc/signin/signinBloc.dart';
import 'package:get2gether/bloc/signin/signin_state.dart';
import 'package:get2gether/bloc/signup/signupBloc.dart';
import 'package:get2gether/bloc/signup/signup_state.dart';
import 'package:get2gether/bloc/verifyCode/verifyCodeBloc.dart';
import 'package:get2gether/bloc/verifyCode/verifycode_event.dart';
import 'package:get2gether/bloc/verifyCode/verifycode_state.dart';
import 'package:get2gether/models/requests/ResendCodeRequest.dart';
import 'package:get2gether/models/requests/VerifyCodeRequest.dart';
import 'package:get2gether/resources/AppData.dart';
import 'package:get2gether/util/AppColors.dart';
import 'package:get2gether/util/AppDimensions.dart';
import 'package:get2gether/util/Strings.dart';

class VerifyPhoneNumber extends StatefulWidget{
  @override
  State<StatefulWidget> createState() {
    return VerifyPhoneNumberState();
  }

}

class VerifyPhoneNumberState extends State<VerifyPhoneNumber>{

  Widget _refreshButton;
  TextEditingController _verificationCodeController = TextEditingController();
  VerifyCodeBloc _verifyCodeBloc;
  SigninBloc _signinBloc;
  SignupBloc _signupBloc;
  String countryCode;
  String phoneNumber;

  @override
  void initState() {
    super.initState();
    _refreshButton = Timer(onTimerEnd);
    _verifyCodeBloc = BlocProvider.of<VerifyCodeBloc>(context);
    _signinBloc = BlocProvider.of<SigninBloc>(context);
    _signupBloc = BlocProvider.of<SignupBloc>(context);
    var userProfile = AppData.currentUser;
    countryCode = userProfile.countryCode;
    phoneNumber = userProfile.phone;
  }


  void onTimerEnd(){
    setState(() {
      _refreshButton = button();
    });
  }

  Widget button(){
    return IconButton(icon: Icon(Icons.autorenew,color: Colors.white,),
      onPressed: (){
        setState(() {
          _refreshButton = Timer(onTimerEnd);
          _resendCode();
        });
      },);
  }

  @override
  Widget build(BuildContext context) {

    return BlocListener<VerifyCodeBloc, VerifyCodeBlocState>(
        listener: (context, state) {
          if (state is VerifyCodeSuccess) {
            if (state.signinResponse.profile.isPhoneVerified == 1) {
              Navigator.of(context).pushNamedAndRemoveUntil(
                  '/Home', (Route<dynamic> route) => false);
            }
          }
        },
        child: BlocBuilder<VerifyCodeBloc, VerifyCodeBlocState>(
            bloc: _verifyCodeBloc,
            builder: (context, state) {
              return  Stack(
                children: <Widget>[
                  Container(
                      margin: EdgeInsets.only(top: 20),
                      padding: EdgeInsets.symmetric(horizontal: 20),
                      child: Column(
                        children: <Widget>[
                          Image.asset('assets/images/logo_white.png', width: AppDimensions.logoWidth,),
                          Text(Strings.welcomeMessage, textAlign: TextAlign.center, style: TextStyle(color: Colors.white),),
                          SizedBox(height: 40.0,),
                          Container(
                            width: double.infinity,
                            child: Text("Please Enter The Code Sent To $countryCode $phoneNumber",style: TextStyle(color: Colors.white,fontSize: 16),),
                          ),
                          SizedBox(height: 10,),
                          Container(
                            width: double.infinity,
                            height: AppDimensions.buttonHeight(),
                            child: Row(
                              children: <Widget>[
                                Expanded(
                                  child: Container(
                                    child: FormTextField(Strings.hintCode, (MediaQuery.of(context).size.width)/2, textController: _verificationCodeController,),
                                  ),
                                ),
                                Container(
                                  decoration: BoxDecoration(
                                      border: Border.all(color: AppColors.TEXT_GREY),
                                      borderRadius: BorderRadius.all(Radius.circular(5)),
                                      color: AppColors.ORANGE
                                  ),
                                  height: double.infinity,
                                  width: 50,
                                  child: Center(
                                    child: _refreshButton,
                                  ),
                                )
                              ],
                            ),
                          ),
                          SizedBox(height: 20,),
                          AppButton(Strings.confirm, _onEnterPressed, loading: state is VerifyCodeInProgress,),
                          state is VerifyCodeError ? Column(
                              children: <Widget>[
                                SizedBox(height: 20.0,),
                                Center(child:
                                Text(state.error, style: TextStyle(
                                    color: Colors.red,
                                    fontFamily: 'Avenir'
                                )
                                )
                                )
                              ])
                              : Container(),
                        ],
                      )
                  ),
                ],
              );

            }
        )
    );
  }

  void _onEnterPressed(){
    FocusScope.of(context).requestFocus(FocusNode());
    _verifyCodeBloc.add(new FormSubmitted(new VerifyCodeRequest( "123456", "a", "123456", countryCode, phoneNumber, _verificationCodeController.text)));
  }

  void _resendCode(){
    _verifyCodeBloc.add(new ResendCode(new ResendCodeRequest(countryCode, phoneNumber)));
  }

}

class Timer extends StatefulWidget{
  Function onFinish;
  Timer(this.onFinish);
  @override
  State<StatefulWidget> createState() {
    return TimerState();
  }
}

class TimerState extends State<Timer>{

  CountDown cd = CountDown(Duration(seconds: 10));
  int time = 0;
  @override
  void initState() {
    var sub = cd.stream.listen(null);
    sub.onData((Duration d){
      if (!mounted) sub.cancel();
      if (!mounted) return;
      setState(() {
        time = d.inSeconds;
      });
    });
    sub.onDone(() {
      widget.onFinish();
    });
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Text(time.toString(),style: TextStyle(color: Colors.white),),
    );
  }
}