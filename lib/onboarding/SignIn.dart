import 'dart:io';

import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get2gether/UIComponents/AppButton.dart';
import 'package:get2gether/UIComponents/FormTextField.dart';
import 'package:get2gether/bloc/signin/signinBloc.dart';
import 'package:get2gether/bloc/signin/signin_event.dart';
import 'package:get2gether/bloc/signin/signin_state.dart';
import 'package:get2gether/models/requests/SigninRequest.dart';
import 'package:get2gether/util/AppConstants.dart';
import 'package:get2gether/util/AppDimensions.dart';
import 'package:get2gether/util/SharedPrefHelper.dart';
import 'package:get2gether/util/Strings.dart';

class SignIn extends StatefulWidget{
  @override
  State<StatefulWidget> createState() {
    return SignInState();
  }

}

class SignInState extends State<SignIn> {
  TextEditingController _emailController = TextEditingController();
  TextEditingController _passwordController = TextEditingController();
  SigninBloc _signInBloc;
  String fcmToken;
  String deviceType;

  @override
  void initState() {

    _signInBloc = BlocProvider.of<SigninBloc>(context);
    _emailController.addListener(_onEmailChanged);
    _passwordController.addListener(_onPasswordChanged);
    final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();
    _firebaseMessaging.getToken().then((token) {
      fcmToken = token;
    });
    deviceType = Platform.isIOS ? "i" : "a";
    super.initState();
  }


  @override
  Widget build(BuildContext context) {

    return BlocListener<SigninBloc, SignInBlocState>(
        listener: (context, state) {
          if (state is SigninSuccess) {
            if (state.signinResponse.profile.isPhoneVerified == 1) {
              Navigator.of(context).pushNamedAndRemoveUntil(
                  '/Home', (Route<dynamic> route) => false);
              SharedPreferencesHelper.setValue(SharedPreferenceKeys.USER_LOGGED_IN, true);
              SharedPreferencesHelper.save(SharedPreferenceKeys.USER, state.signinResponse.profile);
            } else {
              Navigator.of(context).pushNamed(
                  '/Onboarding', arguments: ScreenType.PhoneVerification);
            }
          }
        },
        child: BlocBuilder<SigninBloc, SignInBlocState>(
            bloc: _signInBloc,
            builder: (context, state) {
              return  SingleChildScrollView(
                child: Padding(
                  padding: EdgeInsets.fromLTRB(20.0, 40.0, 20.0, 10.0),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Image.asset('assets/images/logo_white.png', width: AppDimensions.logoWidth,),
                      Text(Strings.welcomeMessage, textAlign: TextAlign.center, style: TextStyle(color: Colors.white),),
                      SizedBox(height: 40.0,),
                      FormTextField(Strings.hintSignInEmail, MediaQuery.of(context).size.width, textController: _emailController, ),
                      SizedBox(height: 15.0,),
                      FormTextField(Strings.hintSignInPassword, MediaQuery.of(context).size.width, textController: _passwordController, obscureText: true,),
                      SizedBox(height: 20.0,),
                      Center(
                        child: GestureDetector(
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Text(Strings.forgotYourPassword, style: TextStyle(
                                  color: Colors.white,
                                  fontFamily: 'Avenir'

                              )),
                              SizedBox(width: 5.0,),
                              Text(Strings.resetNow, style: TextStyle(
                                  decoration: TextDecoration.underline,
                                  color: Colors.white,
                                  fontFamily: 'Avenir'
                              )),
                            ],
                          ),

                          onTap: () => Navigator.of(context).pushNamed('/Onboarding', arguments: ScreenType.ForgotPassword),
                        ),
                      ),
                      SizedBox(height: 20.0,),
                      AppButton(Strings.signIn, () {
                        _onEnterPressed();
                      }, loading: state is SigninInProgress),
                      state is SigninError ? Column(
                          children: <Widget>[
                            SizedBox(height: 20.0,),
                            Center(child:
                            Text(state.error, style: TextStyle(
                                color: Colors.red,
                                fontFamily: 'Avenir'
                            )
                            )

                            )
                          ])
                          : Container(),
                      SizedBox(height: 40.0,),
                      GestureDetector(
                          child: Center(
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Text(Strings.dontHaveAnAccount, style: TextStyle(
                                      color: Colors.white,
                                      fontFamily: 'Avenir'

                                  )),
                                  SizedBox(width: 5.0,),
                                  Text(Strings.signUpText, style: TextStyle(
                                      decoration: TextDecoration.underline,
                                      color: Colors.white,
                                      fontFamily: 'Avenir'
                                  )),
                                ],
                              )
                          ),
                          onTap: () => Navigator.of(context).pushNamed('/Onboarding', arguments: ScreenType.SignUp)
                      ),

                    ],
                  ),
                ),
              );

            }
        )
    );


  }

  void _onEmailChanged() {
//    _signInBloc.add(new EmailChanged(email: _emailController.text));
  }

  void _onPasswordChanged() {
//    _signInBloc.add(new PasswordChanged(password: _passwordController.text));
  }


  void _onEnterPressed() {
    FocusScope.of(context).requestFocus(FocusNode());
    _signInBloc.add(new FormSubmitted(new SigninRequest(_emailController.text, _passwordController.text, "123456", deviceType, fcmToken)));
  }

  @override
  void dispose() {
    _emailController.dispose();
    _passwordController.dispose();
    super.dispose();
  }
}
