
import 'package:countdown/countdown.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get2gether/UIComponents/FormTextField.dart';
import 'package:get2gether/UIComponents/AppButton.dart';
import 'package:get2gether/bloc/forgotPassword/forgotPasswordBloc.dart';
import 'package:get2gether/bloc/forgotPassword/forgotpassword_event.dart';
import 'package:get2gether/bloc/forgotPassword/forgotpassword_state.dart';
import 'package:get2gether/models/requests/ForgotPasswordRequest.dart';
import 'package:get2gether/util/AppColors.dart';
import 'package:get2gether/util/AppDimensions.dart';
import 'package:get2gether/util/Strings.dart';

class ForgotPassword extends StatefulWidget{
  @override
  State<StatefulWidget> createState() {
    return ForgotPasswordState();
  }

}

class ForgotPasswordState extends State<ForgotPassword>{

  TextEditingController _emailController = TextEditingController();
  ForgotPasswordBloc _forgotPasswordBloc;
  @override
  void initState() {
    super.initState();
    _forgotPasswordBloc = BlocProvider.of<ForgotPasswordBloc>(context);
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<ForgotPasswordBloc, ForgotPasswordBlocState>(
        listener: (context, state) {
          if (state is ForgotPasswordSuccess) {

          }
        },
        child: BlocBuilder<ForgotPasswordBloc, ForgotPasswordBlocState>(
            bloc: _forgotPasswordBloc,
            builder: (context, state) {
              return  Stack(
                children: <Widget>[
                  Container(
                      margin: EdgeInsets.only(top: 20),
                      padding: EdgeInsets.symmetric(horizontal: 20),
                      child: Column(
                        children: <Widget>[
                          Image.asset('assets/images/logo_white.png', width: AppDimensions.logoWidth,),
                          Text(Strings.welcomeMessage, textAlign: TextAlign.center, style: TextStyle(color: Colors.white),),
                          SizedBox(height: 40.0,),
                          Container(
                            width: double.infinity,
                            child: Text("Please enter your email address ",style: TextStyle(color: Colors.white,fontSize: 16),),
                          ),
                          SizedBox(height: 10,),
                          Container(
                            width: double.infinity,
                            height: AppDimensions.buttonHeight(),
                            child: FormTextField(Strings.hintSignInEmail, (MediaQuery.of(context).size.width)/2, textController: _emailController,),

                          ),
                          SizedBox(height: 20,),
                          AppButton(Strings.confirm, _onEnterPressed, loading: state is ForgotPasswordInProgress,),
                          state is ForgotPasswordError ? Column(
                              children: <Widget>[
                                SizedBox(height: 20.0,),
                                Center(child:
                                Text(state.error, style: TextStyle(
                                    color: Colors.red,
                                    fontFamily: 'Avenir'
                                )
                                )
                                )
                              ])
                              : Container(),
                          state is ForgotPasswordSuccess ? Column(
                              children: <Widget>[
                                SizedBox(height: 20.0,),
                                Center(child:
                                Text("A password reset link sent to your email successfully. Please reset your password and try logging in again.", style: TextStyle(
                                    color: AppColors.ORANGE,
                                    fontFamily: 'Avenir'
                                )
                                )
                                )
                              ])
                              : Container(),
                        ],
                      )
                  ),
                ],
              );

            }
        )
    );


  }
  void _onEnterPressed(){
    FocusScope.of(context).requestFocus(FocusNode());
    _forgotPasswordBloc.add(new FormSubmitted(new ForgotPasswordRequest(_emailController.text)));
  }
}
