import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get2gether/bloc/forgotPassword/forgotPasswordBloc.dart';
import 'package:get2gether/bloc/signin/signinBloc.dart';
import 'package:get2gether/bloc/signup/signupBloc.dart';
import 'package:get2gether/bloc/userEvents/userEventsBloc.dart';
import 'package:get2gether/bloc/verifyCode/verifyCodeBloc.dart';
import 'package:get2gether/onboarding/SignIn.dart';
import 'package:get2gether/onboarding/SignUp.dart';
import 'package:get2gether/onboarding/VerifyPhoneNumber.dart';
import 'package:get2gether/util/AppColors.dart';
import 'package:get2gether/util/AppConstants.dart';
import 'package:get2gether/util/AppDimensions.dart';
import 'package:get2gether/util/Strings.dart';

import 'ForgotPassword.dart';

class OnboardingContainer extends StatefulWidget {

  @override
  OnboardingContainerState createState() => OnboardingContainerState();

}

class OnboardingContainerState extends State<OnboardingContainer> {


  @override
  Widget build(BuildContext context) {
    ScreenType viewToLoad = ModalRoute.of(context).settings.arguments;
    String viewTitle = getTitle(viewToLoad);
    double height = AppDimensions.onboardingContainerHeight(MediaQuery.of(context).size);
    return MultiBlocProvider(
      providers: [
        BlocProvider<SigninBloc>(
          create: (BuildContext context) => SigninBloc(),
        ),
        BlocProvider<SignupBloc>(
          create: (BuildContext context) => SignupBloc(),
        ),
        BlocProvider<VerifyCodeBloc>(
          create: (BuildContext context) => VerifyCodeBloc(),
        ),
        BlocProvider<ForgotPasswordBloc>(
          create: (BuildContext context) => ForgotPasswordBloc(),
        ),
        BlocProvider<UserEventsBloc>(
          create: (BuildContext context) => UserEventsBloc(),
        ),
      ],
      child: Scaffold(
        body: Stack(
          children: <Widget>[
            Align(
              alignment: AlignmentDirectional.topCenter,
              child: new AppBar(
                centerTitle: true, // this is all you need
                backgroundColor: Colors.white,
                title: Text(viewTitle, textAlign: TextAlign.center, style: TextStyle(fontFamily: 'Avenir', color: AppColors.TEXT_BLACK, fontSize: AppDimensions.appBarTitleTextSize),),
                leading: new IconButton(
                    icon: new Icon(Icons.arrow_back, color: AppColors.TEXT_BLACK,),
                    onPressed: () {
                      Navigator.pop(context, true);
                    }
                ),
              ),
            ),
            AnimatedContainer(
              duration: const Duration(seconds: 1),
              margin: EdgeInsets.fromLTRB(0.0, 120.0, 0.0, 0.0),
              color: Colors.transparent,
              alignment: AlignmentDirectional.bottomEnd,
              child: new Container(
                decoration: new BoxDecoration(
                    color: AppColors.CYAN,
                    borderRadius: new BorderRadius.only(
                        topLeft: const Radius.circular(30.0),
                        topRight: const Radius.circular(30.0))),
                child: getViewToLoad(viewToLoad),
                height: height,
                width: MediaQuery.of(context).size.width,
              ),
            ),
          ],
        ),


      ),
    );
  }

  Widget getViewToLoad(ScreenType viewToLoad) {
    switch(viewToLoad) {
      case ScreenType.SignUp:
        return SignUp();
      case ScreenType.SignIn:
        return SignIn();
      case ScreenType.PhoneVerification:
        return VerifyPhoneNumber();
        case ScreenType.ForgotPassword:
          return ForgotPassword();

    }
  }

  String getTitle(ScreenType viewToLoad) {
    switch(viewToLoad) {
      case ScreenType.SignUp:
        return Strings.signUp;
      case ScreenType.SignIn:
        return Strings.signIn;
      case ScreenType.PhoneVerification:
        return Strings.titlePhoneVerification;
      case ScreenType.ForgotPassword:
        return Strings.titleForgotPassword;


    }
  }

}
