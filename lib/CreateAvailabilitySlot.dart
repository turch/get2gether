import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get2gether/UIComponents/AppButton.dart';
import 'package:get2gether/bloc/createAvailabilitySlot/createAvailabilitySlotBloc.dart';
import 'package:get2gether/bloc/createAvailabilitySlot/createavailabilityslot_event.dart';
import 'package:get2gether/bloc/availabilitySlotsByFilter/AvailabilitySlotsByFilterBloc.dart';
import 'package:get2gether/bloc/availabilitySlotsByFilter/availabilityslotsbyfilter_event.dart';
import 'package:get2gether/resources/AppData.dart';
import 'package:get2gether/util/AppColors.dart';
import 'package:intl/intl.dart';

import 'UIComponents/FormTextControlWithAction.dart';
import 'bloc/createAvailabilitySlot/createavailabilityslot_state.dart';
import 'models/AvailabilitySlot.dart';
import 'models/requests/CreateAvailabilitySlotRequest.dart';
import 'models/requests/EditAvailabilitySlotRequest.dart';
import 'models/requests/AvailabilitySlotsRequestByFilter.dart';


class CreateAvailabilitySlot extends StatefulWidget {
  @override
  _CreateAvailabilitySlotState createState() => _CreateAvailabilitySlotState();
}

class _CreateAvailabilitySlotState extends State<CreateAvailabilitySlot>{
  DateTime date, startTime, endTime;
  CreateAvailabilitySlotBloc _createAvailabilitySlotBloc;
  AvailabilitySlotsByFilterBloc _availabilitySlotsByFilterBloc;
  TextEditingController _dateController = TextEditingController();
  TextEditingController _startTimeController = TextEditingController();
  TextEditingController _endTimeController = TextEditingController();
  AvailabilitySlot _availabilitySlotModel;

  @override
  void initState() {
    super.initState();
    _availabilitySlotsByFilterBloc = BlocProvider.of<AvailabilitySlotsByFilterBloc>(context);
  }

  @override
  void dispose() {
    super.dispose();
  }

  Future<bool> _onWillPop() async {
    if(_availabilitySlotModel != null) {
      Navigator.pop(context, _availabilitySlotModel);
    } else {
      Navigator.pop(context, false);
    }
    return false;
  }

  Widget build(BuildContext context) {
    final arguments = ModalRoute.of(context).settings.arguments;

    if (arguments is DateTime) {
      date = arguments;
    }

    if(_availabilitySlotModel == null && arguments is AvailabilitySlot) {
      _availabilitySlotModel = ModalRoute.of(context).settings.arguments;
    }

    if(_availabilitySlotModel != null) {
      startTime = DateTime.parse("1996-07-20 ${_availabilitySlotModel.startTime}");
      endTime = DateTime.parse("1996-07-20 ${_availabilitySlotModel.endTime}");
      date = DateTime.parse(_availabilitySlotModel.date);
    }

    return new WillPopScope(
        onWillPop: _onWillPop,
        child: BlocProvider<CreateAvailabilitySlotBloc>(
            create: (BuildContext context) {
              _createAvailabilitySlotBloc = CreateAvailabilitySlotBloc();
              return _createAvailabilitySlotBloc;
            },
            child: BlocListener<CreateAvailabilitySlotBloc, CreateAvailabilitySlotBlocState>(
                listener: (context, state) {
                  String dateString;
                  DateTime date;

                  if (state is CreateAvailabilitySlotSuccess) {
                    dateString = state.createAvailabilitySlotResponse.availabilitySlot.date;
                    date = DateTime.parse(dateString);

                    Navigator.pop(context, false);
                  } else if(state is EditAvailabilitySlotSuccess) {
                    dateString = state.editAvailabilitySlotResponse.availabilitySlot.date;
                    date = DateTime.parse(dateString);

                    Navigator.pop(context, state.editAvailabilitySlotResponse.availabilitySlot);
                  }

                  if (date != null) {
                    _availabilitySlotsByFilterBloc.add(
                        new FetchAvailabilitySlotsByMonth(
                            new AvailabilitySlotsRequestByFilter(
                                "month", date.month.toString(),
                                date.year.toString())));
                    _availabilitySlotsByFilterBloc.add(
                        new FetchAvailabilitySlotsByDate(
                            new AvailabilitySlotsRequestByFilter(
                                "date", dateString, '')));
                  }
                },
                child: BlocBuilder<CreateAvailabilitySlotBloc, CreateAvailabilitySlotBlocState>(
                    bloc: _createAvailabilitySlotBloc,
                    builder: (context, state) {
                      return Scaffold(
                          backgroundColor: AppColors.MAIN_BG_GREY,
                          appBar: AppBar(
                              elevation: 1.0,
                              backgroundColor: AppColors.CYAN,
                              centerTitle: true,
                              title: Text(
                                _availabilitySlotModel != null ? "Update Availability" : "Set Availability", style: TextStyle(color: Colors.white),),
                              leading: IconButton(
                                icon: Icon(Icons.arrow_back, color: Colors.white,),
                                onPressed: _onWillPop,
                              )
                          ),
                          body: SingleChildScrollView(
                              padding: EdgeInsets.all(20.0),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  //Section1
                                  Text("Choose time and date of your availability",
                                      style: TextStyle(color: AppColors.TEXT_BLACK,
                                          fontSize: 23.0,
                                          fontFamily: 'Avenir'),textAlign: TextAlign.center),
                                  SizedBox(height: 15.0,),
                                  Text("Choose date", style: TextStyle(
                                      color: AppColors.TEXT_GREY_BLUISH,
                                      fontSize: 18.0,
                                      fontFamily: 'Avenir'),),
                                  SizedBox(height: 10.0,),
                                  FormTextControlWithAction("", MediaQuery
                                      .of(context)
                                      .size
                                      .width, 'assets/images/calendar.png',
                                    text: date != null ? getAvailabilitySlotDate() : "Pick Date",
                                    action: ACTION_TYPE.date, textController: _dateController, enabled: false, getDateTimeObjectOnSelection: (dateTime){
                                        date = dateTime;
                                      }),
                                  SizedBox(height: 10.0,),
                                  Text("Choose start time", style: TextStyle(
                                      color: AppColors.TEXT_GREY_BLUISH,
                                      fontSize: 18.0,
                                      fontFamily: 'Avenir'),),
                                  SizedBox(height: 10.0,),
                                  FormTextControlWithAction("", MediaQuery
                                      .of(context)
                                      .size
                                      .width, 'assets/images/clock.png',
                                      text: _availabilitySlotModel != null? getAvailabilitySlotStartTime() : "Pick Time",
                                      action: ACTION_TYPE.time, textController: _startTimeController, enabled: false, getDateTimeObjectOnSelection: (dateTime){
                                        startTime = dateTime;
                                      }),
                                  SizedBox(height: 10.0,),
                                  Text("Choose end time", style: TextStyle(
                                      color: AppColors.TEXT_GREY_BLUISH,
                                      fontSize: 18.0,
                                      fontFamily: 'Avenir'),),
                                  SizedBox(height: 10.0,),
                                  FormTextControlWithAction("", MediaQuery
                                      .of(context)
                                      .size
                                      .width, 'assets/images/clock.png',
                                      text: _availabilitySlotModel != null? getAvailabilitySlotEndTime() : "Pick Time",
                                      action: ACTION_TYPE.time, textController: _endTimeController, enabled: false, getDateTimeObjectOnSelection: (dateTime){
                                        endTime = dateTime;
                                      }),
                                  SizedBox(height: 10.0,),

                                  state is CreateAvailabilitySlotError ? Column(
                                      children: <Widget>[
                                        SizedBox(height: 20.0,),
                                        Center(child:
                                        Text(state.error, style: TextStyle(
                                            color: Colors.red,
                                            fontFamily: 'Avenir'
                                        )
                                        )

                                        )
                                      ])
                                      : Container(),
                                  AppButton(_availabilitySlotModel != null? "UPDATE AVAILABILITY" : "ADD AVAILABILITY", _availabilitySlotModel != null ? _availabilitySlotUpdatePressed : _availabilitySlotCreatePressed, color: AppColors.CYAN, loading: state is CreateAvailabilitySlotInProgress,),


                                ],
                              )
                          )
                      );
                    }
                )
            )
        )
    );
  }

  String getAvailabilitySlotDate() {
    var formatter = DateFormat("yyyy-MM-dd");
    return formatter.format(date);
  }

  String getAvailabilitySlotStartTime() {
    var formatter = DateFormat('HH:mm');
    return formatter.format(startTime);
  }

  String getAvailabilitySlotEndTime() {
    var formatter = DateFormat('HH:mm');
    return formatter.format(endTime);
  }

  void _availabilitySlotUpdatePressed() {
    FocusScope.of(context).requestFocus(FocusNode());

    String dateString = getAvailabilitySlotDate();
    String startTimeString = getAvailabilitySlotStartTime();
    String endTimeString = getAvailabilitySlotEndTime();

    var requestObj = new EditAvailabilitySlotRequest(_availabilitySlotModel.id, dateString, startTimeString, endTimeString);
    var eventObj = new EditAvailabilitySlotFormSubmitted(requestObj);
    _createAvailabilitySlotBloc.add(eventObj);
  }

  void _availabilitySlotCreatePressed() {
    if(date == null) {
      showErrorMessage("Fields Missing", "Please pick date");
      return;
    }
    String dateString = getAvailabilitySlotDate();

    if(startTime == null) {
      showErrorMessage("Fields Missing", "Please pick start time");
      return;
    }
    String startTimeString = getAvailabilitySlotStartTime();

    if(endTime == null) {
      showErrorMessage("Fields Missing", "Please pick end time");
      return;
    }
    String endTimeString = getAvailabilitySlotEndTime();

    if(endTime.isBefore(startTime)) {
      showErrorMessage("Invalid Fields", "Start time must be before end time");
      return;
    }

    FocusScope.of(context).requestFocus(FocusNode());
    var requestObj = new CreateAvailabilitySlotRequest(AppData.currentUser.id, dateString, startTimeString, endTimeString);
    var eventObj = new CreateAvailabilitySlotFormSubmitted(requestObj);
    _createAvailabilitySlotBloc.add(eventObj);
  }

  showErrorMessage(String title, String error) {
    showDialog(
      context: context,
      builder: (context) =>
          AlertDialog(
            content: ListTile(
              title: Text(title),
              subtitle: Text(error),
            ),
            actions: <Widget>[
              FlatButton(
                  child: Text('Ok'),
                  onPressed: () {
                    Navigator.of(context).pop();
                  }
              ),
            ],
          ),
    );
  }
}