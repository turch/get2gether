import 'package:equatable/equatable.dart';
import 'package:get2gether/models/requests/NotificationsRequest.dart';

class NotificationsEvent extends Equatable {

  @override
  List<Object> get props => [];
}

class FetchNotifications extends NotificationsEvent {
  NotificationsRequest _formData;
  FetchNotifications(this._formData) ;

  NotificationsRequest get formData => _formData;

}


