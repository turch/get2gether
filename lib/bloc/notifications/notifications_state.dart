import 'package:equatable/equatable.dart';
import 'package:get2gether/models/NotificationModel.dart';

abstract class NotificationsBlocState extends Equatable {
  NotificationsBlocState() : super();
}

class NotificationsUninitialized extends NotificationsBlocState {
  @override
  String toString() => 'NotificationsUninitialized';

  @override
  List<Object> get props => null;

}

class NotificationsError extends NotificationsBlocState {
  String _error;


  NotificationsError(this._error);


  String get error => _error;

  @override
  String toString() => 'PostError';

  @override
  List<Object> get props => null;
}

class NotificationsLoaded extends NotificationsBlocState {
  List<NotificationModel> _notifications;
  final bool _hasReachedMax;

  NotificationsLoaded(this._notifications, this._hasReachedMax,) : super();

  NotificationsLoaded copyWith({List<NotificationModel> notifications, bool hasReachedMax,}) {
    return NotificationsLoaded(notifications ?? this._notifications, hasReachedMax ?? this._hasReachedMax);
  }

  bool get hasReachedMax => _hasReachedMax;

  List<NotificationModel> get notifications => _notifications;


  set notifications(List<NotificationModel> value) {
    _notifications = value;
  }

  @override
  String toString() =>
      'PostLoaded { posts: ${notifications.length}, hasReachedMax: $hasReachedMax }';

  @override
  List<Object> get props => null;



}