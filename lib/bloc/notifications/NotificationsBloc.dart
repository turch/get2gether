
import 'package:bloc/bloc.dart';
import 'package:get2gether/models/requests/NotificationsRequest.dart';
import 'package:get2gether/resources/Repository.dart';
import 'package:rxdart/rxdart.dart';

import 'notifications_event.dart';
import 'notifications_state.dart';


class NotificationsBloc extends Bloc<NotificationsEvent, NotificationsBlocState> {

  final _repository = Repository();

  @override
  NotificationsBlocState get initialState => NotificationsUninitialized();


  @override
  Stream<NotificationsBlocState> transformEvents(Stream<NotificationsEvent> events,
      Stream<NotificationsBlocState> Function(NotificationsEvent event) next,) {
    return super.transformEvents(
      (events as Observable<NotificationsEvent>).debounceTime(
        Duration(milliseconds: 500),
      ),
      next,
    );
  }

  @override
  void onTransition(Transition<NotificationsEvent, NotificationsBlocState> transition) {
    print(transition);
  }

  @override
  Stream<NotificationsBlocState> mapEventToState(NotificationsEvent event) async* {
    if (event != null) {
      if (event is FetchNotifications && !_hasReachedMax(state)) {
        try {
          if (state is NotificationsUninitialized) {
            NotificationsResponse response = await _repository.fetNotifications(event.formData);
            yield NotificationsLoaded(response.notifications, false);
            return;
          }
          if (state is NotificationsLoaded) {
            NotificationsLoaded currentState = state;
            if(event.formData.pageNumber == 1) {
              currentState.notifications = new List();
              yield NotificationsUninitialized();

            }

            NotificationsResponse response = await  _repository.fetNotifications(event.formData);
            yield response.notifications.isEmpty ? currentState.copyWith(notifications: currentState.notifications, hasReachedMax: true)
                : NotificationsLoaded(currentState.notifications + response.notifications, false);
          }
        } catch (_) {
          yield NotificationsError("Something went wrong");
        }
      }
    }
  }
  bool _hasReachedMax(NotificationsBlocState state) =>
      state is NotificationsLoaded && state.hasReachedMax;


}
