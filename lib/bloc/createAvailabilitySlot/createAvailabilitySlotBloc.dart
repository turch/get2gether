import 'package:bloc/bloc.dart';
import 'package:get2gether/models/requests/CreateAvailabilitySlotRequest.dart';
import 'package:get2gether/models/requests/EditAvailabilitySlotRequest.dart';
import 'package:get2gether/resources/AppData.dart';
import 'package:get2gether/resources/Repository.dart';
import 'package:rxdart/rxdart.dart';

import 'createavailabilityslot_event.dart';
import 'createavailabilityslot_state.dart';


class CreateAvailabilitySlotBloc extends Bloc<CreateAvailabilitySlotEvent, CreateAvailabilitySlotBlocState> {
  final _repository = Repository();

  @override
  CreateAvailabilitySlotBlocState get initialState => CreateAvailabilitySlotUninitialized();

  @override
  Stream<CreateAvailabilitySlotBlocState> transformEvents(Stream<CreateAvailabilitySlotEvent> events,
      Stream<CreateAvailabilitySlotBlocState> Function(CreateAvailabilitySlotEvent event) next,) {
    return super.transformEvents(
      (events as Observable<CreateAvailabilitySlotEvent>).debounceTime(
        Duration(milliseconds: 500),
      ),
      next,
    );
  }

  @override
  void onTransition(Transition<CreateAvailabilitySlotEvent, CreateAvailabilitySlotBlocState> transition) {
    print(transition);
  }

  @override
  Stream<CreateAvailabilitySlotBlocState> mapEventToState(CreateAvailabilitySlotEvent event) async* {
    if (event != null) {
      if(event is CreateAvailabilitySlotFormSubmitted) {
        String error = _isCreateRequestValid(event.formData);
        if(error != null) {
          yield CreateAvailabilitySlotError(error);
        }

        if(state is CreateAvailabilitySlotUninitialized || state is CreateAvailabilitySlotError) {
          yield CreateAvailabilitySlotInProgress();
          CreateAvailabilitySlotResponse response = await _repository.createAvailabilitySlot(event.formData);
          if (response.status == 1) {
            AppData.currentUserAvailabilitySlots.addAvailabilitySlot(response.availabilitySlot);
            yield CreateAvailabilitySlotSuccess(createAvailabilitySlotResponse: response);
          }
          else {
            yield CreateAvailabilitySlotError(response.message.toString());
          }
        }
      }
      else if(event is EditAvailabilitySlotFormSubmitted) {
        String error = _isEditRequestValid(event.formData);
        if(error != null) {
          yield CreateAvailabilitySlotError(error);
        }

        if(state is CreateAvailabilitySlotUninitialized || state is CreateAvailabilitySlotError) {
          yield CreateAvailabilitySlotInProgress();
          EditAvailabilitySlotResponse response = await _repository.editAvailabilitySlot(event.formData);
          if (response.status == 1) {
            yield EditAvailabilitySlotSuccess(editAvailabilitySlotResponse: response);
          }
          else {
            yield CreateAvailabilitySlotError(response.message.toString());
          }
        }
      }
    }
  }

  String _isCreateRequestValid(CreateAvailabilitySlotRequest request) {
    if(request.date.isEmpty) {
      return "Please provide valid Date.";
    } else if(request.startTime.isEmpty) {
      return "Please provide valid Start Time.";
    } else if(request.endTime.isEmpty) {
      return "Please provide valid End Time.";
    }
    return null;
  }

  String _isEditRequestValid(EditAvailabilitySlotRequest request) {
    if(request.startTime.isEmpty) {
      return "Please provide valid StartTime.";
    } else if(request.endTime.isEmpty) {
      return "Please provide valid End Time.";
    }
    return null;
  }
}
