import 'package:equatable/equatable.dart';
import 'package:get2gether/models/requests/CreateAvailabilitySlotRequest.dart';
import 'package:get2gether/models/requests/EditAvailabilitySlotRequest.dart';

class CreateAvailabilitySlotEvent extends Equatable {

  @override
  List<Object> get props => [];
}

class CreateAvailabilitySlotFormSubmitted extends CreateAvailabilitySlotEvent {
  CreateAvailabilitySlotRequest _formData;

  CreateAvailabilitySlotFormSubmitted(this._formData) ;

  CreateAvailabilitySlotRequest get formData => _formData;
}

class EditAvailabilitySlotFormSubmitted extends CreateAvailabilitySlotEvent {
  EditAvailabilitySlotRequest _formData;

  EditAvailabilitySlotFormSubmitted(this._formData) ;

  EditAvailabilitySlotRequest get formData => _formData;
}

class FormReset extends CreateAvailabilitySlotEvent {}
