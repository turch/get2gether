import 'package:equatable/equatable.dart';
import 'package:get2gether/models/requests/CreateAvailabilitySlotRequest.dart';
import 'package:get2gether/models/requests/EditAvailabilitySlotRequest.dart';
import 'package:get2gether/models/requests/SigninRequest.dart';

abstract class CreateAvailabilitySlotBlocState extends Equatable {
  const CreateAvailabilitySlotBlocState():super();

  @override
  List<Object> get props => [];
}

class CreateAvailabilitySlotUninitialized extends CreateAvailabilitySlotBlocState {}
class CreateAvailabilitySlotInProgress extends CreateAvailabilitySlotBlocState {}
class CreateAvailabilitySlotError extends CreateAvailabilitySlotBlocState {
  final String _error;
  CreateAvailabilitySlotError(this._error);

  String get error => _error;
}


class CreateAvailabilitySlotSuccess extends CreateAvailabilitySlotBlocState {
  final CreateAvailabilitySlotResponse createAvailabilitySlotResponse;

  const CreateAvailabilitySlotSuccess({
    this.createAvailabilitySlotResponse,
  });

  CreateAvailabilitySlotSuccess copyWith({
    SigninResponse response,
  }) {
    return CreateAvailabilitySlotSuccess(
      createAvailabilitySlotResponse: response ?? this.createAvailabilitySlotResponse,
    );
  }

  @override
  List<Object> get props => [createAvailabilitySlotResponse];

  @override
  String toString() =>
      'Response Loaded { response: ${createAvailabilitySlotResponse} }';
}
class EditAvailabilitySlotSuccess extends CreateAvailabilitySlotBlocState {
  final EditAvailabilitySlotResponse editAvailabilitySlotResponse;

  const EditAvailabilitySlotSuccess({
    this.editAvailabilitySlotResponse,
  });

  EditAvailabilitySlotSuccess copyWith({
    SigninResponse response,
  }) {
    return EditAvailabilitySlotSuccess(
      editAvailabilitySlotResponse: response ?? this.editAvailabilitySlotResponse,
    );
  }

  @override
  List<Object> get props => [editAvailabilitySlotResponse];

  @override
  String toString() =>
      'Response Loaded { response: ${editAvailabilitySlotResponse} }';
}
