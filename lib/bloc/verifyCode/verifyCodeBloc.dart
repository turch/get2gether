
import 'package:bloc/bloc.dart';
import 'package:get2gether/bloc/verifyCode/verifycode_event.dart';
import 'package:get2gether/bloc/verifyCode/verifycode_state.dart';
import 'package:get2gether/models/GeneralResponse.dart';
import 'package:get2gether/models/requests/SigninRequest.dart';
import 'package:get2gether/resources/Repository.dart';
import 'package:get2gether/resources/AppData.dart';
import 'package:rxdart/rxdart.dart';


class VerifyCodeBloc extends Bloc<VerifyCodeEvent, VerifyCodeBlocState> {

  final _repository = Repository();

  @override
  VerifyCodeBlocState get initialState => VerifyCodeUninitialized();


  @override
  Stream<VerifyCodeBlocState> transformEvents(Stream<VerifyCodeEvent> events,
      Stream<VerifyCodeBlocState> Function(VerifyCodeEvent event) next,) {
    return super.transformEvents(
      (events as Observable<VerifyCodeEvent>).debounceTime(
        Duration(milliseconds: 500),
      ),
      next,
    );
  }

  @override
  void onTransition(Transition<VerifyCodeEvent, VerifyCodeBlocState> transition) {
    print(transition);
  }

  @override
  Stream<VerifyCodeBlocState> mapEventToState(VerifyCodeEvent event) async* {
    if (event != null) {
      if(event is FormSubmitted) {
        if( !_isCodeValid(event.formData.verificationCode)){
          yield VerifyCodeError("Please enter valide code.");
        }

        else if(state is VerifyCodeUninitialized || state is VerifyCodeError) {
          yield VerifyCodeInProgress();
          SigninResponse response = await _repository.verifyCode(event.formData);
          if (response.status == 1) {
            AppData.currentUser = response.profile;
            AppData.currentUserEvents = response.events;
            yield VerifyCodeSuccess(signinResponse: response);
          }
          else {
            yield VerifyCodeError(response.message.toString());
          }
        }
      }
      else if(event is ResendCode) {
        yield VerifyCodeUninitialized();
        GeneralResponse response = await _repository.resendCode(event.formData);
        if (response.status == 1) {
          yield VerifyCodeSuccess(signinResponse: response);
        }
      }

    }
  }

  bool _isCodeValid(String code) {
    return code.isNotEmpty;
  }

}
