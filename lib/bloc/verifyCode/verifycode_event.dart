import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';
import 'package:get2gether/models/requests/ResendCodeRequest.dart';
import 'package:get2gether/models/requests/SigninRequest.dart';
import 'package:get2gether/models/requests/VerifyCodeRequest.dart';

class VerifyCodeEvent extends Equatable {

  @override
  List<Object> get props => [];


}

class FormSubmitted extends VerifyCodeEvent {
  VerifyCodeRequest _formData;

  FormSubmitted(this._formData) ;

  VerifyCodeRequest get formData => _formData;
}


class ResendCode extends VerifyCodeEvent {
  ResendCodeRequest _formData;

  ResendCode(this._formData) ;

  ResendCodeRequest get formData => _formData;
}

class FormReset extends VerifyCodeEvent {}
