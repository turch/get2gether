import 'package:equatable/equatable.dart';
import 'package:get2gether/models/requests/SigninRequest.dart';

abstract class VerifyCodeBlocState extends Equatable {
  const VerifyCodeBlocState():super();

  @override
  List<Object> get props => [];
}

class VerifyCodeUninitialized extends VerifyCodeBlocState {}
class VerifyCodeInProgress extends VerifyCodeBlocState {}
class VerifyCodeError extends VerifyCodeBlocState {
  final String _error;
  VerifyCodeError(this._error);

  String get error => _error;
}

class VerifyCodeSuccess extends VerifyCodeBlocState {
  final SigninResponse signinResponse;

  const VerifyCodeSuccess({
    this.signinResponse,
  });

  VerifyCodeSuccess copyWith({
    SigninResponse response,
  }) {
    return VerifyCodeSuccess(
      signinResponse: response ?? this.signinResponse,
    );
  }

  @override
  List<Object> get props => [signinResponse];

  @override
  String toString() =>
      'Response Loaded { response: ${signinResponse} }';
}