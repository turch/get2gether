import 'package:equatable/equatable.dart';
import 'package:get2gether/models/requests/SigninRequest.dart';
import 'package:get2gether/models/requests/UserEventsRequest.dart';

abstract class UserEventsBlocState extends Equatable {
  const UserEventsBlocState():super();

  @override
  List<Object> get props => [];
}

class UserEventsUninitialized extends UserEventsBlocState {}
class UserEventsInProgress extends UserEventsBlocState {}
class UserEventsNotFound extends UserEventsBlocState {}
class UserEventsError extends UserEventsBlocState {
  final String _error;
  UserEventsError(this._error);

  String get error => _error;
}
class UserEventsFetched extends UserEventsBlocState {
  final UserEventsResponse userEventsResponse;

  const UserEventsFetched({
    this.userEventsResponse,
  });

  UserEventsFetched copyWith({
    SigninResponse response,
  }) {
    return UserEventsFetched(
      userEventsResponse: response ?? this.userEventsResponse,
    );
  }

  @override
  List<Object> get props => [userEventsResponse];

  @override
  String toString() =>
      'Response Loaded { response: ${userEventsResponse} }';
}

class EventDeletionInProgress extends UserEventsBlocState {
  int _deletedEventId;

  EventDeletionInProgress(this._deletedEventId);

  int get deletedEventId => _deletedEventId;


}
class EventDeleted extends UserEventsBlocState {

  int _deletedEventId;

  EventDeleted(this._deletedEventId);

  int get deletedEventId => _deletedEventId;


}