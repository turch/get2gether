
import 'package:bloc/bloc.dart';
import 'package:get2gether/models/GeneralResponse.dart';
import 'package:get2gether/models/requests/UserEventsRequest.dart';
import 'package:get2gether/resources/Repository.dart';
import 'package:rxdart/rxdart.dart';

import 'userevents_event.dart';
import 'userevents_state.dart';


class UserEventsBloc extends Bloc<UserEventsEvent, UserEventsBlocState> {

  final _repository = Repository();

  @override
  UserEventsBlocState get initialState => UserEventsUninitialized();


  @override
  Stream<UserEventsBlocState> transformEvents(Stream<UserEventsEvent> events,
      Stream<UserEventsBlocState> Function(UserEventsEvent event) next,) {
    return super.transformEvents(
      (events as Observable<UserEventsEvent>).debounceTime(
        Duration(milliseconds: 500),
      ),
      next,
    );
  }

  @override
  void onTransition(Transition<UserEventsEvent, UserEventsBlocState> transition) {
    print(transition);
  }

  @override
  Stream<UserEventsBlocState> mapEventToState(UserEventsEvent event) async* {
    if (event != null) {
      if(event is FetchUserEvents) {

        yield UserEventsInProgress();
        UserEventsResponse response = await _repository.fetchUserEvents(event.formData);
        if (response.status == 1) {
          yield UserEventsFetched(userEventsResponse: response);
        }
        else {
          yield UserEventsError(response.message.toString());
        }
      } else if(event is DeleteEvent) {
        yield EventDeletionInProgress(event.eventId);
        GeneralResponse response = await _repository.deleteEvent(event.eventId);
        if (response.status == 1) {
          yield EventDeleted(event.eventId);
        }
      }

    }
  }

}
