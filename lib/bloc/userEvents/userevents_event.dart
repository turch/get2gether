import 'package:equatable/equatable.dart';
import 'package:get2gether/models/requests/UserEventsRequest.dart';

class UserEventsEvent extends Equatable {

  @override
  List<Object> get props => [];
}

class FetchUserEvents extends UserEventsEvent {
   UserEventsRequest _formData;

   FetchUserEvents(this._formData) ;

   UserEventsRequest get formData => _formData;
}

class DeleteEvent extends UserEventsEvent {
  int _eventId;

  DeleteEvent(this._eventId);

  int get eventId => _eventId;


}

