import 'package:bloc/bloc.dart';
import 'package:get2gether/models/requests/EventSlotsByFilterRequest.dart';
import 'package:get2gether/resources/Repository.dart';
import 'package:rxdart/rxdart.dart';

import 'eventslotsbyfilter_event.dart';
import 'eventslotsbyfilter_state.dart';


class EventSlotsByFilterBloc extends Bloc<EventSlotsByFilterEvent, EventSlotsByFilterBlocState> {

  final _repository = Repository();

  @override
  EventSlotsByFilterBlocState get initialState => EventSlotsUninitialized();

  @override
  Stream<EventSlotsByFilterBlocState> mapEventToState(EventSlotsByFilterEvent event) async* {
    if (event != null) {
      if(event is FetchEventSlotsByFilter) {
        yield EventSlotsInProgress();
        EventSlotsByFilterResponse response = await _repository.fetchEventSlotsByFilter(event.formData);
        if(response == null) {
          yield EventSlotsError("Something Went Wrong");
        }
        else if (response.status == 1) {
          if(event is FetchEventSlotsByMonth)
            yield EventsSlotsFetchedByMonth(response);
          else if(event is FetchEventSlotsByDate)
            yield EventsSlotsFetchedByDate(response);
        }
        else {
          yield EventSlotsError(response.message.toString());
        }
      }
    }
  }
}
