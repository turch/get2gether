import 'package:equatable/equatable.dart';
import 'package:get2gether/models/requests/EventSlotsByFilterRequest.dart';

class EventSlotsByFilterEvent extends Equatable {

  @override
  List<Object> get props => [];
}

class FetchEventSlotsByFilter extends EventSlotsByFilterEvent {
  EventSlotsByFilterRequest _formData;
  FetchEventSlotsByFilter(this._formData) ;
  EventSlotsByFilterRequest get formData => _formData;
}

class FetchEventSlotsByDate extends FetchEventSlotsByFilter {
  FetchEventSlotsByDate(EventSlotsByFilterRequest formData) : super(formData);
}
class FetchEventSlotsByMonth extends FetchEventSlotsByFilter {
  FetchEventSlotsByMonth(EventSlotsByFilterRequest formData) : super(formData);
}
