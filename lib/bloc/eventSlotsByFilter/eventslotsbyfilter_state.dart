import 'package:equatable/equatable.dart';
import 'package:get2gether/models/requests/EventSlotsByFilterRequest.dart';

abstract class EventSlotsByFilterBlocState extends Equatable {
  const EventSlotsByFilterBlocState():super();

  @override
  List<Object> get props => [];
}

class EventSlotsUninitialized extends EventSlotsByFilterBlocState {}
class EventSlotsInProgress extends EventSlotsByFilterBlocState {}
class EventSlotsNotFound extends EventSlotsByFilterBlocState {}
class EventSlotsError extends EventSlotsByFilterBlocState {
  final String _error;
  EventSlotsError(this._error);

  String get error => _error;
}
class EventSlotsFetched extends EventSlotsByFilterBlocState {
  final EventSlotsByFilterResponse friendsAvailabilityResponse;

  const EventSlotsFetched(this.friendsAvailabilityResponse);

  @override
  List<Object> get props => [friendsAvailabilityResponse];

  @override
  String toString() =>
      'Response Loaded { response: ${friendsAvailabilityResponse} }';
}

class EventsSlotsFetchedByDate extends EventSlotsFetched {
  EventsSlotsFetchedByDate(EventSlotsByFilterResponse userEventsResponse) : super(userEventsResponse);
}
class EventsSlotsFetchedByMonth extends EventSlotsFetched {
  EventsSlotsFetchedByMonth(EventSlotsByFilterResponse userEventsResponse) : super(userEventsResponse);
}
