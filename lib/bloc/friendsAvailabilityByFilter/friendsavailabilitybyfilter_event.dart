import 'package:equatable/equatable.dart';
import 'package:get2gether/models/requests/FriendsAvailabilityByFilterRequest.dart';

class FriendsAvailabilityByFilterEvent extends Equatable {

  @override
  List<Object> get props => [];
}

class FetchFriendsAvailabilityByFilter extends FriendsAvailabilityByFilterEvent {
  FriendsAvailabilityByFilterRequest _formData;
  FetchFriendsAvailabilityByFilter(this._formData) ;
  FriendsAvailabilityByFilterRequest get formData => _formData;
}

class FetchFriendsAvailabilityByDate extends FetchFriendsAvailabilityByFilter {
  FetchFriendsAvailabilityByDate(FriendsAvailabilityByFilterRequest formData) : super(formData);
}
class FetchFriendsAvailabilityByMonth extends FetchFriendsAvailabilityByFilter {
  FetchFriendsAvailabilityByMonth(FriendsAvailabilityByFilterRequest formData) : super(formData);
}
class FetchFriendsAvailabilityByFriend extends FetchFriendsAvailabilityByFilter {
  FetchFriendsAvailabilityByFriend(FriendsAvailabilityByFilterRequest formData) : super(formData);
}
