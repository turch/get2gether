import 'package:equatable/equatable.dart';
import 'package:get2gether/models/requests/FriendsAvailabilityByFilterRequest.dart';

abstract class FriendsAvailabilityByFilterBlocState extends Equatable {
  const FriendsAvailabilityByFilterBlocState():super();

  @override
  List<Object> get props => [];
}

class FriendsAvailabilityUninitialized extends FriendsAvailabilityByFilterBlocState {}
class FriendsAvailabilityInProgress extends FriendsAvailabilityByFilterBlocState {}
class FriendsAvailabilityNotFound extends FriendsAvailabilityByFilterBlocState {}
class FriendsAvailabilityError extends FriendsAvailabilityByFilterBlocState {
  final String _error;
  FriendsAvailabilityError(this._error);

  String get error => _error;
}
class FriendsAvailabilityFetched extends FriendsAvailabilityByFilterBlocState {
  final FriendsAvailabilityByFilterResponse friendsAvailabilityResponse;

  const FriendsAvailabilityFetched(this.friendsAvailabilityResponse);

  @override
  List<Object> get props => [friendsAvailabilityResponse];

  @override
  String toString() =>
      'Response Loaded { response: ${friendsAvailabilityResponse} }';
}

class FriendsAvailabilityFetchedByDate extends FriendsAvailabilityFetched {
  FriendsAvailabilityFetchedByDate(FriendsAvailabilityByFilterResponse userAvailabilitySlotsResponse) : super(userAvailabilitySlotsResponse);
}
class FriendsAvailabilityFetchedByMonth extends FriendsAvailabilityFetched {
  FriendsAvailabilityFetchedByMonth(FriendsAvailabilityByFilterResponse userAvailabilitySlotsResponse) : super(userAvailabilitySlotsResponse);
}
