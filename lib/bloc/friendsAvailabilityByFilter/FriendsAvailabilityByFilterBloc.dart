import 'package:bloc/bloc.dart';
import 'package:get2gether/models/requests/FriendsAvailabilityByFilterRequest.dart';
import 'package:get2gether/resources/Repository.dart';
import 'package:rxdart/rxdart.dart';

import 'friendsavailabilitybyfilter_event.dart';
import 'friendsavailabilitybyfilter_state.dart';


class FriendsAvailabilityByFilterBloc extends Bloc<FriendsAvailabilityByFilterEvent, FriendsAvailabilityByFilterBlocState> {

  final _repository = Repository();

  @override
  FriendsAvailabilityByFilterBlocState get initialState => FriendsAvailabilityUninitialized();

  @override
  Stream<FriendsAvailabilityByFilterBlocState> mapEventToState(FriendsAvailabilityByFilterEvent event) async* {
    if (event != null) {
      if(event is FetchFriendsAvailabilityByFilter) {
        yield FriendsAvailabilityInProgress();
        FriendsAvailabilityByFilterResponse response = await _repository.fetchFriendsAvailabilityByFilter(event.formData);
        if(response == null) {
          yield FriendsAvailabilityError("Something Went Wrong");
        }
        else if (response.status == 1) {
          if(event is FetchFriendsAvailabilityByMonth)
            yield FriendsAvailabilityFetchedByMonth(response);
          else if(event is FetchFriendsAvailabilityByDate)
            yield FriendsAvailabilityFetchedByDate(response);
          yield FriendsAvailabilityFetched(response);
        }
        else {
          yield FriendsAvailabilityError(response.message.toString());
        }
      }
    }
  }
}
