import 'package:equatable/equatable.dart';
import 'package:get2gether/models/requests/EventsRequestByFilter.dart';

abstract class EventsByFilterBlocState extends Equatable {
  const EventsByFilterBlocState():super();

  @override
  List<Object> get props => [];
}

class EventsUninitialized extends EventsByFilterBlocState {}
class EventsInProgress extends EventsByFilterBlocState {}
class EventsNotFound extends EventsByFilterBlocState {}
class EventsError extends EventsByFilterBlocState {
  final String _error;
  EventsError(this._error);

  String get error => _error;
}
class EventsFetched extends EventsByFilterBlocState {
  final EventsResponseByFilter userEventsResponse;

  const EventsFetched(this.userEventsResponse);

  @override
  List<Object> get props => [userEventsResponse];

  @override
  String toString() =>
      'Response Loaded { response: ${userEventsResponse} }';
}

class EventsFetchedByDate extends EventsFetched {
  EventsFetchedByDate(EventsResponseByFilter userEventsResponse) : super(userEventsResponse);
}
class EventsFetchedByMonth extends EventsFetched {
  EventsFetchedByMonth(EventsResponseByFilter userEventsResponse) : super(userEventsResponse);
}
