import 'package:equatable/equatable.dart';
import 'package:get2gether/models/requests/EventsRequestByFilter.dart';

class EventsByFilterEvent extends Equatable {

  @override
  List<Object> get props => [];
}

class FetchEventsByFilter extends EventsByFilterEvent {
  EventsRequestByFilter _formData;
  FetchEventsByFilter(this._formData) ;
  EventsRequestByFilter get formData => _formData;
}

class FetchEventsByDate extends FetchEventsByFilter {
  FetchEventsByDate(EventsRequestByFilter formData) : super(formData);
}
class FetchEventsByMonth extends FetchEventsByFilter {
  FetchEventsByMonth(EventsRequestByFilter formData) : super(formData);
}