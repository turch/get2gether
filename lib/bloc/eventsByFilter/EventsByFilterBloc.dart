import 'package:bloc/bloc.dart';
import 'package:get2gether/models/requests/EventsRequestByFilter.dart';
import 'package:get2gether/resources/Repository.dart';
import 'package:rxdart/rxdart.dart';

import 'eventsbyfilter_event.dart';
import 'eventsbyfilter_state.dart';


class EventsByFilterBloc extends Bloc<EventsByFilterEvent, EventsByFilterBlocState> {

  final _repository = Repository();

  @override
  EventsByFilterBlocState get initialState => EventsUninitialized();

  @override
  Stream<EventsByFilterBlocState> mapEventToState(EventsByFilterEvent event) async* {
    if (event != null) {
      if(event is FetchEventsByFilter) {
        yield EventsInProgress();
        EventsResponseByFilter response = await _repository.fetchEventsByFilter(event.formData);
        if(response == null) {
          yield EventsError("Something Went Wrong");
        }
        else if (response.status == 1) {
          if(event is FetchEventsByMonth)
            yield EventsFetchedByMonth(response);
          else if(event is FetchEventsByDate)
            yield EventsFetchedByDate(response);
        }
        else {
          yield EventsError(response.message.toString());
        }
      }
    }
  }
}
