import 'package:equatable/equatable.dart';
import 'package:get2gether/models/requests/EditEventRequest.dart';


abstract class EventDetailBlocState extends Equatable {
  const EventDetailBlocState():super();

  @override
  List<Object> get props => [];
}

class PrevModelDisplayed extends EventDetailBlocState {}

class NewModelFetching extends EventDetailBlocState {}

class NewModelFetched extends EventDetailBlocState {

  EditEventResponse eventDetailModel;

  NewModelFetched(this.eventDetailModel);
}