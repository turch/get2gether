import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:get2gether/bloc/eventDetail/eventDetail_state.dart';
import 'package:get2gether/bloc/eventDetail/eventDetail_event.dart';

import 'package:get2gether/models/requests/EditEventRequest.dart';
import 'package:get2gether/resources/Repository.dart';


class EventDetailBloc extends Bloc<EventDetailEvent,EventDetailBlocState> {
  final Repository _repository = new Repository();

  @override
  EventDetailBlocState get initialState => PrevModelDisplayed();

  @override
  Stream<EventDetailBlocState> mapEventToState(EventDetailEvent event) async* {
    if (event != null) {
      EditEventResponse _response = await _repository.viewEventDetail(event.id);
      if(_response != null){
        if(_response.status == 1){
          print(_response.event.title);
          yield NewModelFetched(_response);
        }
      }
    }
  }
}