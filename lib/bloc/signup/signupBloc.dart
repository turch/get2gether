
import 'package:bloc/bloc.dart';
import 'package:get2gether/bloc/signup/signup_event.dart';
import 'package:get2gether/bloc/signup/signup_state.dart';
import 'package:get2gether/models/requests/SigninRequest.dart';
import 'package:get2gether/resources/Repository.dart';
import 'package:get2gether/resources/AppData.dart';
import 'package:rxdart/rxdart.dart';


class SignupBloc extends Bloc<SignupEvent, SignUpBlocState> {

  final _repository = Repository();

  final RegExp _emailRegExp = RegExp(r'^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$',);
  final RegExp _passwordRegExp = RegExp(r'^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{6,}$',);
  final RegExp _phoneRegExp = RegExp(r'[!@#<>?":_`~;[\]\\|=+)(*&^%0-9-]');
  final RegExp _nameRegExp = RegExp(r'[!@#<>?":_`~;[\]\\|=+)(*&^%\s-]');

  @override
  SignUpBlocState get initialState => SignupUninitialized();


  @override
  Stream<SignUpBlocState> transformEvents(Stream<SignupEvent> events,
      Stream<SignUpBlocState> Function(SignupEvent event) next,) {
    return super.transformEvents(
      (events as Observable<SignupEvent>).debounceTime(
        Duration(milliseconds: 500),
      ),
      next,
    );
  }

  @override
  void onTransition(Transition<SignupEvent, SignUpBlocState> transition) {
    print(transition);
  }

  @override
  Stream<SignUpBlocState> mapEventToState(SignupEvent event) async* {
    if (event != null) {
      if(event is FormSubmitted) {
        if( !isNameValid(event.formData.firstName)){
          yield SignupError("Enter Valid First Name.");
        }
        else if( !isNameValid(event.formData.lastName)){
          yield SignupError("Enter Valid Last Name.");
        }
        else if( !_isEmailValid(event.formData.email)) {
          yield SignupError("Enter Valid Email.");
        }
        else if(!_isPasswordValid(event.formData.password)) {
          yield SignupError("Enter Valid Password.");
        }
        else if(!isPhoneValid(event.formData.phoneNumber)) {
          yield SignupError("Enter Valid Phone Number.");
        }

        else if(state is SignupUninitialized || state is SignupError) {
          yield SignupInProgress();
          SigninResponse response = await _repository.signupUser(event.formData);
          if (response.status == 1) {
            AppData.currentUser = response.profile;
            yield SignupSuccess(signupResponse: response);
          }
          else {
            yield SignupError(response.message.toString());
          }
        }
      }

    }
  }

  bool isNameValid(String name) {
    return !_nameRegExp.hasMatch(name);
  }

  bool isPhoneValid(String phone) {
    return _phoneRegExp.hasMatch(phone);
  }

  bool _isEmailValid(String email) {
    return _emailRegExp.hasMatch(email);
  }

  bool _isPasswordValid(String password) {
    return password.isNotEmpty;
  }

}
