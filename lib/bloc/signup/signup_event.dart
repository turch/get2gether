import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';
import 'package:get2gether/models/requests/SignupRequest.dart';

class SignupEvent extends Equatable {

  @override
  List<Object> get props => [];


}

class EmailChanged extends SignupEvent {
  final String email;

  EmailChanged({@required this.email});

  @override
  List<Object> get props => [email];

  @override
  String toString() => 'EmailChanged { email: $email }';
}

class PasswordChanged extends SignupEvent {
  final String password;

  PasswordChanged({@required this.password});

  @override
  List<Object> get props => [password];

  @override
  String toString() => 'PasswordChanged { password: $password }';
}

class FirstNameChanged extends SignupEvent {
  final String firstName;

  FirstNameChanged({@required this.firstName});

  @override
  List<Object> get props => [firstName];

  @override
  String toString() => 'FirstNameChanged { firstName: $firstName }';
}

class LastNameChanged extends SignupEvent {
  final String lastName;

  LastNameChanged({@required this.lastName});

  @override
  List<Object> get props => [lastName];

  @override
  String toString() => 'LastNameChanged { LastName: $lastName }';
}


class PhoneNumberChanged extends SignupEvent {
  final String phoneNumber;

  PhoneNumberChanged({@required this.phoneNumber});

  @override
  List<Object> get props => [phoneNumber];

  @override
  String toString() => 'PhoneNumberChanged { LastName: $phoneNumber }';
}

class FormSubmitted extends SignupEvent {
  SignupRequest _formData;

  FormSubmitted(this._formData) ;

  SignupRequest get formData => _formData;
}

class FormReset extends SignupEvent {}
