import 'package:equatable/equatable.dart';
import 'package:get2gether/models/requests/SigninRequest.dart';

abstract class SignUpBlocState extends Equatable {
  const SignUpBlocState():super();

  @override
  List<Object> get props => [];
}

class SignupUninitialized extends SignUpBlocState {}
class SignupInProgress extends SignUpBlocState {}
class SignupError extends SignUpBlocState {
  final String _error;
  SignupError(this._error);

  String get error => _error;
}

class SignupSuccess extends SignUpBlocState {
  final SigninResponse signupResponse;

  const SignupSuccess({
    this.signupResponse,
  });

  SignupSuccess copyWith({
    SigninResponse response,
  }) {
    return SignupSuccess(
      signupResponse: response ?? this.signupResponse,
    );
  }

  @override
  List<Object> get props => [signupResponse];

  @override
  String toString() =>
      'Response Loaded { response: ${signupResponse} }';
}