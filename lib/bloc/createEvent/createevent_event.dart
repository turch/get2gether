import 'package:equatable/equatable.dart';
import 'package:get2gether/models/requests/CreateEventRequest.dart';
import 'package:get2gether/models/requests/EditEventRequest.dart';
import 'package:get2gether/models/requests/InviteFriendRequest.dart';
import 'package:get2gether/models/requests/RemoveFriendRequest.dart';

class CreateEventEvent extends Equatable {

  @override
  List<Object> get props => [];
}

class CreateEventFormSubmitted extends CreateEventEvent {
  CreateEventRequest _formData;

  CreateEventFormSubmitted(this._formData) ;

  CreateEventRequest get formData => _formData;
}

class EditEventFormSubmitted extends CreateEventEvent {
  EditEventRequest _formData;

  EditEventFormSubmitted(this._formData) ;

  EditEventRequest get formData => _formData;
}

class FormReset extends CreateEventEvent {}
