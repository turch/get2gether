import 'package:equatable/equatable.dart';
import 'package:get2gether/models/requests/CreateEventRequest.dart';
import 'package:get2gether/models/requests/EditEventRequest.dart';
import 'package:get2gether/models/requests/SigninRequest.dart';

abstract class CreateEventBlocState extends Equatable {
  const CreateEventBlocState():super();

  @override
  List<Object> get props => [];
}

class CreateEventUninitialized extends CreateEventBlocState {}
class CreateEventInProgress extends CreateEventBlocState {}
class CreateEventError extends CreateEventBlocState {
  final String _error;
  CreateEventError(this._error);

  String get error => _error;
}


class CreateEventSuccess extends CreateEventBlocState {
  final CreateEventResponse createEventResponse;

  const CreateEventSuccess({
    this.createEventResponse,
  });

  CreateEventSuccess copyWith({
    SigninResponse response,
  }) {
    return CreateEventSuccess(
      createEventResponse: response ?? this.createEventResponse,
    );
  }

  @override
  List<Object> get props => [createEventResponse];

  @override
  String toString() =>
      'Response Loaded { response: ${createEventResponse} }';
}
class EditEventSuccess extends CreateEventBlocState {
  final EditEventResponse editEventResponse;

  const EditEventSuccess({
    this.editEventResponse,
  });

  EditEventSuccess copyWith({
    SigninResponse response,
  }) {
    return EditEventSuccess(
      editEventResponse: response ?? this.editEventResponse,
    );
  }

  @override
  List<Object> get props => [editEventResponse];

  @override
  String toString() =>
      'Response Loaded { response: ${editEventResponse} }';
}
