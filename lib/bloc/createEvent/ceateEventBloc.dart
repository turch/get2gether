
import 'package:bloc/bloc.dart';
import 'package:get2gether/models/requests/CreateEventRequest.dart';
import 'package:get2gether/models/requests/EditEventRequest.dart';
import 'package:get2gether/resources/AppData.dart';
import 'package:get2gether/resources/Repository.dart';
import 'package:rxdart/rxdart.dart';

import 'createevent_event.dart';
import 'createevent_state.dart';


class CreateEventBloc extends Bloc<CreateEventEvent, CreateEventBlocState> {

  final _repository = Repository();

  @override
  CreateEventBlocState get initialState => CreateEventUninitialized();


  @override
  Stream<CreateEventBlocState> transformEvents(Stream<CreateEventEvent> events,
      Stream<CreateEventBlocState> Function(CreateEventEvent event) next,) {
    return super.transformEvents(
      (events as Observable<CreateEventEvent>).debounceTime(
        Duration(milliseconds: 500),
      ),
      next,
    );
  }

  @override
  void onTransition(Transition<CreateEventEvent, CreateEventBlocState> transition) {
    print(transition);
  }

  @override
  Stream<CreateEventBlocState> mapEventToState(CreateEventEvent event) async* {
    if (event != null) {
      if(event is CreateEventFormSubmitted) {
        String error = _isCreateRequestValid(event.formData);
        if(error != null) {
          yield CreateEventError(error);
        }

        if(state is CreateEventUninitialized || state is CreateEventError) {
          yield CreateEventInProgress();
          CreateEventResponse response = await _repository.createEvent(event.formData);
          if (response.status == 1) {
            if(AppData.currentUserEvents == null) AppData.currentUserEvents = new List();
            AppData.currentUserEvents.insert(0, response.event);
            yield CreateEventSuccess(createEventResponse: response);
          }
          else {
            yield CreateEventError(response.message.toString());
          }
        }
      }
      else if(event is EditEventFormSubmitted) {
        String error = _isEditRequestValid(event.formData);
        if(error != null) {
          yield CreateEventError(error);
        }

        if(state is CreateEventUninitialized || state is CreateEventError) {
          yield CreateEventInProgress();
          EditEventResponse response = await _repository.editEvent(event.formData);
          if (response.status == 1) {
            yield EditEventSuccess(editEventResponse: response);
          }
          else {
            yield CreateEventError(response.message.toString());
          }
        }
      }

    }
  }

  String _isCreateRequestValid(CreateEventRequest request) {
    if(request.title.isEmpty) {
      return "Please provide valid Title.";
    } else if(request.shortDescription.isEmpty) {
      return "Please provide valid Description.";
    } else if(request.paymentRequired == 1 && request.perHeadContribution <= 0) {
      return "Please provide per head contribution amount.";
    }
    return null;
  }

  String _isEditRequestValid(EditEventRequest request) {
    if(request.title.isEmpty) {
      return "Please provide valid Title.";
    } else if(request.shortDescription.isEmpty) {
      return "Please provide valid Description.";
    }
    return null;
  }

}
