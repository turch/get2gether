import 'package:equatable/equatable.dart';
import 'package:get2gether/models/requests/EditEventRequest.dart';

abstract class UpdateEventInvitesBlocState extends Equatable {
  const UpdateEventInvitesBlocState():super();

  @override
  List<Object> get props => [];
}
class UpdateInviteError extends UpdateEventInvitesBlocState {
  final String _error;
  UpdateInviteError(this._error);

  String get error => _error;
}
class UpdateInviteUninitialized extends UpdateEventInvitesBlocState {}
class UpdateInviteInProgress extends UpdateEventInvitesBlocState {}
class FriendInvited extends UpdateEventInvitesBlocState {
  EditEventResponse response;

  FriendInvited(this.response);
}
class FriendRemoved extends UpdateEventInvitesBlocState {
  EditEventResponse response;

  FriendRemoved(this.response);

}