
import 'package:bloc/bloc.dart';
import 'package:get2gether/models/GeneralResponse.dart';
import 'package:get2gether/models/requests/EditEventRequest.dart';
import 'package:get2gether/resources/Repository.dart';
import 'package:rxdart/rxdart.dart';

import 'updateeventinvites_event.dart';
import 'updateeventinvites_state.dart';


class UpdateEventInvitesBloc extends Bloc<UpdateEventInvitesEvent, UpdateEventInvitesBlocState> {

  final _repository = Repository();

  @override
  UpdateEventInvitesBlocState get initialState => UpdateInviteUninitialized();


  @override
  Stream<UpdateEventInvitesBlocState> transformEvents(Stream<UpdateEventInvitesEvent> events,
      Stream<UpdateEventInvitesBlocState> Function(UpdateEventInvitesEvent event) next,) {
    return super.transformEvents(
      (events as Observable<UpdateEventInvitesEvent>).debounceTime(
        Duration(milliseconds: 500),
      ),
      next,
    );
  }

  @override
  void onTransition(Transition<UpdateEventInvitesEvent, UpdateEventInvitesBlocState> transition) {
    print(transition);
  }

  @override
  Stream<UpdateEventInvitesBlocState> mapEventToState(UpdateEventInvitesEvent event) async* {
    if (event != null) {

      if(event is RemoveFriend) {
        yield UpdateInviteInProgress();
        EditEventResponse response = await _repository.removeFriend(event.request);
        if (response.status == 1) {
          yield FriendRemoved(response);
        }
        else {
          yield UpdateInviteError(response.message.toString());
        }
      }

      else if(event is InviteFriend) {
        yield UpdateInviteInProgress();
        EditEventResponse response = await _repository.inviteFriend(event.request);
        if (response.status == 1) {
          yield FriendInvited(response);
        }
        else {
          yield UpdateInviteError(response.message.toString());
        }
      }

    }
  }


}
