import 'package:equatable/equatable.dart';
import 'package:get2gether/models/requests/InviteFriendRequest.dart';
import 'package:get2gether/models/requests/RemoveFriendRequest.dart';

class UpdateEventInvitesEvent extends Equatable {

  @override
  List<Object> get props => [];
}


class RemoveFriend extends UpdateEventInvitesEvent {
  RemoveFriendRequest _request;

  RemoveFriend(this._request);

  RemoveFriendRequest get request => _request;

}

class InviteFriend extends UpdateEventInvitesEvent {
  InviteFriendRequest _request;

  InviteFriend(this._request);

  InviteFriendRequest get request => _request;


}

