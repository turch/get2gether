import 'package:equatable/equatable.dart';
import 'package:get2gether/models/Event.dart';
import 'package:get2gether/models/requests/EditEventRequest.dart';

abstract class UpdateInviteBlocState extends Equatable {
  const UpdateInviteBlocState():super();

  @override
  List<Object> get props => [];
}

class UpdateInviteUninitialized extends UpdateInviteBlocState {}
class UpdateInviteAcceptInProgress extends UpdateInviteBlocState {}
class UpdateInviteRejectInProgress extends UpdateInviteBlocState {}
class UpdateInviteError extends UpdateInviteBlocState {
  final String _error;
  UpdateInviteError(this._error);

  String get error => _error;
}

class RefreshEventCell extends UpdateInviteBlocState {
  final Event event;
  RefreshEventCell(this.event);

}

class UpdateInviteSuccess extends UpdateInviteBlocState {
  final EditEventResponse response;

  const UpdateInviteSuccess({
    this.response,
  });

  UpdateInviteSuccess copyWith({
    EditEventResponse response,
  }) {
    return UpdateInviteSuccess(
      response: response ?? this.response,
    );
  }

  @override
  List<Object> get props => [response];

  @override
  String toString() =>
      'Response Loaded { response: ${response} }';
}

class NewModelFetched extends UpdateInviteBlocState {

  EditEventResponse eventDetailModel;

  NewModelFetched(this.eventDetailModel);


}