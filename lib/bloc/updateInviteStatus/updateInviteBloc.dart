
import 'package:bloc/bloc.dart';
import 'package:get2gether/models/GeneralResponse.dart';
import 'package:get2gether/models/requests/EditEventRequest.dart';
import 'package:get2gether/resources/Repository.dart';
import 'package:rxdart/rxdart.dart';

import 'updateinvite_event.dart';
import 'updateinvite_state.dart';


class UpdateInviteBloc extends Bloc<UpdateInviteEvent, UpdateInviteBlocState> {

  final _repository = Repository();

  @override
  UpdateInviteBlocState get initialState => UpdateInviteUninitialized();


  @override
  Stream<UpdateInviteBlocState> transformEvents(Stream<UpdateInviteEvent> events,
      Stream<UpdateInviteBlocState> Function(UpdateInviteEvent event) next,) {
    return super.transformEvents(
      (events as Observable<UpdateInviteEvent>).debounceTime(
        Duration(milliseconds: 500),
      ),
      next,
    );
  }

  @override
  void onTransition(Transition<UpdateInviteEvent, UpdateInviteBlocState> transition) {
    print(transition);
  }

  @override
  Stream<UpdateInviteBlocState> mapEventToState(UpdateInviteEvent event) async* {
    if (event != null) {
      if(event is RejectInvite) {
        if (state is UpdateInviteUninitialized || state is UpdateInviteError) {
          yield UpdateInviteRejectInProgress();
          EditEventResponse response = await _repository.updateInvite(event.inviteUpdateRequest);
          if (response.status == 1) {
            yield UpdateInviteSuccess(response: response);
          }
        }
      }
      else if(event is AcceptInvite) {
        if (state is UpdateInviteUninitialized || state is UpdateInviteError) {
          yield UpdateInviteAcceptInProgress();
          EditEventResponse response = await _repository.updateInvite(event.inviteUpdateRequest);
          if (response.status == 1) {
            yield UpdateInviteSuccess(response: response);
          }
        }
      }
      else if(event is UpdateEventCell) {
        yield RefreshEventCell(event.event);
      }
      else if(event is RestEvent) {
        yield UpdateInviteUninitialized();
      }
      else if(event is EventDetailEvent) {
        EditEventResponse _response = await _repository.viewEventDetail(event.id);
        if(_response != null){
          if(_response.status == 1){
            print(_response.event.title);
            yield NewModelFetched(_response);
          }
        }
      }
    }
  }

}
