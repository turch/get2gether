import 'package:equatable/equatable.dart';
import 'package:get2gether/models/Event.dart';
import 'package:get2gether/models/requests/UpdateInviteRequest.dart';

class UpdateInviteEvent extends Equatable {

  @override
  List<Object> get props => [];
}

class RestEvent extends UpdateInviteEvent{}
class AcceptInvite extends UpdateInviteEvent {
  UpdateInviteRequest inviteUpdateRequest;

  AcceptInvite(this.inviteUpdateRequest);
}

class RejectInvite extends UpdateInviteEvent {
  UpdateInviteRequest inviteUpdateRequest;

  RejectInvite(this.inviteUpdateRequest);
}

class UpdateEventCell extends UpdateInviteEvent {
  Event event;

  UpdateEventCell(this.event);
}

class EventDetailEvent extends UpdateInviteEvent {
  String _id;
  EventDetailEvent(this._id);

  String get id => _id;

}


