import 'package:equatable/equatable.dart';
import 'package:get2gether/models/requests/ForgotPasswordRequest.dart';

class ForgotPasswordEvent extends Equatable {

  @override
  List<Object> get props => [];


}

class FormSubmitted extends ForgotPasswordEvent {
  ForgotPasswordRequest _formData;

  FormSubmitted(this._formData) ;

  ForgotPasswordRequest get formData => _formData;
}


class FormReset extends ForgotPasswordEvent {}
