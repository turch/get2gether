
import 'package:bloc/bloc.dart';
import 'package:get2gether/bloc/forgotPassword/forgotpassword_state.dart';
import 'package:get2gether/models/GeneralResponse.dart';
import 'package:get2gether/resources/Repository.dart';
import 'package:rxdart/rxdart.dart';

import 'forgotpassword_event.dart';


class ForgotPasswordBloc extends Bloc<ForgotPasswordEvent, ForgotPasswordBlocState> {

  final _repository = Repository();
  final RegExp _emailRegExp = RegExp(
    r'^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$',
  );

  @override
  ForgotPasswordBlocState get initialState => ForgotPasswordUninitialized();


  @override
  Stream<ForgotPasswordBlocState> transformEvents(Stream<ForgotPasswordEvent> events,
      Stream<ForgotPasswordBlocState> Function(ForgotPasswordEvent event) next,) {
    return super.transformEvents(
      (events as Observable<ForgotPasswordEvent>).debounceTime(
        Duration(milliseconds: 500),
      ),
      next,
    );
  }

  @override
  void onTransition(Transition<ForgotPasswordEvent, ForgotPasswordBlocState> transition) {
    print(transition);
  }

  @override
  Stream<ForgotPasswordBlocState> mapEventToState(ForgotPasswordEvent event) async* {
    if (event != null) {
      if(event is FormSubmitted) {
        if( !_isEmailValid(event.formData.email)){
          yield ForgotPasswordError("Please enter valide code.");
        }

        else if(state is ForgotPasswordUninitialized || state is ForgotPasswordError) {
          yield ForgotPasswordInProgress();
          GeneralResponse response = await _repository.forgotPassword(event.formData);
          if (response.status == 1) {
            yield ForgotPasswordSuccess(generalResponse: response);
          }
          else {
            yield ForgotPasswordError(response.message.toString());
          }
        }
      }
    }
  }

  bool _isEmailValid(String email) {
    return _emailRegExp.hasMatch(email);
  }

}
