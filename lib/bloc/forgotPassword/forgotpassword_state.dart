import 'package:equatable/equatable.dart';
import 'package:get2gether/models/GeneralResponse.dart';
import 'package:get2gether/models/requests/SigninRequest.dart';

abstract class ForgotPasswordBlocState extends Equatable {
  const ForgotPasswordBlocState():super();

  @override
  List<Object> get props => [];
}

class ForgotPasswordUninitialized extends ForgotPasswordBlocState {}
class ForgotPasswordInProgress extends ForgotPasswordBlocState {}
class ForgotPasswordError extends ForgotPasswordBlocState {
  final String _error;
  ForgotPasswordError(this._error);

  String get error => _error;
}

class ForgotPasswordSuccess extends ForgotPasswordBlocState {
  final GeneralResponse generalResponse;

  const ForgotPasswordSuccess({
    this.generalResponse,
  });

  ForgotPasswordSuccess copyWith({
    SigninResponse response,
  }) {
    return ForgotPasswordSuccess(
      generalResponse: response ?? this.generalResponse,
    );
  }

  @override
  List<Object> get props => [generalResponse];

  @override
  String toString() =>
      'Response Loaded { response: ${generalResponse} }';
}