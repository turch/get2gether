import 'package:equatable/equatable.dart';
import 'package:get2gether/models/requests/AvailabilitySlotsRequestByFilter.dart';

abstract class AvailabilitySlotsByFilterBlocState extends Equatable {
  const AvailabilitySlotsByFilterBlocState():super();

  @override
  List<Object> get props => [];
}

class AvailabilitySlotsUninitialized extends AvailabilitySlotsByFilterBlocState {}
class AvailabilitySlotsInProgress extends AvailabilitySlotsByFilterBlocState {}
class AvailabilitySlotsNotFound extends AvailabilitySlotsByFilterBlocState {}
class AvailabilitySlotsError extends AvailabilitySlotsByFilterBlocState {
  final String _error;
  AvailabilitySlotsError(this._error);

  String get error => _error;
}
class AvailabilitySlotsFetched extends AvailabilitySlotsByFilterBlocState {
  final AvailabilitySlotsResponseByFilter userAvailabilitySlotsResponse;

  const AvailabilitySlotsFetched(this.userAvailabilitySlotsResponse);

  @override
  List<Object> get props => [userAvailabilitySlotsResponse];

  @override
  String toString() =>
      'Response Loaded { response: ${userAvailabilitySlotsResponse} }';
}

class AvailabilitySlotsFetchedByDate extends AvailabilitySlotsFetched {
  AvailabilitySlotsFetchedByDate(AvailabilitySlotsResponseByFilter userAvailabilitySlotsResponse) : super(userAvailabilitySlotsResponse);
}
class AvailabilitySlotsFetchedByMonth extends AvailabilitySlotsFetched {
  AvailabilitySlotsFetchedByMonth(AvailabilitySlotsResponseByFilter userAvailabilitySlotsResponse) : super(userAvailabilitySlotsResponse);
}
