import 'package:bloc/bloc.dart';
import 'package:get2gether/models/requests/AvailabilitySlotsRequestByFilter.dart';
import 'package:get2gether/resources/Repository.dart';
import 'package:rxdart/rxdart.dart';

import 'availabilityslotsbyfilter_event.dart';
import 'availabilityslotsbyfilter_state.dart';


class AvailabilitySlotsByFilterBloc extends Bloc<AvailabilitySlotsByFilterEvent, AvailabilitySlotsByFilterBlocState> {

  final _repository = Repository();

  @override
  AvailabilitySlotsByFilterBlocState get initialState => AvailabilitySlotsUninitialized();

  @override
  Stream<AvailabilitySlotsByFilterBlocState> mapEventToState(AvailabilitySlotsByFilterEvent event) async* {
    if (event != null) {
      if(event is FetchAvailabilitySlotsByFilter) {
        yield AvailabilitySlotsInProgress();
        AvailabilitySlotsResponseByFilter response = await _repository.fetchAvailabilitySlotsByFilter(event.formData);
        if(response == null) {
          yield AvailabilitySlotsError("Something Went Wrong");
        }
        else if (response.status == 1) {
          if(event is FetchAvailabilitySlotsByMonth)
            yield AvailabilitySlotsFetchedByMonth(response);
          else if(event is FetchAvailabilitySlotsByDate)
            yield AvailabilitySlotsFetchedByDate(response);
        }
        else {
          yield AvailabilitySlotsError(response.message.toString());
        }
      }
    }
  }

}
