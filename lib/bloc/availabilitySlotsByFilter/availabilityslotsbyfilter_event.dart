import 'package:equatable/equatable.dart';
import 'package:get2gether/models/requests/AvailabilitySlotsRequestByFilter.dart';

class AvailabilitySlotsByFilterEvent extends Equatable {

  @override
  List<Object> get props => [];
}

class FetchAvailabilitySlotsByFilter extends AvailabilitySlotsByFilterEvent {
  AvailabilitySlotsRequestByFilter _formData;
  FetchAvailabilitySlotsByFilter(this._formData) ;
  AvailabilitySlotsRequestByFilter get formData => _formData;
}

class FetchAvailabilitySlotsByDate extends FetchAvailabilitySlotsByFilter {
  FetchAvailabilitySlotsByDate(AvailabilitySlotsRequestByFilter formData) : super(formData);
}
class FetchAvailabilitySlotsByMonth extends FetchAvailabilitySlotsByFilter {
  FetchAvailabilitySlotsByMonth(AvailabilitySlotsRequestByFilter formData) : super(formData);
}


