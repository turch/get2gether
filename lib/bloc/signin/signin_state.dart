import 'package:equatable/equatable.dart';
import 'package:get2gether/models/requests/SigninRequest.dart';

abstract class SignInBlocState extends Equatable {
  const SignInBlocState():super();

  @override
  List<Object> get props => [];

}

class SigninUninitialized extends SignInBlocState {}
class SigninInProgress extends SignInBlocState {}
class SigninError extends SignInBlocState {
  final String _error;
  SigninError(this._error);

  String get error => _error;
}

class SigninSuccess extends SignInBlocState {
  final SigninResponse signinResponse;

  const SigninSuccess({
    this.signinResponse,
  });

  SigninSuccess copyWith({
    SigninResponse response,
  }) {
    return SigninSuccess(
      signinResponse: response ?? this.signinResponse,
    );
  }

  @override
  List<Object> get props => [signinResponse];

  @override
  String toString() =>
      'Response Loaded { response: ${signinResponse} }';
}