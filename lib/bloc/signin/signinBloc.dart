
import 'package:bloc/bloc.dart';
import 'package:get2gether/bloc/signin/signin_event.dart';
import 'package:get2gether/bloc/signin/signin_state.dart';
import 'package:get2gether/models/requests/SigninRequest.dart';
import 'package:get2gether/resources/Repository.dart';
import 'package:get2gether/resources/AppData.dart';
import 'package:rxdart/rxdart.dart';


class SigninBloc extends Bloc<SigninEvent, SignInBlocState> {

  final _repository = Repository();

  final RegExp _emailRegExp = RegExp(
    r'^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$',
  );
  final RegExp _passwordRegExp = RegExp(
    r'^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{6,}$',
  );

  @override
  SignInBlocState get initialState => SigninUninitialized();


  @override
  Stream<SignInBlocState> transformEvents(Stream<SigninEvent> events,
      Stream<SignInBlocState> Function(SigninEvent event) next,) {
    return super.transformEvents(
      (events as Observable<SigninEvent>).debounceTime(
        Duration(milliseconds: 500),
      ),
      next,
    );
  }

  @override
  void onTransition(Transition<SigninEvent, SignInBlocState> transition) {
    print(transition);
  }

  @override
  Stream<SignInBlocState> mapEventToState(SigninEvent event) async* {
    if (event != null) {
      if(event is FormSubmitted) {
        if( !_isEmailValid(event.formData.email)){
          yield SigninError("Enter Valid Email.");
        }
        else if(!_isPasswordValid(event.formData.password)) {
          yield SigninError("Enter Valid Password.");
        }

        else if(state is SigninUninitialized || state is SigninError) {
          yield SigninInProgress();
          SigninResponse response = await _repository.signinUser(event.formData);
          if (response.status == 1) {
            AppData.currentUser = response.profile;
            AppData.currentUserEvents = response.events;
            yield SigninSuccess(signinResponse: response);
          }
          else {
            yield SigninError(response.message.toString());
          }
        }
      }

    }
  }

  bool _isEmailValid(String email) {
    return _emailRegExp.hasMatch(email);
  }

  bool _isPasswordValid(String password) {
    return password.isNotEmpty;
  }

}
