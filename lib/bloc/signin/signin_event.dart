import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';
import 'package:get2gether/models/requests/SigninRequest.dart';

class SigninEvent extends Equatable {

  @override
  List<Object> get props => [];


}

class EmailChanged extends SigninEvent {
  final String email;

  EmailChanged({@required this.email});

  @override
  List<Object> get props => [email];

  @override
  String toString() => 'EmailChanged { email: $email }';
}

class PasswordChanged extends SigninEvent {
  final String password;

  PasswordChanged({@required this.password});

  @override
  List<Object> get props => [password];

  @override
  String toString() => 'PasswordChanged { password: $password }';
}

class FormSubmitted extends SigninEvent {
  SigninRequest _formData;

  FormSubmitted(this._formData) ;

  SigninRequest get formData => _formData;
}

class FormReset extends SigninEvent {}
