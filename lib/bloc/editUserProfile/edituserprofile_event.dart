import 'package:equatable/equatable.dart';
import 'package:get2gether/models/requests/EditUserProfileRequest.dart';

class EditUserProfileEvent extends Equatable {

  @override
  List<Object> get props => [];
}

class FormSubmitted extends EditUserProfileEvent {
  UpdateRequest _requestType;
  EditUserProfileRequest _formData;


  FormSubmitted(this._requestType, this._formData);


  UpdateRequest get requestType => _requestType;

  EditUserProfileRequest get formData => _formData;
}

enum UpdateRequest{
  firstName,
  lastName,
  image,
  password
}

