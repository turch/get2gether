import 'package:equatable/equatable.dart';
import 'package:get2gether/models/GeneralResponse.dart';
import 'package:get2gether/models/requests/EditUserProfileRequest.dart';
import 'package:get2gether/models/requests/SigninRequest.dart';

abstract class EditUserProfileBlocState extends Equatable {
  const EditUserProfileBlocState():super();

  @override
  List<Object> get props => [];
}

class EditProfileUninitialized extends EditUserProfileBlocState {}
class EditProfileInProgress extends EditUserProfileBlocState {}
class EditProfileError extends EditUserProfileBlocState {
  final String _error;
  EditProfileError(this._error);

  String get error => _error;
}


class EditProfileSuccess extends EditUserProfileBlocState {
  final EditUserProfileResponse response;

  const EditProfileSuccess({
    this.response,
  });

  EditProfileSuccess copyWith({
    SigninResponse response,
  }) {
    return EditProfileSuccess(
      response: response ?? this.response,
    );
  }

  @override
  List<Object> get props => [response];

  @override
  String toString() =>
      'Response Loaded { response: ${response} }';
}