
import 'package:bloc/bloc.dart';
import 'package:get2gether/models/GeneralResponse.dart';
import 'package:get2gether/models/requests/EditUserProfileRequest.dart';
import 'package:get2gether/resources/Repository.dart';
import 'package:get2gether/util/SharedPrefHelper.dart';
import 'package:rxdart/rxdart.dart';

import 'edituserprofile_event.dart';
import 'edituserprofile_state.dart';


class EditUserProfileBloc extends Bloc<EditUserProfileEvent, EditUserProfileBlocState> {

  final _repository = Repository();
  final RegExp _nameRegExp = RegExp(r'[!@#<>?":_`~;[\]\\|=+)(*&^%\s-]');

  @override
  EditUserProfileBlocState get initialState => EditProfileUninitialized();


  @override
  Stream<EditUserProfileBlocState> transformEvents(Stream<EditUserProfileEvent> events,
      Stream<EditUserProfileBlocState> Function(EditUserProfileEvent event) next,) {
    return super.transformEvents(
      (events as Observable<EditUserProfileEvent>).debounceTime(
        Duration(milliseconds: 500),
      ),
      next,
    );
  }

  @override
  void onTransition(Transition<EditUserProfileEvent, EditUserProfileBlocState> transition) {
    print(transition);
  }

  @override
  Stream<EditUserProfileBlocState> mapEventToState(EditUserProfileEvent event) async* {
    if (event != null) {
      if(event is FormSubmitted) {
        String error = _isRequestValid(event.formData, event.requestType);
        if(error != null) {
          yield EditProfileError(error);
        }

        if(! (state is EditProfileInProgress)) {
          yield EditProfileInProgress();
          EditUserProfileResponse response = await _repository.editProfile(event.formData, event.requestType);
          if (response.status == 1) {
            SharedPreferencesHelper.save(SharedPreferenceKeys.USER, response.profile);
            yield EditProfileSuccess(response: response);
          }
          else {
            yield EditProfileError(response.message.toString());
          }
        }
      }

    }
  }

  String _isRequestValid(EditUserProfileRequest request, UpdateRequest requestType) {
    if(requestType == UpdateRequest.image)
      return request.image == null? "Please provide valid Image." : null;
    else if(requestType == UpdateRequest.firstName)
      return request.firstName == null || !isNameValid(request.firstName) ? "Please provide valid First Name." : null;
    else if(requestType == UpdateRequest.lastName)
      return request.lastName == null || !isNameValid(request.lastName) ? "Please provide valid Last Name." : null;
    else
      return "Invalid Request";
  }

  bool isNameValid(String name) {
    return !_nameRegExp.hasMatch(name);
  }

}
