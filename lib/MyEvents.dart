import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get2gether/UIComponents/AppButton.dart';
import 'package:get2gether/bloc/userEvents/userEventsBloc.dart';
import 'package:get2gether/bloc/userEvents/userevents_event.dart';
import 'package:get2gether/bloc/userEvents/userevents_state.dart';
import 'package:get2gether/models/Event.dart';
import 'package:get2gether/resources/AppData.dart';

import 'EventCell.dart';
import 'bloc/eventsByFilter/eventsbyfilter_state.dart';

class MyEvents extends StatefulWidget {

  @override
  _MyEventsState createState() => _MyEventsState();
}

class _MyEventsState extends State<MyEvents>{

  UserEventsBloc _userEventsBloc;

  @override
  void initState() {
    _userEventsBloc = BlocProvider.of<UserEventsBloc>(context);
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }


  Widget build(BuildContext context) {
    return BlocListener<UserEventsBloc, UserEventsBlocState>(
        bloc: _userEventsBloc,
        listener: (context, state) {
          if (state is EventDeleted) {
            _removeEventFromList(state.deletedEventId);
          }
        },
        child:

        BlocBuilder<UserEventsBloc, UserEventsBlocState>(
            bloc: _userEventsBloc,
            builder: (context, state) {
              if (state is UserEventsFetched || state is EventDeleted ||
                  state is EventDeletionInProgress) {
                if (AppData.currentUserEvents != null &&
                    AppData.currentUserEvents.length > 0) {
                  return Container(
                      padding: EdgeInsets.only(top: 10.0),
                      child: ListView.builder(
                          itemCount: AppData.currentUserEvents.length,
                          itemBuilder: (BuildContext context, int index) {
                            var eventModel = AppData.currentUserEvents[index];
                            return EventsCell(eventModel, deleteCallback: () {
                              _userEventsBloc.add(new DeleteEvent(
                                  eventModel.id));
                            },);
                          }
                      )
                  );
                } else {
                  return Center(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Text("You haven't added any Events yet."),
                          SizedBox(height: 10.0,),
                          AppButton('Add Event', _createEvent)
                        ],
                      )
                  );
                }
              } else if (state is UserEventsInProgress) {
                return Center(
                  child: CircularProgressIndicator(),
                );
              } else {
                return Container();
              }
            }
        )
    );
  }

  _removeEventFromList(int eventId) {
      List<Event> events = AppData.currentUserEvents;
      for (int i = 0; i < events.length; i++) {
        Event event = events.elementAt(i);
        if (event.id == eventId) {
          AppData.currentUserEvents.removeAt(i);
          break;
        }
      }
    }

  _createEvent() {
    Navigator.pushNamed(context, '/CreateEvent');
  }
}