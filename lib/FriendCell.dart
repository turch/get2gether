import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get2gether/util/AppColors.dart';
import 'package:get2gether/util/AppDimensions.dart';

import 'models/Profile.dart';

class FriendCell extends StatefulWidget {

  Profile _friend;
  @override
  _FriendCellState createState() => _FriendCellState();

  FriendCell(this._friend);
}

class _FriendCellState extends State<FriendCell>{

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  Widget build(BuildContext context) {
    double _imageWidth = AppDimensions.imageWidth(MediaQuery.of(context).size);
    var title = "";
    var fName = widget._friend.firstName;
    var lName = widget._friend.lastName;
    if(fName == null && lName == null) {
      if(widget._friend.phone != null) {
        title = widget._friend.phone;
      } else {
        title = widget._friend.email;
      }
    }
    else {
      if(fName != null) title = fName;
      if(lName != null) title += " " + lName;
      if(widget._friend.phone != null) {
        title += "\n" + widget._friend.countryCode+widget._friend.phone;
      } else {
        title += "\n" +  widget._friend.email;
      }
    }

    return Card(
        color: Colors.white,
        child: Container(
            height: AppDimensions.friendCellHeight,
            width: MediaQuery.of(context).size.width,
            margin: EdgeInsets.all(10.0),
            child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  ClipOval(
                      child: CachedNetworkImage(
                        width: _imageWidth,
                        height: _imageWidth,
                        fit: BoxFit.fill,
                        imageUrl: widget._friend.image ?? "",
                        placeholder: (context, url) => SizedBox(
                          height: _imageWidth,
                          width: _imageWidth,
                          child: CircularProgressIndicator(strokeWidth: 1,),
                        ),
                        errorWidget: (context, url, error) => Icon(Icons.supervised_user_circle, color: AppColors.TEXT_GREY_BLUISH, size: _imageWidth,),
                      )),
                  Padding(
                    padding: EdgeInsets.only(left: 10.0),
                    child: Column(

                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Text(title, style: TextStyle(color: AppColors.TEXT_BLACK, fontSize: 18.0, fontFamily: 'Avenir'),),
                        SizedBox(height: 5,),
                        Text(widget._friend.status.toUpperCase(), style: TextStyle(color: AppColors.TEXT_GREY_BLUISH, fontSize: 16.0, fontFamily: 'Avenir'),),
                      ],
                    ),
                  ),
                  
                ]
            )
        )
    );
  }
}
