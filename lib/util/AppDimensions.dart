import 'dart:ui';

import 'package:flutter/cupertino.dart';

class AppDimensions{

  static final double appbarTitleSize = 16;
  static final double appbarHeight = 80.0;
  static final double appBarTitleTextSize = 20.0;
  static final double buttonTextSize = 18.0;
  static final double textAreaHeight = 180;
  static final double logoWidth = 400.0;
  static final double logoHeight = 200.0;
  static final double bottomNavBarHeight = 60.0;
  static final double friendCellHeight = 70.0;



  static double buttonHeight(){
    return 50.0;
  }

  static double onboardingContainerHeight(Size screensize) {
    return screensize.height;
  }

  static double eventCellHeightWithPayment(Size screensize) {
    return screensize.height/2.7;
  }

  static double eventCellHeight(Size screensize) {
    return screensize.height/3.5;
  }

  static double buttonWidth(Size screenSize){
    return screenSize.width/1.1;
  }
  static double textFieldHeight(Size screenSize){
    return screenSize.height/15;
  }
  static double imageWidth(Size screenSize) {
    return screenSize.width/8;
  }
}