import 'dart:ui';

import 'package:flutter/material.dart';

class AppColors{
  static const Color CYAN = Color(0xFF00AED9);
  static const Color ORANGE = Color(0xFFFFAA00);
  static const Color TEXT_GREY = Color(0xFF979797);
  static const Color TEXT_GREY_BLUISH = Color(0xFF8F9BB3);
  static const Color TEXT_BLACK = Color(0xFF222B45);
  static const Color MAIN_BG_GREY = Color(0xFFF9F9F9);
  static const Color HINT_COLOR = Color.fromRGBO( 143, 155, 178, 1);
}