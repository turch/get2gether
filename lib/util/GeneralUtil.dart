import 'package:flutter/cupertino.dart';

class GeneralUtil {
  static List<String> getCountryCode(BuildContext context, String phoneNumberWithCountryCode) {
    String countryCode, phoneNumber;
    if(phoneNumberWithCountryCode != null) {
      phoneNumberWithCountryCode = phoneNumberWithCountryCode.replaceAll("-", "");

      if(phoneNumberWithCountryCode.startsWith("+")) {
        if(phoneNumberWithCountryCode.contains("+92")) {
          phoneNumber = phoneNumberWithCountryCode.replaceAll("+92", "").replaceAll(" ","");
          countryCode = "+92";
        }
        else if(phoneNumberWithCountryCode.contains("+1")) {
          phoneNumber = phoneNumberWithCountryCode.replaceAll("+1", "").replaceAll(" ","");
          countryCode = "+1";
        }

      } else {
        countryCode = "+92"; //"+1";
        phoneNumber = phoneNumberWithCountryCode.substring(1);
      }
    }
    return [countryCode, phoneNumber];
  }
}