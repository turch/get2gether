class WebConstants {
  static const String BASE_URL = "http://3.17.141.134/api/";
  static const String BASE_URL_FOR_PAGES = "http://3.17.141.134/";
  static const String SIGN_IN = "login";
  static const String SIGN_UP = "register";
  static const String VERIFY_CODE = "checkVerificationCode";
  static const String RESEND_CODE = "resendVerificationCode";
  static const String FORGOT_PASSWORD = "forgotPasswordLink";
  static const String CREATE_EVENT = "createEvent";
  static const String CREATE_AVAILABILITY_SLOT = "createAvailabilitySlot";
  static const String EVENTS_BY_FILTER = "getEventsWithFilter";
  static const String AVAILABILITY_SLOTS_BY_FILTER = "getAvailabilitySlotsWithFilter";
  static const String USER_EVENTS = "getUserEvents";
  static const String DELETE_EVENT = "deleteEvent";
  static const String GET_ALL_NOTIFICATIONS = "getAllNotifications";
  static const String EDIT_PROFILE = "editUserProfile";
  static const String EDIT_EVENT = "editEvent";
  static const String EDIT_AVAILABILITY_SLOT = "editAvailabilitySlot";
  static const String UPDATE_INVITATION_STATUS = "updateInvitationStatus";
  static const String INVITE_FRIEND = "inviteFriend";
  static const String REMOVE_FRIEND = "removeFriend";
  static const String EVENT_DETAIL = "getEventById";
  static const String GET_FRIENDS_AVAILABILITY = "getFriendsAvailability";
  static const String GET_EVENT_SLOTS = "getEventSlots";
}

class ThirdParties {
  static const String GoogleAPIKey = "AIzaSyCS8j5zOT8zZsmnJFg4jJfzv9PLn5AZWjw";
}