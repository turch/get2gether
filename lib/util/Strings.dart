class Strings {
  static const String welcomeMessage = "Simply connect and make plans\nwith friends.";
  static const String signIn = "SIGN-IN";
  static const String signUp = "SIGN-UP";
  static const String needHelp = "Need help? FAQ";
  static const String titleSignIn = "Sign-in";
  static const String hintSignInEmail = "Email address";
  static const String hintSignInPassword = "Password";
  static const String forgotYourPassword = "Forgot your Password?";
  static const String resetNow = "Reset now";
  static const String dontHaveAnAccount = "Don't have an account?";
  static const String signUpText = "Sign up.";

  static const String hintSignUpFirstName = "First Name";
  static const String hintSignUpLastName = "Last Name";
  static const String hintSignUpCountryCode = "Country Code";
  static const String hintSignUpPhoneNumber = "Phone Number";
  static const String termsAndConditions = "By Continuning you agree to Terms & Conditions and Privacy Policy";
  static const String alreadyHaveAnAccount = "Already have an account?";
  static const String signInText = "Sign in.";
  static const String titlePhoneVerification= "Phone Verification";
  static const String titleForgotPassword= "Forgot Password";

  static const String confirm = "CONFIRM";
  static const String hintCode = "Code";


}