import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class DialogUtil {
  static showDialogBox(BuildContext context, title, message){
    showDialog(
      barrierDismissible: false,
      context: context,
      // ignore: deprecated_member_use
      child: new CupertinoAlertDialog(
        title: new Text(title),
        content: new Text(message),
        actions: <Widget>[
          new FlatButton(
              splashColor: Colors.transparent,
              highlightColor: Colors.transparent,
              onPressed: () {
                Navigator.of(context).pop();
              },
              child: new Text("OK",style: TextStyle(color: Colors.blue),))
        ],
      ),
    );
  }
}