class EventTypes {
  static const String singleDayEvent = 'Single Day Event';
  static const String multiDayEvent = 'Multi-Day Event';
  static const String flexibleEvent = 'Flexible Event';
}