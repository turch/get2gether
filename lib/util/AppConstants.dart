
enum ScreenType {
  SignIn,
  SignUp,
  PhoneVerification,
  ForgotPassword
}

enum Tabs {
  MyEvents,
  Profile,
  Notifications,
  Settings
}

