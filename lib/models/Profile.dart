class Profile {

  int _id;
  String _firstName;
  String _lastName;
  String _image;
  String _email;
  int _isEmailVerified;
  int _isPhoneVerified;
  String _verificationKey;
  String _phone;
  String _countryCode;
  String _createdAt;
  String _updatedAt;
  String _token;
  String _password;
  String _status;

  Profile();

  Profile.fromJson(Map<String,dynamic> json) {
    _id = json["id"];
    _firstName = json["first_name"];
    _lastName = json["last_name"];
    _image = json["image"];
    _email = json["email"];
    _isEmailVerified = json["is_email_verified"];
    _isPhoneVerified = json["is_phone_verified"];
    _verificationKey = json["verification_key"];
    _phone = json["phone"];
    _countryCode = json["country_code"];
    _createdAt = json["created_at"];
    _updatedAt = json["updated_at"];
    if(json.containsKey("token")){
      _token = json["token"];
    }
    if(json.containsKey("password")){
      _password = json["password"];
    }
    _status = json["status"];
  }
  Map<String, dynamic> toJson() => {
    'id': _id,
    'first_name': _firstName,
    'last_name': _lastName,
    'image' : _image,
    'email' :_email,
    'is_email_verified':_isEmailVerified,
    'is_phone_verified':_isPhoneVerified,
    'verification_key':_verificationKey,
    'phone':_phone,
    'country_code':_countryCode,
    'created_at':_createdAt,
    'updated_at':_updatedAt,
    'token':_token,
    'password':_password,
    'staus' : _status
  };

  String get password => _password;

  String get token => _token;

  String get updatedAt => _updatedAt;

  String get createdAt => _createdAt;

  String get countryCode => _countryCode;

  String get phone => _phone;

  String get verificationKey => _verificationKey;

  int get isPhoneVerified => _isPhoneVerified;

  int get isEmailVerified => _isEmailVerified;

  String get email => _email;

  String get image => _image;

  String get lastName => _lastName;

  String get firstName => _firstName;

  int get id => _id;

  String get status => _status;


}