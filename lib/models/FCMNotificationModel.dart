import 'dart:convert';

import 'package:get2gether/models/Event.dart';
import 'package:get2gether/models/Profile.dart';

class FCMNotificationModel {
  String _id;
  String _notificationType;
  String _fromUser;
  String _eventId;
  String _createdAt;
  String _updatedAt;
  Profile _profile;
  Event _event;

  FCMNotificationModel.fromJson(Map<String,dynamic> responsJson) {
    try{
      _id = responsJson["id"];
      _notificationType = responsJson["notification_type"];
      _fromUser = responsJson["from_user"];
      _eventId = responsJson["event_id"];
      _createdAt = responsJson["created_at"];
      _updatedAt = responsJson["updated_at"];
      var profileMap = new Map<String, dynamic>.from(json.decode(responsJson["profile"]));
      var eventMap = new Map<String, dynamic>.from(json.decode(responsJson["event"]));
      _profile = Profile.fromJson(profileMap);
      _event = Event.fromJson(eventMap);
    } catch(e) {
      print(e);
    }

  }

  Event get event => _event;

  Profile get profile => _profile;

  String get updatedAt => _updatedAt;

  String get createdAt => _createdAt;

  String get eventId => _eventId;

  String get fromUser => _fromUser;

  String get notificationType => _notificationType;

  String get id => _id;


}
class NOTIFICATION_TYPE {
  static String event_invitation = "event_invitation";
  static String event_paid = "event_paid";
  static String event_accepted = "event_accepted";
  static String event_rejected = "event_rejected";
  static String event_expired = "event_expired";

}


