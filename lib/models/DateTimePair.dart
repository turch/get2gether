class DateTimePair {
  String _date;
  String _time;

  DateTimePair(this._date, this._time);

  DateTimePair.fromJson(Map<String,dynamic> json) {
    _date = json["date"];
    _time = json["time"];
  }

  String get date => _date;
  String get time => _time;

  Map<String, dynamic> toMap() {
    return {
      "date" : "${_date}",
      "time": "${_time}",
    };
  }
}