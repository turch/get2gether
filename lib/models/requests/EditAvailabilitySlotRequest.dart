import 'package:get2gether/models/AvailabilitySlot.dart';
import 'package:get2gether/models/Event.dart';
import 'package:get2gether/models/GeneralResponse.dart';

class EditAvailabilitySlotRequest {

  int _availabilitySlotId;
  String _date, _startTime, _endTime;

  EditAvailabilitySlotRequest(this._availabilitySlotId, this._date, this._startTime, this._endTime);

  Map<String, dynamic> toMap() {
    return {
      "availability_slot_id" : _availabilitySlotId,
      "date": _date,
      "start_time": _startTime,
      "end_time": _endTime,
    };
  }

  int get eventId => _availabilitySlotId;
  get date => _date;
  get startTime => _startTime;
  get endTime => _endTime;
}

class EditAvailabilitySlotResponse extends GeneralResponse{
  AvailabilitySlot _availabilitySlot;
  EditAvailabilitySlotResponse.fromJson(Map<String,dynamic> json) : super.fromJson(json){
    if(json.containsKey("availabilitySlot")) {
      _availabilitySlot = AvailabilitySlot.fromJson(json["availabilitySlot"]);
    }
  }

  AvailabilitySlot get availabilitySlot => _availabilitySlot;

}