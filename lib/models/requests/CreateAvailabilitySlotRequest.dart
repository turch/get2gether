import 'package:get2gether/models/AvailabilitySlot.dart';
import 'package:get2gether/models/GeneralResponse.dart';

class CreateAvailabilitySlotRequest {
  int _user_id;
  String _date, _startTime, _endTime;

  CreateAvailabilitySlotRequest(this._user_id, this._date, this._startTime, this._endTime);

  Map<String, dynamic> toMap() {
    return {
      "user_id" : _user_id,
      "date": _date,
      "start_time": _startTime,
      "end_time": _endTime,
    };
  }

  get userId => _user_id;
  get date => _date;
  get startTime => _startTime;
  get endTime => _endTime;
}

class CreateAvailabilitySlotResponse extends GeneralResponse {
  AvailabilitySlot _availabilitySlot;
  CreateAvailabilitySlotResponse.fromJson(Map<String, dynamic> json) : super.fromJson(json) {
    if(json.containsKey("availability_slot")) {
      _availabilitySlot = AvailabilitySlot.fromJson(json["availability_slot"]);
    }
  }

  AvailabilitySlot get availabilitySlot => _availabilitySlot;
}