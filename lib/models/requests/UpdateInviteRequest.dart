import 'package:get2gether/models/Event.dart';
import 'package:get2gether/models/GeneralResponse.dart';

class UpdateInviteRequest {

  int _eventId;
  int _userId;
  String _status;
  String _selectedDate;
  String _selectedTime;

  UpdateInviteRequest(this._eventId, this._userId, this._status, this._selectedDate, this._selectedTime);

  Map<String, dynamic> toMap() {
    return {
      "event_id" : _eventId,
      "user_id": _userId,
      "status": _status,
      "selected_date": _selectedDate,
      "selected_time": _selectedTime,
    };
  }

  String get status => _status;

  int get userId => _userId;

  int get eventId => _eventId;

  String get selectedDate => _selectedDate;

  String get selectedTime => _selectedTime;
}