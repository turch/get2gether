import '../AvailabilitySlot.dart';
import '../GeneralResponse.dart';

class AvailabilitySlotsRequestByFilter {
  String _type, _value, _year;

  AvailabilitySlotsRequestByFilter(this._type, this._value, this._year)
  {
    var test = '';
  }

  get value => _value;
  String get type => _type;
  String get year => _year;

}

class AvailabilitySlotsResponseByFilter extends GeneralResponse {
  List<AvailabilitySlot> _availabilitySlots;
  AvailabilitySlotsResponseByFilter.fromJson(Map<String,dynamic> json) : super.fromJson(json){

    if(json.containsKey("availability_slots")) {
      List<AvailabilitySlot> availabilitySlots = new List();
      json["availability_slots"].forEach((availabilitySlot) {
        availabilitySlots.add(AvailabilitySlot.fromJson(availabilitySlot));
      });
      _availabilitySlots = availabilitySlots;
    }
  }

  List<AvailabilitySlot> get availabilitySlots => _availabilitySlots;
}
