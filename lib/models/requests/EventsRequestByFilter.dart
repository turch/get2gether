

import '../Event.dart';
import '../GeneralResponse.dart';

class EventsRequestByFilter {

  String _type, _value, _year;

  EventsRequestByFilter(this._type, this._value, this._year);

  get value => _value;

  String get type => _type;

  String get year => _year;

}

class EventsResponseByFilter extends GeneralResponse {

  List<Event> _events;
  EventsResponseByFilter.fromJson(Map<String,dynamic> json) : super.fromJson(json){

    if(json.containsKey("events")) {
      List<Event> events = new List();
      json["events"].forEach((event) {
        events.add(Event.fromJson(event));
      });
      _events = events;
    }
  }

  List<Event> get events => _events;

}
