import 'package:get2gether/models/Event.dart';
import 'package:get2gether/models/GeneralResponse.dart';
import 'package:get2gether/models/Profile.dart';

class SignupRequest{
  String _email,_password, _firstName, _lastName, _countryCode, _phoneNumber;


  SignupRequest(this._email, this._password, this._firstName, this._lastName,
      this._countryCode, this._phoneNumber);

  get phoneNumber => _phoneNumber;

  get countryCode => _countryCode;

  get lastName => _lastName;

  get firstName => _firstName;

  get password => _password;

  String get email => _email;


}
