import 'package:get2gether/models/Event.dart';
import 'package:get2gether/models/GeneralResponse.dart';
import 'package:get2gether/models/Profile.dart';

class ForgotPasswordRequest{
  String _email;

  ForgotPasswordRequest(this._email);

  String get email => _email;


}
