import 'package:get2gether/models/Event.dart';
import 'package:get2gether/models/GeneralResponse.dart';
import 'package:get2gether/models/Profile.dart';

class ResendCodeRequest {
  String _countryCode, _phoneNumber;

  ResendCodeRequest(this._countryCode, this._phoneNumber);

  get phoneNumber => _phoneNumber;

  String get countryCode => _countryCode;


}
