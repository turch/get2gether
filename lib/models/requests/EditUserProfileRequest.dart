import 'dart:io';

import '../GeneralResponse.dart';
import '../Profile.dart';

class EditUserProfileRequest{

  int _userId;
  String _firstName, _lastName;
  File _image;


  EditUserProfileRequest(this._userId, this._firstName, this._lastName,
      this._image);

  Map<String, dynamic> toMap() {
    return {
      "user_id": "${_userId}",
      "first_name": "${_firstName}",
      "last_name": "${_lastName}",
      "image": _image,
    };
  }

  File get image => _image;

  get lastName => _lastName;

  get firstName => _firstName;

  int get userId => _userId;

}

class EditUserProfileResponse extends GeneralResponse{

  Profile _profile;
  EditUserProfileResponse.fromJson(Map<String,dynamic> json) : super.fromJson(json){
    if(json.containsKey("profile")) {
      _profile = Profile.fromJson(json["profile"]);
    }
  }

  Profile get profile => _profile;
}
