

class InviteFriendRequest{

  String _phone, _countryCode, _name, _email;
  int _eventId;


  InviteFriendRequest(this._name, this._phone, this._countryCode, this._eventId, this._email);

  int get eventId => _eventId;

  get countryCode => _countryCode;

  String get phone => _phone;

  get name => _name;

  get email => _email;


}