import '../AvailabilitySlot.dart';
import '../GeneralResponse.dart';

class FriendsAvailabilityByFilterRequest {
  String _type, _value, _year;
  List<String> _friends;

  FriendsAvailabilityByFilterRequest(this._type, this._value, this._year, this._friends);

  get value => _value;
  String get type => _type;
  String get year => _year;
  List<String> get friends => _friends;
}

class FriendsAvailabilityByFilterResponse extends GeneralResponse {
  List<AvailabilitySlot> _availabilitySlots;
  FriendsAvailabilityByFilterResponse.fromJson(Map<String,dynamic> json) : super.fromJson(json){

    if(json.containsKey("availability_slots")) {
      List<AvailabilitySlot> availabilitySlots = new List();
      json["availability_slots"].forEach((event) {
        availabilitySlots.add(AvailabilitySlot.fromJson(event));
      });
      _availabilitySlots = availabilitySlots;
    }
  }

  List<AvailabilitySlot> get availabilitySlots => _availabilitySlots;
}
