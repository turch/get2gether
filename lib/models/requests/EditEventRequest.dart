import 'package:get2gether/models/Event.dart';
import 'package:get2gether/models/DateTimePair.dart';
import 'package:get2gether/models/GeneralResponse.dart';

class EditEventRequest {

  int _eventId;
  String _title, _shortDescription, _address, _deadline, _notificationTime, _paymentEmail;
  int _perHeadContribution, _adminId, _paymentRequired;
  List<DateTimePair> _dateTimeList;
  double _latitude, _longitude;


  EditEventRequest(this._eventId, this._title, this._shortDescription,
      this._dateTimeList, this._address, this._deadline,
      this._notificationTime, this._perHeadContribution, this._adminId, this._paymentRequired,
      this._latitude, this._longitude, this._paymentEmail);

  Map<String, dynamic> toMap() {

    return {
      "event_id" : _eventId,
      "title": "${_title}",
      "short_description": "${_shortDescription}",
      "per_head_contribution": _perHeadContribution,
      "dateTimeList": "${_dateTimeList}",
      "latitude": _latitude,
      "longitude": _longitude,
      "address": "${_address}",
      "deadline": "${_deadline}",
      "admin_id":_adminId,
      "payment_required": _paymentRequired,
      "notification_time" : "${_notificationTime}",
      "payment_email": "${_paymentEmail}"
    };
  }


  String get title => _title;

  get shortDescription => _shortDescription;

  get dateTimeList => _dateTimeList;

  get address => _address;

  get deadline => _deadline;

  get notificationTime => _notificationTime;

  int get perHeadContribution => _perHeadContribution;

  get adminId => _adminId;

  get paymentRequired => _paymentRequired;

  double get latitude => _latitude;

  get longitude => _longitude;

  int get eventId => _eventId;

  get paymentEmail => _paymentEmail;


}

class EditEventResponse extends GeneralResponse{
  Event _event;
  EditEventResponse.fromJson(Map<String,dynamic> json) : super.fromJson(json){
    if(json.containsKey("event")) {
      _event = Event.fromJson(json["event"]);
    }
  }

  Event get event => _event;

}