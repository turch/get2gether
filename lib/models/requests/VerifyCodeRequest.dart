import 'package:get2gether/models/Event.dart';
import 'package:get2gether/models/GeneralResponse.dart';
import 'package:get2gether/models/Profile.dart';

class VerifyCodeRequest{
  String _deviceId, _deviceType, fcmToken, _countryCode, _phoneNumber, _verificationCode;

  VerifyCodeRequest(this._deviceId, this._deviceType, this.fcmToken,
      this._countryCode, this._phoneNumber, this._verificationCode);

  get verificationCode => _verificationCode;

  get phoneNumber => _phoneNumber;

  get countryCode => _countryCode;

  get deviceType => _deviceType;

  String get deviceId => _deviceId;


}
