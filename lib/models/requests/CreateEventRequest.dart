import 'package:flutter/cupertino.dart';
import 'package:get2gether/models/Event.dart';
import 'package:get2gether/models/GeneralResponse.dart';
import 'package:get2gether/models/DateTimePair.dart';
import 'package:get2gether/models/Invitation.dart';
import 'package:get2gether/models/Profile.dart';

class CreateEventRequest{

  String _title, _shortDescription, _eventType, _address, _deadline, _notificationTime, _paymentEmail;
  int _perHeadContribution, _adminId, _paymentRequired;
  double _latitude, _longitude;
  List<DateTimePair> _dateTimeList;
  List<Invitation> _invitations;


  CreateEventRequest(this._title, this._shortDescription, this._eventType,
      this._dateTimeList, this._address, this._deadline, this._notificationTime,
      this._perHeadContribution, this._adminId, this._paymentRequired, this._latitude,
      this._longitude, this._paymentEmail, this._invitations);


  Map<String, dynamic> toMap() {
    List<Map<String, dynamic>> dateTimeList = _dateTimeList
        .map((dateTimePair) => dateTimePair.toMap())
        .toList();

    List<Map<String, dynamic>> invitationList = _invitations
        .map((invitation) => invitation.toMap())
        .toList();

    return {
      "title": "${_title}",
      "short_description": "${_shortDescription}",
      "per_head_contribution": _perHeadContribution,
      "event_type": _eventType,
      "date_time_list": dateTimeList,
      "latitude": _latitude,
      "longitude": _longitude,
      "address": "${_address}",
      "deadline": "${_deadline}",
      "admin_id":_adminId,
      "payment_required": _paymentRequired,
      "notification_time" : "${_notificationTime}",
      "payment_email" : "${_paymentEmail}",
      "invitations": invitationList
    };
  }


  String get title => _title;

  get shortDescription => _shortDescription;

  get eventType => _eventType;

  get dateTimeList => _dateTimeList;

  get address => _address;

  get deadline => _deadline;

  get notificationTime => _notificationTime;

  int get perHeadContribution => _perHeadContribution;

  get adminId => _adminId;

  get paymentRequired => _paymentRequired;

  double get latitude => _latitude;

  get longitude => _longitude;


  get paymentEmail => _paymentEmail;

  List<Invitation> get invitations => _invitations;


}

class CreateEventResponse extends GeneralResponse{
  Event _event;
  CreateEventResponse.fromJson(Map<String,dynamic> json) : super.fromJson(json){
    if(json.containsKey("event")) {
      _event = Event.fromJson(json["event"]);
    }
  }

  Event get event => _event;

}