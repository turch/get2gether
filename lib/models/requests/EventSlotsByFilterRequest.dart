import '../AvailabilitySlot.dart';
import '../GeneralResponse.dart';

class EventSlotsByFilterRequest {
  String _type, _value, _year;

  EventSlotsByFilterRequest(this._type, this._value, this._year);

  get value => _value;
  String get type => _type;
  String get year => _year;
}

class EventSlotsByFilterResponse extends GeneralResponse {
  List<AvailabilitySlot> _eventDates;
  EventSlotsByFilterResponse.fromJson(Map<String,dynamic> json) : super.fromJson(json){

    if(json.containsKey("availability_slots")) {
      List<AvailabilitySlot> eventDates = new List();
      json["availability_slots"].forEach((event) {
        eventDates.add(AvailabilitySlot.fromJson(event));
      });
      _eventDates = eventDates;
    }
  }

  List<AvailabilitySlot> get eventDates => _eventDates;
}
