

import 'package:get2gether/models/NotificationModel.dart';

import '../GeneralResponse.dart';

class NotificationsRequest {

  int _pageSize, _pageNumber;

  NotificationsRequest(this._pageSize, this._pageNumber);

  get pageNumber => _pageNumber;

  int get pageSize => _pageSize;


}

class NotificationsResponse extends GeneralResponse {

  List<NotificationModel> _notifications;
  NotificationsResponse.fromJson(Map<String,dynamic> json) : super.fromJson(json){

    if(json.containsKey("notifications")) {
      List<NotificationModel> notifications = new List();
      json["notifications"].forEach((notification) {
        notifications.add(NotificationModel.fromJson(notification));
      });
      _notifications = notifications;
    }
  }

  List<NotificationModel> get notifications => _notifications;

}
