

import '../Event.dart';
import '../GeneralResponse.dart';

class UserEventsRequest {
  int _userId;

  UserEventsRequest(this._userId);

  int get userId => _userId;

}

class UserEventsResponse extends GeneralResponse {

  List<Event> _events;
  UserEventsResponse.fromJson(Map<String,dynamic> json) : super.fromJson(json){

    if(json.containsKey("events")) {
      List<Event> events = new List();
      json["events"].forEach((event) {
        events.add(Event.fromJson(event));
      });
      _events = events;
    }
  }

  List<Event> get events => _events;

}
