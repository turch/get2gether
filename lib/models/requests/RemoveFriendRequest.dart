

class RemoveFriendRequest{

  int _eventId, _userId;

  RemoveFriendRequest(this._eventId, this._userId);

  get userId => _userId;

  int get eventId => _eventId;


}