import 'package:get2gether/models/Event.dart';
import 'package:get2gether/models/GeneralResponse.dart';
import 'package:get2gether/models/Profile.dart';

class SigninRequest{
  String _email,_password, _deviceId, _deviceType, fcmToken;


  SigninRequest(this._email, this._password, this._deviceId, this._deviceType,
      this.fcmToken);

  get deviceType => _deviceType;

  get deviceId => _deviceId;

  get password => _password;

  String get email => _email;


}

class SigninResponse extends GeneralResponse{

  Profile _profile;
  List<Event> _events;
  SigninResponse.fromJson(Map<String,dynamic> json) : super.fromJson(json){
    if(json.containsKey("profile")) {
      _profile = Profile.fromJson(json["profile"]);
    }

    if(json.containsKey("events")) {
      List<Event> events = new List();
      json["events"].forEach((event) {
        events.add(Event.fromJson(event));
      });
      _events = events;
    }
  }

  Profile get profile => _profile;
  List<Event> get events => _events;

}