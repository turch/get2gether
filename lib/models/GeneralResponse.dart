class GeneralResponse {
  int _status;
  String _message;

  GeneralResponse.fromJson(Map<String, dynamic> json) {
    _status = json["status"];
    _message = json["message"];
  }

  get message => _message;

  get status => _status;

}