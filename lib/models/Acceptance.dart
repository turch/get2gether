
import 'package:get2gether/models/DateTimePair.dart';

class Acceptance {
  String _name;
  String _email;
  String _phoneNumber;
  String _countryCode;
  DateTimePair _dateTime;

  Acceptance(this._name, this._email, this._phoneNumber,
      this._countryCode, this._dateTime);

  Acceptance.fromJson(Map<String,dynamic> json) {
    if(json.containsKey("email")) {
      _email = json["email"];
      _name = json["name"];
      _dateTime = DateTimePair.fromJson(json["date_time"]);
    } else {
      _name = json["name"];
      _phoneNumber = json["phone"];
      _countryCode = json["country_code"];
      _dateTime = DateTimePair.fromJson(json["date_time"]);
    }
  }

  String get countryCode => _countryCode;
  String get phoneNumber => _phoneNumber;
  String get email => _email;
  DateTimePair get dateTime => _dateTime;

  Map<String, dynamic> toMap() {
    return {
      "name" : "${_name}",
      "email" : "${_email}",
      "phone": "${_phoneNumber}",
      "country_code": "${_countryCode}",
      "availability_slot_id": "${dateTime}"
    };
  }
}