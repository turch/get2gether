class AvailabilitySlot {
  int _id;
  String _date;
  String _startTime;
  String _endTime;
  String _createAt;
  String _updatedAt;
  String _owner;
  int _eventId;

  AvailabilitySlot.fromJson(Map<String,dynamic> json) {
    _id = json["id"];
    _date = json["date"];
    _startTime = json["start_time"];
    _endTime = json["end_time"];
    _createAt = json["created_at"];
    _updatedAt = json["updated_at"];
    _owner = json["owner"];
    _eventId = json["event_id"];
  }

  String get date => _date;

  String get startTime => _startTime;

  String get endTime => _endTime;

  String get createAt => _createAt;

  String get updatedAt => _updatedAt;

  String get owner => _owner;

  int get eventId => _eventId;

  int get id => _id;
}