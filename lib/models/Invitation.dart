
class Invitation {
  INVITATION_TYPE _type;
  String _name;
  String _email;
  String _phoneNumber;
  String _countryCode;


  Invitation(this._type, this._name, this._email, this._phoneNumber,
      this._countryCode);

  Invitation.fromJson(Map<String,dynamic> json) {
    if(json.containsKey("email")) {
      _email = json["email"];
      _name = json["name"];
      _type = INVITATION_TYPE.Email;
    } else {
      _name = json["name"];
      _phoneNumber = json["phone"];
      _countryCode = json["country_code"];
      _type = INVITATION_TYPE.Phone;
    }
  }

  String get countryCode => _countryCode;
  String get phoneNumber => _phoneNumber;
  String get email => _email;

  INVITATION_TYPE get type => _type;

  Map<String, dynamic> toMap() {
    if (_type == INVITATION_TYPE.Email) {
      return {
        "name" : "${_name}",
        "email": "${_email}"
      };
    }
    else {
      return {
        "name" : "${_name}",
        "phone": "${_phoneNumber}",
        "country_code": "${_countryCode}"
      };
    }
  }

}

enum INVITATION_TYPE {
  Email,
  Phone,
}