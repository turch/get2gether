import 'package:get2gether/models/Event.dart';
import 'package:get2gether/models/Profile.dart';

class NotificationModel {
  int _id;
  String _notificationType;
  int _fromUser;
  int _eventId;
  String _createdAt;
  String _updatedAt;
  Profile _profile;
  Event _event;

  NotificationModel.fromJson(Map<String,dynamic> json) {
    _id = json["id"];
    _notificationType = json["notification_type"];
    _fromUser = json["from_user"];
    _eventId = json["event_id"];
    _createdAt = json["created_at"];
    _updatedAt = json["updated_at"];
    _profile = Profile.fromJson(json["profile"]);
    _event = Event.fromJson(json["event"]);
  }

  Event get event => _event;

  Profile get profile => _profile;

  String get updatedAt => _updatedAt;

  String get createdAt => _createdAt;

  int get eventId => _eventId;

  int get fromUser => _fromUser;

  String get notificationType => _notificationType;

  int get id => _id;


}
class NOTIFICATION_TYPE {
  static String event_invitation = "event_invitation";
  static String event_paid = "event_paid";
  static String event_accepted = "event_accepted";
  static String event_rejected = "event_rejected";
  static String event_expired = "event_expired";

}


