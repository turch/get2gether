import 'package:get2gether/models/AvailabilitySlot.dart';

class AvailabilitySlotRepository {
  List<AvailabilitySlot> _availabilitySlots = List();

  void addAvailabilitySlot(AvailabilitySlot availabilitySlot) {
    _availabilitySlots.insert(0, availabilitySlot);
  }

  Stream<AvailabilitySlot> availabilitySlots() {
    return Stream.fromIterable(_availabilitySlots);
  }
}