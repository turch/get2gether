class ArgumentGetVerificationCode{
  final String title;
  final String codeReceiver;
  final String route;

  ArgumentGetVerificationCode(this.title, this.codeReceiver, this.route);

}