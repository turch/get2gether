import 'package:get2gether/models/DateTimePair.dart';
import 'package:get2gether/models/Invitation.dart';
import 'package:get2gether/models/DateTimePair.dart';
import 'package:get2gether/models/Profile.dart';
import 'package:get2gether/models/Acceptance.dart';

class Event {
  int _id;
  String _title;
  String _shortDescription;
  String _eventType;
  int _perHeadContribution;
  double _latitude;
  double _longitude;
  String _address;
  String _deadline;
  int _adminId;
  int _paymentRequired;
  String _notificationTime;
  String _createAt;
  String _updatedAt;
  String _status;
  String _paymentEmail;
  List<DateTimePair> _dateTimeList;
  List<Profile> _invitations;
  List<Acceptance> _acceptances;

  Event.fromJson(Map<String,dynamic> json) {
    _id = json["id"];
    _title = json["title"];
    _shortDescription = json["short_description"];
    _eventType = json["event_type"];
    _perHeadContribution = json["per_head_contribution"];
    _latitude = json["latitude"];
    _longitude = json["longitude"];
    _address = json["address"];
    _deadline = json["deadline"];
    _adminId = json["admin_id"];
    _paymentRequired = json["payment_required"];
    _notificationTime = json["notification_time"];
    _createAt = json["created_at"];
    _updatedAt = json["updated_at"];
    _status = json["status"];
    _paymentEmail = json["payment_email"];
    List<DateTimePair> dateTimes = new List();
    List<Profile> invitations = new List();
    List<Acceptance> acceptances = new List();

    json["date_time_list"].forEach((dateTime) {
      dateTimes.add(DateTimePair.fromJson(dateTime));
    });
    _dateTimeList = dateTimes;

    json["invitations"].forEach((invitation) {
      invitations.add(Profile.fromJson(invitation));
    });
    _invitations = invitations;

    json["acceptances"].forEach((acceptance) {
      acceptances.add(Acceptance.fromJson(acceptance));
    });
    _acceptances = acceptances;
  }

  List<DateTimePair> get dateTimeList => _dateTimeList;
  List<Profile> get invitations => _invitations;
  List<Acceptance> get acceptances => _acceptances;

  String get notificationTime => _notificationTime;

  int get paymentRequired => _paymentRequired;

  int get adminId => _adminId;

  String get deadline => _deadline;

  String get address => _address;

  double get longitude => _longitude;

  double get latitude => _latitude;

  int get perHeadContribution => _perHeadContribution;

  String get shortDescription => _shortDescription;

  String get eventType => _eventType;

  String get title => _title;

  String get updatedAt => _updatedAt;

  String get status => _status;

  String get createAt => _createAt;


  String get paymentEmail => _paymentEmail;

  int get id => _id;


}