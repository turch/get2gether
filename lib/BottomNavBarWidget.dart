
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get2gether/util/AppColors.dart';

class BottomNavBarWidget extends StatefulWidget {

  Function callback;
  int selectedTab;
  @override
  _BottomNavBarWidgetState createState() => _BottomNavBarWidgetState();

  BottomNavBarWidget(this.callback, {this.selectedTab = 0});

}

class _BottomNavBarWidgetState extends State<BottomNavBarWidget> {



  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  Widget build(BuildContext context) {
    return new Row(
      mainAxisSize: MainAxisSize.max,
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        IconButton(icon: Icon(Icons.event, size: 25.0), color: getColorForTab(0, widget.selectedTab),
          onPressed: () {
            widget.callback(0);
          },
        ),
        IconButton(icon: Icon(Icons.contacts, size: 25.0, color: getColorForTab(1, widget.selectedTab)),   onPressed: () {
          widget.callback(1);
        },
        ),
        SizedBox(width: 50.0,),
        IconButton(icon: Icon(Icons.notifications, size: 25.0, color: getColorForTab(2, widget.selectedTab)),   onPressed: () {
          widget.callback(2);
        },
        ),
        IconButton(icon: Icon(Icons.settings, size: 25.0, color: getColorForTab(3, widget.selectedTab)),   onPressed: () {
          widget.callback(3);
        },
        ),
      ],
    );
  }

  Color getColorForTab(int tab, int selectedTab) {
    if(tab == selectedTab) {
      return AppColors.CYAN;
    } else {
      return AppColors.TEXT_GREY;
    }

  }

}



