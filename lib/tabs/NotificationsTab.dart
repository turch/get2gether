import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get2gether/NotificationCell.dart';
import 'package:get2gether/bloc/notifications/NotificationsBloc.dart';
import 'package:get2gether/bloc/notifications/notifications_event.dart';
import 'package:get2gether/bloc/notifications/notifications_state.dart';
import 'package:get2gether/models/requests/NotificationsRequest.dart';
import 'package:get2gether/util/AppColors.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class NotificationsTab extends StatefulWidget {
  NotificationsBloc notificationsBloc;
  @override
  _NotificationsTabState createState() => _NotificationsTabState();

  NotificationsTab(this.notificationsBloc);


}

class _NotificationsTabState extends State<NotificationsTab>{

  final _scrollController = ScrollController();
  final _scrollThreshold = 200.0;
  var _pageNumber = 1;
  var _pageSize = 20;

  @override
  void initState() {
    _scrollController.addListener(_onScroll);

    widget.notificationsBloc.add(new FetchNotifications(new NotificationsRequest(_pageSize, _pageNumber)));
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }
  RefreshController _refreshController = RefreshController(initialRefresh: false);

  void _onRefresh() async {
    _pageNumber = 1;
    widget.notificationsBloc.add(new FetchNotifications(new NotificationsRequest(_pageSize, _pageNumber)));
    _refreshController.refreshCompleted();
  }

  void _onLoading() async{
//    _refreshController.loadComplete();
  }


  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: AppColors.MAIN_BG_GREY,
        appBar: AppBar(
          elevation: 1.0,
          automaticallyImplyLeading: false,
          backgroundColor: AppColors.CYAN,
          centerTitle: true,
          title: Text("Notifications", style: TextStyle(color: Colors.white),),
        ),
        body: SmartRefresher(
            enablePullDown: true,
            enablePullUp: true,
            header: WaterDropHeader(),
            controller: _refreshController,
            onRefresh: _onRefresh,
            onLoading: _onLoading,
            child: BlocBuilder(
              bloc: widget.notificationsBloc,
              builder: (BuildContext context, NotificationsBlocState state) {
                if (state is NotificationsUninitialized) {
                  return Center(
                    child: CircularProgressIndicator(),
                  );
                }
                if (state is NotificationsError) {
                  return Center(
                    child: Text(state.error),
                  );
                }
                if (state is NotificationsLoaded) {
                  if (state.notifications.isEmpty) {
                    return Center(
                      child: Text('No notifications.'),
                    );
                  }
                  return ListView.builder(
                    itemBuilder: (BuildContext context, int index) {
                      return index >= state.notifications.length
                          ? BottomLoader()
                          : NotificationCell(state.notifications[index]);
                    },
                    itemCount: state.hasReachedMax || state.notifications.length < _pageSize? state.notifications.length : state.notifications.length + 1,
                    controller: _scrollController,
                  );
                } else {
                  return Container();
                }
              },
            )
        )

    );
  }

  void _onScroll() {
    final maxScroll = _scrollController.position.maxScrollExtent;
    final currentScroll = _scrollController.position.pixels;
    if (maxScroll - currentScroll <= _scrollThreshold) {
      widget.notificationsBloc.add(new FetchNotifications(new NotificationsRequest(_pageSize, ++_pageNumber)));
    }
  }
}

class BottomLoader extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.center,
      child: Center(
        child: SizedBox(
          width: 33,
          height: 33,
          child: CircularProgressIndicator(
            strokeWidth: 1.5,
          ),
        ),
      ),
    );
  }
}