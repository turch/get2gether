import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get2gether/UIComponents/FormTextControlWithAction.dart';
import 'package:get2gether/bloc/editUserProfile/editUserProfileBloc.dart';
import 'package:get2gether/bloc/editUserProfile/edituserprofile_event.dart';
import 'package:get2gether/models/requests/EditUserProfileRequest.dart';
import 'package:get2gether/resources/AppData.dart';
import 'package:get2gether/util/AppColors.dart';
import 'package:get2gether/util/AppConstants.dart';
import 'package:get2gether/util/SharedPrefHelper.dart';
import 'package:image_picker/image_picker.dart';

class SettingsTab extends StatefulWidget {

  @override
  _SettingsTabState createState() => _SettingsTabState();
}

class _SettingsTabState extends State<SettingsTab>{

  bool reminderSwitch = false;
  bool notificationSwitch = false;
  EditUserProfileBloc _editUserProfileBloc;
  TextEditingController _firstNameController = TextEditingController();
  TextEditingController _lastNameController = TextEditingController();
  File _image;

  Future getImage() async {
    var image = await ImagePicker.pickImage(source: ImageSource.camera);
    _editUserProfileBloc.add(new FormSubmitted(UpdateRequest.image, new EditUserProfileRequest(AppData.currentUser.id, null, null, image)));
    setState(() {
      _image = image;
    });

  }

  @override
  void initState() {
    _editUserProfileBloc = EditUserProfileBloc();
    SharedPreferencesHelper.getValueByKey(SharedPreferenceKeys.NOTIFICATION).then((onValue) {
      setState(() {
        notificationSwitch = onValue;
      });
    });
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: AppColors.MAIN_BG_GREY,
        appBar: AppBar(
          elevation: 1.0,
          automaticallyImplyLeading: false,
          backgroundColor: AppColors.CYAN,
          centerTitle: true,
          title: Text("Settings", style: TextStyle(color: Colors.white),),
        ),
        body: SingleChildScrollView(
            child : Column(
              children: <Widget>[
                SizedBox(height: 10),
                Container(
                    height: 40,
                    decoration: BoxDecoration(
                        color: Colors.grey
                    ),
                    child: Align(
                      alignment: Alignment(-0.8, 0),
                      child: Text('Notifications Settings', textAlign: TextAlign.left, style: TextStyle(color: Colors.white, fontSize: 17.0),),
                    )
                ),
                Container(
                  height: 0.3,
                  color: AppColors.TEXT_GREY,
                ),
                Padding(
                  padding: EdgeInsets.only(left:20.0, right: 20.0, top: 5.0, bottom: 5.0),
                  child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        new Text(
                          "Notifications",
                          style: TextStyle(color: AppColors.TEXT_BLACK, fontSize: 16.0),
                        ),
                        Switch(value: notificationSwitch, onChanged: (newVal) {
                          setState(() {
                            notificationSwitch = newVal;
                            SharedPreferencesHelper.setValue(SharedPreferenceKeys.NOTIFICATION, newVal);
                            AppData.notificationSwitch = newVal;
                          });
                        },)
                      ]
                  ),
                ),
                Container(
                  height: 0.3,
                  color: AppColors.TEXT_GREY,
                ),
                Container(
                    height: 40,
                    decoration: BoxDecoration(
                        color: Colors.grey
                    ),
                    child: Align(
                      alignment: Alignment(-0.8, 0),
                      child: Text('Profile', textAlign: TextAlign.left, style: TextStyle(color: Colors.white, fontSize: 17.0),),
                    )
                ),
                Padding(
                  padding: EdgeInsets.all(10.0),
                  child: Column(
                    children: <Widget>[
                      SizedBox(height: 10.0,),
                      GestureDetector(
                          onTap: () => getImage(),
                          child: Center(
                              child: ClipOval(
                                  child: _image != null ? Image.file(
                                    _image, height: 80.0, width: 80.0, fit: BoxFit.fill,)
                                      :CachedNetworkImage(
                                    width: 80.0,
                                    height: 80.0,
                                    fit: BoxFit.fill,
                                    imageUrl: AppData.currentUser.image ?? "",
                                    placeholder: (context, url) =>
                                        SizedBox(
                                          height: 80.0,
                                          width: 80.0,
                                          child: CircularProgressIndicator(strokeWidth: 1,),
                                        ),
                                    errorWidget: (context, url, error) =>
                                        Icon(Icons.supervised_user_circle,
                                          color: AppColors.TEXT_GREY_BLUISH, size: 80.0,),
                                  )
                              )
                          )
                      ),
                      SizedBox(height: 10.0,),
                      FormTextControlWithAction("Alen", MediaQuery.of(context).size.width, null, icon: Icons.edit,
                        textController: _firstNameController, enabled: false, action: ACTION_TYPE.edit, editCallback: _saveFirstName,
                        text: AppData.currentUser.firstName,),
                      FormTextControlWithAction("Mchgrat", MediaQuery.of(context).size.width, null ,icon: Icons.edit,
                          textController: _lastNameController, enabled: false, action: ACTION_TYPE.edit, editCallback: _saveLastName,
                          text: AppData.currentUser.lastName),
                      FormTextControlWithAction(AppData.currentUser.email, MediaQuery.of(context).size.width, null, enabled: false),
                      FormTextControlWithAction(
                          AppData.currentUser.countryCode + AppData.currentUser.phone,
                          MediaQuery
                              .of(context)
                              .size
                              .width, null, text: "+1 234-45313432", enabled: false)
                    ],
                  ),
                ),
                GestureDetector(
                  onTap: (){
                    SharedPreferencesHelper.removeValueByKey(SharedPreferenceKeys.USER_LOGGED_IN);
                    SharedPreferencesHelper.removeValueByKey(SharedPreferenceKeys.USER);
                    Navigator.of(context).pushNamedAndRemoveUntil(
                        '/Onboarding', (Route<dynamic> route) => false, arguments: ScreenType.SignIn);

                  },
                  child: Container(
                      height: 40,
                      decoration: BoxDecoration(
                          color: Colors.white
                      ),
                      child: Align(
                        alignment: Alignment(-0.8, 0),
                        child: Text('Logout', textAlign: TextAlign.left, style: TextStyle(color: Colors.red, fontSize: 17.0),),
                      )
                  ),
                ),
                SizedBox(height: 30.0,)
              ],
            )

        )
    );

  }

  _saveFirstName(String fName) {
    _editUserProfileBloc.add(new FormSubmitted(UpdateRequest.firstName, new EditUserProfileRequest(AppData.currentUser.id, fName, null, null)));
  }

  _saveLastName(String lName) {
    _editUserProfileBloc.add(new FormSubmitted(UpdateRequest.lastName, new EditUserProfileRequest(AppData.currentUser.id, null, lName, null)));
  }


}