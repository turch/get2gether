import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get2gether/InvitedEvents.dart';
import 'package:get2gether/MyEvents.dart';
import 'package:get2gether/bloc/userEvents/userEventsBloc.dart';
import 'package:get2gether/bloc/userEvents/userevents_event.dart';
import 'package:get2gether/bloc/userEvents/userevents_state.dart';
import 'package:get2gether/models/Event.dart';
import 'package:get2gether/models/requests/UserEventsRequest.dart';
import 'package:get2gether/resources/AppData.dart';
import 'package:get2gether/util/AppColors.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

import '../FriendCell.dart';

class ProfileTab extends StatefulWidget {

  @override
  _ProfileTabState createState() => _ProfileTabState();
}

class _ProfileTabState extends State<ProfileTab>{

  bool paymentSwitch = false;
  UserEventsBloc _userEventsBloc;

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  RefreshController _refreshController = RefreshController(initialRefresh: false);

  void _onRefresh() async {
    _userEventsBloc.add(new FetchUserEvents(new UserEventsRequest(AppData.currentUser.id)));
    _refreshController.refreshCompleted();
  }

  void _onLoading() async{
//    _refreshController.loadComplete();
  }

  Widget build(BuildContext context) {
    return BlocProvider<UserEventsBloc>(
        builder: (BuildContext context) {
          _userEventsBloc = UserEventsBloc();
          _onRefresh();
          return _userEventsBloc;
        },
        child: BlocListener<UserEventsBloc, UserEventsBlocState>(
            listener: (context, state) {
              if(state is UserEventsFetched) {
                _populateUserEvents(state.userEventsResponse.events);
                _populateInvitedEvents(state.userEventsResponse.events);
                _refreshController.loadComplete();
              } else if(state is EventDeleted) {
                _removeEventFromLocalList(state.deletedEventId);
              }

            },
            child:  SmartRefresher(
                enablePullDown: true,
                enablePullUp: true,
                header: WaterDropHeader(),
                controller: _refreshController,
                onRefresh: _onRefresh,
                onLoading: _onLoading,
                child: DefaultTabController(
                    length: 2,
                    child: Scaffold(
                      appBar: AppBar(
                        backgroundColor: AppColors.CYAN,
                        elevation: 0,
                        automaticallyImplyLeading: false,

                        bottom: TabBar(
                            labelColor: AppColors.ORANGE,
                            unselectedLabelColor: Colors.white,
                            indicatorSize: TabBarIndicatorSize.label,
                            indicator: BoxDecoration(
                                borderRadius: BorderRadius.only(
                                    topLeft: Radius.circular(10),
                                    topRight: Radius.circular(10)),
                                color: AppColors.MAIN_BG_GREY),
                            tabs: [
                              Tab(
                                child: Align(
                                  alignment: Alignment.center,
                                  child: Text("My Events", style: TextStyle(fontSize: 16.0, fontFamily: 'Avenir'),),
                                ),
                              ),
                              Tab(
                                child: Align(
                                  alignment: Alignment.center,
                                  child: Text("Invited Events", style: TextStyle(fontSize: 16.0, fontFamily: 'Avenir'),),
                                ),
                              ),
                            ]
                        ),
                      ),
                      body: TabBarView(children: [
                        MyEvents(),
                        InvitedEvents()
                      ]),
                    )
                )
            )
        )
    );
  }

  _removeEventFromLocalList(int eventId) {
    int indexToRemove = -1;
    if(AppData.currentUserEvents != null) {
      for(int i = 0; i < AppData.currentUserEvents.length; i++) {
        var model = AppData.currentUserEvents[i];
        if(model.id == eventId) {
          indexToRemove = i;
          break;
        }
      }
      if(indexToRemove >= 0){
        AppData.currentUserEvents.removeAt(indexToRemove);
      }
    }
  }
  _populateUserEvents(List<Event> events) {
    AppData.currentUserEvents = new List();
    for(int i = 0; i < events.length; i++) {
      var eventModel = events[i];
      if(eventModel.adminId == AppData.currentUser.id) {
        AppData.currentUserEvents.add(eventModel);
      }
    }
  }
  _populateInvitedEvents(List<Event> events) {
    AppData.currentUserInvitedEvents = new List();
    for(int i = 0; i < events.length; i++) {
      var eventModel = events[i];
      if(eventModel.adminId != AppData.currentUser.id) {
        AppData.currentUserInvitedEvents.add(eventModel);
      }
    }
  }
}