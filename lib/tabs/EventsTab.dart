import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get2gether/EventCell.dart';
import 'package:get2gether/AvailabilitySlotCell.dart';
import 'package:get2gether/UIComponents/AppButton.dart';
import 'package:get2gether/bloc/eventsByFilter/EventsByFilterBloc.dart';
import 'package:get2gether/bloc/eventsByFilter/eventsbyfilter_event.dart';
import 'package:get2gether/bloc/eventsByFilter/eventsbyfilter_state.dart';
import 'package:get2gether/bloc/availabilitySlotsByFilter/AvailabilitySlotsByFilterBloc.dart';
import 'package:get2gether/bloc/availabilitySlotsByFilter/availabilityslotsbyfilter_event.dart';
import 'package:get2gether/bloc/availabilitySlotsByFilter/availabilityslotsbyfilter_state.dart';
import 'package:get2gether/bloc/userEvents/userEventsBloc.dart';
import 'package:get2gether/bloc/userEvents/userevents_event.dart';
import 'package:get2gether/bloc/userEvents/userevents_state.dart';
import 'package:get2gether/models/Event.dart';
import 'package:get2gether/models/AvailabilitySlot.dart';
import 'package:get2gether/models/requests/EventsRequestByFilter.dart';
import 'package:get2gether/models/requests/AvailabilitySlotsRequestByFilter.dart';
import 'package:get2gether/util/AppColors.dart';
import 'package:intl/intl.dart';
import 'package:table_calendar/table_calendar.dart';
import 'package:rxdart/rxdart.dart';


class EventsTab extends StatefulWidget {

  @override
  _EventsTabState createState() => _EventsTabState();
}

class _EventsTabState extends State<EventsTab>{
  DateTime _currentDate;
  UserEventsBloc _userEventsBloc;
  EventsByFilterBloc _eventsByFilterBloc;
  AvailabilitySlotsByFilterBloc _availabilitySlotsByFilterBloc;
  CalendarController _calendarController;
  Map<DateTime, List<Event>> _eventsForCalendarIndicators = new Map();
  Map<DateTime, List<AvailabilitySlot>> _availabilitySlotsForCalendarIndicators = new Map();
  Map<DateTime, List<dynamic>> _events = new Map();
  bool isCalenderMonthly = false;

  Observable<List<dynamic>> test;

  @override
  void initState() {
    _userEventsBloc = BlocProvider.of<UserEventsBloc>(context);
    _eventsByFilterBloc = BlocProvider.of<EventsByFilterBloc>(context);
    _availabilitySlotsByFilterBloc = BlocProvider.of<AvailabilitySlotsByFilterBloc>(context);
    _currentDate = new DateTime.now();
    _calendarController = CalendarController();
    super.initState();

    test = Observable.combineLatest2(_eventsByFilterBloc, _availabilitySlotsByFilterBloc, (a, b) => [a, b]);
  }

  @override
  void dispose() {
    _calendarController.dispose();
    super.dispose();
  }

  void _onVisibleDaysChanged(DateTime first, DateTime last, CalendarFormat format) {
    if(format == CalendarFormat.month) {
      _eventsByFilterBloc.add(new FetchEventsByMonth(new EventsRequestByFilter("month", _calendarController.focusedDay.month.toString(), _calendarController.focusedDay.year.toString())));
      _availabilitySlotsByFilterBloc.add(new FetchAvailabilitySlotsByMonth(new AvailabilitySlotsRequestByFilter("month", _calendarController.focusedDay.month.toString(), _calendarController.focusedDay.year.toString())));
      isCalenderMonthly = true;
    } else {
      fetchSpecificDateEventsAndAvailabilitySlots();
    }
  }

  void fetchSpecificDateEventsAndAvailabilitySlots() {
    var formatter = new DateFormat('yyyy-MM-dd');
    var stringDate = formatter.format(_currentDate);
    _eventsByFilterBloc.add(new FetchEventsByDate(new EventsRequestByFilter("date", stringDate, _currentDate.year.toString())));
    _availabilitySlotsByFilterBloc.add(new FetchAvailabilitySlotsByDate(new AvailabilitySlotsRequestByFilter("date", stringDate, _currentDate.year.toString())));
    isCalenderMonthly = false;
  }

  Widget _buildCalendarMarker(DateTime date, dynamic event) {
    if (event is AvailabilitySlot) {
      return Container(
        width: 10,
        height: 10,
        decoration: BoxDecoration(
          shape: BoxShape.circle,
          color: Colors.red,
        ),
      );
    }

    return Container(
      width: 10,
      height: 10,
      decoration: BoxDecoration(
        shape: BoxShape.circle,
        color: _calendarController.isSelected(date) ? Colors.white : _calendarController.isToday(date) ?  AppColors.ORANGE : AppColors.ORANGE,
      ),
    );
  }

  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: AppColors.MAIN_BG_GREY,
        body: MultiBlocProvider(
            providers: [
              BlocProvider<EventsByFilterBloc>(
                  create: (BuildContext context) {
                    var formatter = new DateFormat('yyyy-MM-dd');
                    var stringDate = formatter.format(_currentDate);
                    _eventsByFilterBloc.add(new FetchEventsByDate(new EventsRequestByFilter("date", stringDate, _currentDate.year.toString())));
                    _eventsByFilterBloc.add(new FetchEventsByMonth(new EventsRequestByFilter("month", _currentDate.month.toString(), _currentDate.year.toString())));
                    return _eventsByFilterBloc;
                  }),
              BlocProvider<AvailabilitySlotsByFilterBloc>(
                  create: (BuildContext context) {
                    var formatter = new DateFormat('yyyy-MM-dd');
                    var stringDate = formatter.format(_currentDate);
                    _eventsByFilterBloc.add(new FetchEventsByDate(new EventsRequestByFilter("date", stringDate, _currentDate.year.toString())));
                    _availabilitySlotsByFilterBloc.add(new FetchAvailabilitySlotsByMonth(new AvailabilitySlotsRequestByFilter("month", _currentDate.month.toString(), _currentDate.year.toString())));
                    return _availabilitySlotsByFilterBloc;
                  }),
            ],
            child: MultiBlocListener(
                listeners: [
                  BlocListener<EventsByFilterBloc, EventsByFilterBlocState>(
                      bloc: _eventsByFilterBloc,
                      listener: (context, state) {
                        if (state is EventsFetchedByMonth) {
                          _populateEventsFromResponse(state.userEventsResponse.events);
                        }
                      }),
                  BlocListener<AvailabilitySlotsByFilterBloc, AvailabilitySlotsByFilterBlocState>(
                      bloc: _availabilitySlotsByFilterBloc,
                      listener: (context, state) {
                        if (state is AvailabilitySlotsFetchedByMonth) {
                          _populateAvailabilitySlotsFromResponse(state.userAvailabilitySlotsResponse.availabilitySlots);
                        }
                      }),
                ],
                child: StreamBuilder<List<dynamic>>(
                  stream: test,
                  builder: (context, state) {
                    return Stack(
                      children: <Widget>[
                        Container(
                          padding: EdgeInsets.only(top: 30, bottom: 10),
                          decoration: new BoxDecoration(
                              color: AppColors.CYAN,
                              borderRadius: new BorderRadius.only(
                                  bottomLeft: const Radius.circular(30.0),
                                  bottomRight: const Radius.circular(30.0)
                              )
                          ),
                          child: TableCalendar(
                            startingDayOfWeek: StartingDayOfWeek.monday,
                            calendarController: _calendarController,
                            initialSelectedDay: _currentDate,
                            initialCalendarFormat: CalendarFormat.week,
                            events: _events,
                            availableCalendarFormats: const {
                              CalendarFormat.month: 'Month',
                              CalendarFormat.week: 'Week',
                            },
                            onVisibleDaysChanged: _onVisibleDaysChanged,
                            onDaySelected: (date, events) {
                              _currentDate = date;
                              fetchSpecificDateEventsAndAvailabilitySlots();
                              _calendarController.setCalendarFormat(CalendarFormat.week);
                            },

                            headerStyle: HeaderStyle(
                              titleTextBuilder: (date, locale) => DateFormat.yMMM(locale).format(date),
                              titleTextStyle: TextStyle(fontFamily: 'Lato', color: Colors.white, fontSize: 20.0),
                              formatButtonTextStyle: TextStyle().copyWith(color: Colors.white, fontFamily: 'Lato'),
                              formatButtonDecoration: BoxDecoration(
                                color: AppColors.ORANGE,
                                borderRadius: BorderRadius.circular(16.0),
                              ),
                              leftChevronIcon: Icon(Icons.chevron_left, color: Colors.white),
                              rightChevronIcon: Icon(Icons.chevron_right, color: Colors.white),
                            ),
                            daysOfWeekStyle:  DaysOfWeekStyle(
                                weekdayStyle: TextStyle(color: Colors.white, fontWeight: FontWeight.w600),
                                weekendStyle: TextStyle(color: Colors.white, fontWeight: FontWeight.w600)
                            ),
                            calendarStyle: CalendarStyle(
                              selectedColor: AppColors.ORANGE,
                              todayColor:  AppColors.TEXT_GREY_BLUISH,
                              weekdayStyle: TextStyle(color: Colors.white),
                              weekendStyle: TextStyle(color: Colors.white),
                              outsideStyle: TextStyle(color: Colors.white),
                              outsideWeekendStyle: TextStyle(color: Colors.white),
                              outsideHolidayStyle: TextStyle(color: Colors.white),
                            ),
                            builders: CalendarBuilders(
                              markersBuilder: (context, date, events, holidays) {
                                final children = <Widget>[];
                                final dots = <Widget>[];

                                int i = 0;
                                for (; i < 4 && i < events.length; ++i) {
                                  dots.add(_buildCalendarMarker(date, events[i]));
                                }

                                if (i < events.length) {
                                  dots.add(_buildCalendarMarker(date, events[events.length - 1]));
                                }

                                children.add(
                                  Row(
                                    children: dots,
                                    mainAxisAlignment: MainAxisAlignment.center,
                                  )
                                );

                                return children;
                              },
                            ),
                          ),
                        ),
                        Container(
                            height:  isCalenderMonthly ? 0 : null,
                            margin: EdgeInsets.only(top: 180.0),
                            child: _getChildViewByState(state)
                        )
                      ],
                    );
                  },
                )
            )
        )
    );
  }

  Widget _getChildViewByState(AsyncSnapshot<List<dynamic>> stateTest) {
    if (stateTest.connectionState == ConnectionState.active) {
      return ListView(
          children: <Widget>[
            Text("Events",
              style: TextStyle(color: AppColors.TEXT_BLACK,
                  fontSize: 23.0,
                  fontFamily: 'Avenir'), textAlign: TextAlign.center,),
            _getEventsDayView(stateTest.data[0]),
            SizedBox(height: 20.0,),
            Text("Availability",
              style: TextStyle(color: AppColors.TEXT_BLACK,
                  fontSize: 23.0,
                  fontFamily: 'Avenir'), textAlign: TextAlign.center,),
            _getAvailabilitySlotsDayView(stateTest.data[1])
          ]
      );
    }

    return Container();
  }

  Widget _getEventsDayView(EventsByFilterBlocState stateWithEvents) {
    if(stateWithEvents is EventsFetched) {
      if(stateWithEvents is EventsFetchedByDate) {
        return BlocListener<UserEventsBloc, UserEventsBlocState>(
            bloc: _userEventsBloc,
            listener: (context, state) {
              if (state is EventDeleted) {
                _removeEventFromList(state.deletedEventId, stateWithEvents);
              }
            },
            child: BlocBuilder<UserEventsBloc, UserEventsBlocState>(
                bloc: _userEventsBloc,
                builder: (context, state) {
                  if (stateWithEvents.userEventsResponse.events.length == 0) {
                    return Center(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Text("No event added on selected date."),
                            SizedBox(height: 10.0,),
                            AppButton('Add Event', _createEvent),
                          ],
                        )
                    );
                  } else {
                    var eventsList = List<Widget>();

                    stateWithEvents.userEventsResponse.events.forEach((eventModel) {
                      eventsList.add(
                        EventsCell(
                          eventModel,
                          deleteCallback: () {
                            _userEventsBloc.add(new DeleteEvent(eventModel.id));
                          }
                        )
                      );
                    });

                    return Column(
                        children: eventsList
                    );
                  }
                }
            )
        );
      } else if(stateWithEvents is EventsFetchedByMonth) {
        return Container();
      }
    }
    else if(stateWithEvents is EventsInProgress) {
      return Center(
        child: CircularProgressIndicator(),
      );
    }
    else if(stateWithEvents is EventsError) {
      return Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(stateWithEvents.error),
            ],
          )
      );
    } else {
      return Container();
    }
  }

  Widget _getAvailabilitySlotsDayView(AvailabilitySlotsByFilterBlocState stateWithAvailabilitySlots) {
    if(stateWithAvailabilitySlots is AvailabilitySlotsFetched) {
      if(stateWithAvailabilitySlots is AvailabilitySlotsFetchedByDate) {
        return BlocListener<AvailabilitySlotsByFilterBloc, AvailabilitySlotsByFilterBlocState>(
            bloc: _availabilitySlotsByFilterBloc,
            listener: (context, state) {
              if (state is EventDeleted) {
                //_removeEventFromList(state.deletedEventId, stateWithAvailabilitySlots);
              }
            },
            child: BlocBuilder<AvailabilitySlotsByFilterBloc, AvailabilitySlotsByFilterBlocState>(
                bloc: _availabilitySlotsByFilterBloc,
                builder: (context, state) {
                  var children = List<Widget>();

                  if (stateWithAvailabilitySlots.userAvailabilitySlotsResponse.availabilitySlots.length == 0) {
                    children.add(SizedBox(height: 10.0,));
                    children.add(Text("No availability added on selected date."));
                  }

                  stateWithAvailabilitySlots.userAvailabilitySlotsResponse.availabilitySlots.forEach((availabilitySlotModel) {
                    children.add(
                        AvailabilitySlotCell(
                            availabilitySlotModel,
                            deleteCallback: () {
                              _userEventsBloc.add(new DeleteEvent(availabilitySlotModel.id));
                            }
                        )
                    );
                  });

                  children.add(SizedBox(height: 10.0,));
                  children.add(AppButton('Add Availability', _createAvailability));
                  children.add(SizedBox(height: 40.0,));

                  return Column(
                      children: children
                  );
                }
            )
        );
      } else if(stateWithAvailabilitySlots is EventsFetchedByMonth) {
        return Container();
      }
    }
    else if(stateWithAvailabilitySlots is AvailabilitySlotsInProgress) {
      return Center(
        child: CircularProgressIndicator(),
      );
    }
    else if(stateWithAvailabilitySlots is AvailabilitySlotsError) {
      return Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              //Text(stateWithAvailabilitySlots.error),
            ],
          )
      );
    }

    return Container();
  }

  _createEvent() {
    Navigator.pushNamed(context, '/CreateEvent', arguments: _currentDate);
  }

  _createAvailability() {
    Navigator.pushNamed(context, '/CreateAvailabilitySlot', arguments: _currentDate);
  }

  _populateEventsFromResponse(List<Event> events) {
    _eventsForCalendarIndicators.clear();

    events.forEach((event){
      event.dateTimeList.forEach((dateTime) {
        var date = DateTime.parse(dateTime.date);
        if (_eventsForCalendarIndicators[date] == null) {
          _eventsForCalendarIndicators[date] = new List();
        }

        _eventsForCalendarIndicators[date].add(event);
      });
    });

    _mergeEventsAndAvailabilitySlots();
  }

  _populateAvailabilitySlotsFromResponse(List<AvailabilitySlot> availabilitySlots) {
    _availabilitySlotsForCalendarIndicators.clear();

    availabilitySlots.forEach((slot) {
      DateTime date = DateTime.parse(slot.date);

      if (_availabilitySlotsForCalendarIndicators[date] == null) {
        _availabilitySlotsForCalendarIndicators[date] = List();
      }

      _availabilitySlotsForCalendarIndicators[DateTime.parse(slot.date)].add(slot);
    });

    _mergeEventsAndAvailabilitySlots();
  }

  _mergeEventsAndAvailabilitySlots() {
    _events.clear();

    _availabilitySlotsForCalendarIndicators.forEach((date, slotList) {
      if (!_events.containsKey(date)) {
        _events[date] = new List<dynamic>();
      }

      slotList.forEach((slot) {
        _events[date].add(slot);
      });
    });

    _eventsForCalendarIndicators.forEach((date, eventList) {
      if (!_events.containsKey(date)) {
        _events[date] = new List<dynamic>();
      }

      _events[date].add(eventList[0]);
    });
  }

  _removeEventFromList(int eventId, EventsFetchedByDate stateWithEvents) {
    if(stateWithEvents.userEventsResponse.events != null) {
      List<Event> events = stateWithEvents.userEventsResponse.events;
      int indexToRemove = -1;
      for(int i = 0; i < events.length; i++) {
        Event event = events.elementAt(i);
        if(event.id == eventId) {
          indexToRemove = i;
          break;
        }
      }
      if(indexToRemove >= 0) stateWithEvents.userEventsResponse.events.removeAt(indexToRemove);

    }
  }
}