import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get2gether/util/AppColors.dart';
import 'package:timeago/timeago.dart' as timeago;
import 'models/NotificationModel.dart';

class NotificationCell extends StatefulWidget {

  NotificationModel notification;
  @override
  _NotificationCellState createState() => _NotificationCellState();

  NotificationCell(this.notification);
}

class _NotificationCellState extends State<NotificationCell>{

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  Widget build(BuildContext context) {
    return GestureDetector(
      child: Card(
        color: Colors.white,
        child: Container(
            width: MediaQuery.of(context).size.width,
            margin: EdgeInsets.all(10.0),
            child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  ClipOval(
                      child: CachedNetworkImage(
                        width: 40.0,
                        height: 40.0,
                        imageUrl: widget.notification.profile.image ?? "",
                        placeholder: (context, url) => SizedBox(
                          height: 40.0,
                          width: 40.0,
                          child: CircularProgressIndicator(strokeWidth: 1,),
                        ),
                        errorWidget: (context, url, error) => Icon(Icons.supervised_user_circle, color: AppColors.TEXT_GREY_BLUISH, size: 40.0,),
                      )),
                  Flexible(
                    child: Padding(
                      padding: EdgeInsets.only(left: 10.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Text(_getNotificationTitle(), style: TextStyle(color: AppColors.TEXT_BLACK, fontSize: 16.0, fontFamily: 'Avenir'),),
                          SizedBox(height: 5,),
                          Text(_getNotificationDescription(), style: TextStyle(color: AppColors.TEXT_GREY_BLUISH, fontSize: 14.0, fontFamily: 'Avenir'), maxLines: 2,),
                          SizedBox(height: 2,),
                          Text(getTimeAgo(), style: TextStyle(color: AppColors.TEXT_GREY, fontSize: 14.0, fontFamily: 'Lato'),),
                        ],

                      ),
                    ),
                  )

                ]
            )
        ),

      ),
      onTap: () {
        Navigator.pushNamed(context, '/EventDetail', arguments: widget.notification.event);
      },
    );
  }

  String _getNotificationTitle() {
    if(widget.notification.notificationType == NOTIFICATION_TYPE.event_accepted) {
      return "Invited Accepted";
    } else if(widget.notification.notificationType == NOTIFICATION_TYPE.event_expired) {
      return "Event Expired";
    } else if(widget.notification.notificationType.compareTo(NOTIFICATION_TYPE.event_invitation) == 0) {
      return "New Invite";
    } else if(widget.notification.notificationType == NOTIFICATION_TYPE.event_paid) {
      return "Invite Accepted - Payment Done.";
    } else if(widget.notification.notificationType == NOTIFICATION_TYPE.event_rejected) {
      return "Invited Rejected";
    }
  }

  String _getNotificationDescription() {

    var userProfile = widget.notification.profile;
    var event = widget.notification.event;


    if(widget.notification.notificationType == NOTIFICATION_TYPE.event_accepted.toString()) {
      return "${userProfile.firstName} has accepted your invite against " + event.title +".";
    } else if(widget.notification.notificationType == NOTIFICATION_TYPE.event_expired.toString()) {
      return event.title + " is expried.";
    } else if(widget.notification.notificationType == NOTIFICATION_TYPE.event_invitation.toString()) {
      return "You are invited to a new event '" + event.title +"' by " + userProfile.firstName?? ""+"." ;
    } else if(widget.notification.notificationType == NOTIFICATION_TYPE.event_paid.toString()) {
      return "${userProfile.firstName} has paid against your invite for '" + event.title +"'.";
    } else if(widget.notification.notificationType == NOTIFICATION_TYPE.event_rejected.toString()) {
      return "${userProfile.firstName}  is not interested to join '" + event.title +"'.";
    }
  }
  String getTimeAgo() {
    var date1 = DateTime.parse(widget.notification.createdAt+" Z");
    return timeago.format(date1);
  }
}
