
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get2gether/util/AppColors.dart';
import 'package:get2gether/util/AppDimensions.dart';

class FormTextField extends StatefulWidget{

  Function callback;
  String hint;
  double width;
  String text;
  TextEditingController textController;
  bool obscureText;
  bool autovalidate;
  Function validator;
  TextInputType inputType;
  @override
  FormTextFieldState createState() => new FormTextFieldState();

  FormTextField(this.hint, this.width, {this.text, this.textController, this.obscureText = false, this.autovalidate = false, this.validator, this.inputType = TextInputType.text});
}

class FormTextFieldState extends State<FormTextField> {



  @override
  void initState() {
    super.initState();
    if(widget.text != null && widget.textController != null) widget.textController.text = widget.text;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: AppDimensions.buttonHeight(),
      width: widget.width,
      child: TextFormField(
          keyboardType: widget.inputType,
          style: TextStyle(fontSize: 16.0, color: AppColors.TEXT_GREY_BLUISH),

          decoration: new InputDecoration(
            focusedBorder: OutlineInputBorder(
              borderSide: BorderSide(color: AppColors.TEXT_GREY, width: 1.0),
            ),
            enabledBorder: OutlineInputBorder(
              borderSide: BorderSide(color: Colors.white, width: 1.0),
            ),
            filled: true,
            hintStyle: new TextStyle(color: AppColors.HINT_COLOR),
            hintText: widget.hint,
            fillColor: Colors.white,

          ),
          controller: widget.textController,
          obscureText: widget.obscureText,
          autovalidate: widget.autovalidate,
          validator: widget.validator
      ),
    );
  }
}