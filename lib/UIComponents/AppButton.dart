import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get2gether/util/AppColors.dart';
import 'package:get2gether/util/AppDimensions.dart';

class AppButton extends StatefulWidget {

  var title;
  Color color;
  bool loading;
  double fontSize;
  @override
  State<StatefulWidget> createState() {
    return AppButtonState();
  }
  Function callback;

  AppButton(this.title, this.callback, {this.color, this.loading = false, this.fontSize});


}

class AppButtonState extends State<AppButton> {
  @override
  Widget build(BuildContext context) {

    return Container(
      child: Material(
        borderRadius: const BorderRadius.all(
          const Radius.circular(5.0),
        ),
        child: new InkWell(

            onTap: (){
              widget.callback();
              },
            child: Container(

              height: AppDimensions.buttonHeight(),
              width: AppDimensions.buttonWidth(MediaQuery.of(context).size),
              child: Center(child:
                widget.loading ? CircularProgressIndicator()
                    :Text(widget.title, style: TextStyle(color: Colors.white, fontSize: widget.fontSize == null ? AppDimensions.buttonTextSize : widget.fontSize), textAlign: TextAlign.center)),
            )
        ),
        color: widget.color == null ? AppColors.ORANGE : widget.color,
        elevation: 2,
      ),
      color: Colors.transparent,
    );
  }
}
