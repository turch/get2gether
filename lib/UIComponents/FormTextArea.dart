
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get2gether/util/AppColors.dart';
import 'package:get2gether/util/AppDimensions.dart';

class FormTextArea extends StatefulWidget{

  Function callback;
  String hint;
  double width;
  double height;
  String text;
  TextEditingController textController;

  @override
  FormTextAreaState createState() => new FormTextAreaState();

  FormTextArea(this.hint, this.width, {this.text, this.height, this.textController});
}

class FormTextAreaState extends State<FormTextArea> {


  @override
  void initState() {
    super.initState();
    if(widget.text != null && widget.textController != null) widget.textController.text = widget.text;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: widget.height != null ? widget.height : AppDimensions.buttonHeight()*2,
      width: widget.width,
      child: TextField(
        style: TextStyle(fontSize: 16.0, color: AppColors.TEXT_GREY_BLUISH),
        maxLines: 4,
        decoration: new InputDecoration(
          focusedBorder: OutlineInputBorder(
            borderSide: BorderSide(color: AppColors.TEXT_GREY, width: 1.0),
          ),
          enabledBorder: OutlineInputBorder(
            borderSide: BorderSide(color: Colors.white, width: 1.0),
          ),
          filled: true,
          hintStyle: new TextStyle(color: AppColors.HINT_COLOR),
          hintText: widget.hint,
          fillColor: Colors.white,
        ),
        controller: widget.textController,

      ),
    );
  }
}