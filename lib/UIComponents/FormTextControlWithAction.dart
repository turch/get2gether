
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:get2gether/util/AppColors.dart';
import 'package:get2gether/util/AppDimensions.dart';
import 'package:intl/intl.dart';

class FormTextControlWithAction extends StatefulWidget{

  Function callback;
  String hint;
  double width;
  String text;
  String image;
  bool enabled;
  Function editCallback;
  Function getDateTimeObjectOnSelection;
  var icon;
  ACTION_TYPE action;
  TextEditingController textController;

  @override
  FormTextControlWithActionState createState() => new FormTextControlWithActionState();

  FormTextControlWithAction(this.hint, this.width, this.image, {this.text, this.action, this.textController, this.icon, this.enabled = true, this.editCallback, this.getDateTimeObjectOnSelection});
}

class FormTextControlWithActionState extends State<FormTextControlWithAction> {
  DateTime _selectedTime;

//  String _dateTimeTextToDisplay;

  var focusNode = new FocusNode();
  @override
  void initState() {
    _selectedTime = DateTime.now();
    if(widget.text != null && widget.textController != null) widget.textController.text = widget.text; //_dateTimeTextToDisplay = widget.text;
    focusNode.addListener(() {
      if(focusNode.hasFocus) {
        setState(() {
          widget.icon = Icons.save;
          widget.action = ACTION_TYPE.save;
          widget.enabled = true;
        });
      } else {
        setState(() {
          widget.icon = Icons.edit;
          widget.action = ACTION_TYPE.edit;
          widget.enabled = false;
        });
      }
    });
    super.initState();
  }

  void onTap() {
    switch(widget.action){
      case ACTION_TYPE.date:
        DatePicker.showDatePicker(context,
            showTitleActions: true,
            minTime: DateTime.now(),
            maxTime: DateTime(2023, 12, 12),
            onChanged: (date) {
              if(widget.getDateTimeObjectOnSelection != null) widget.getDateTimeObjectOnSelection(date);
              _selectedTime = date;
            },
            onConfirm: (date) {
              print('confirm $date');
              setState(() {
                var formatter = new DateFormat('yyyy-MM-dd');
                var a = formatter.format(date);
                widget.textController.text = a;
              });

            },
            currentTime: DateTime.now(), locale: LocaleType.en);
        FocusScope.of(context).requestFocus(new FocusNode());
        break;
      case ACTION_TYPE.time:
        showModalBottomSheet(
            context: context,
            builder: (context) {
              return Container(
                height: 200,
                child:  CupertinoDatePicker(
                  onDateTimeChanged: (DateTime date) {
                    if(widget.getDateTimeObjectOnSelection != null) widget.getDateTimeObjectOnSelection(date);
                    _selectedTime = date;
                    setState(() {
                      widget.textController.text = DateFormat('hh:mm a').format(date);
                    });
                  },
                  initialDateTime: _selectedTime,
                  mode: CupertinoDatePickerMode.time,
                  use24hFormat: false,
                ),
              );
            });
        FocusScope.of(context).requestFocus(new FocusNode());
        break;
      case ACTION_TYPE.edit:
        FocusScope.of(context).requestFocus(focusNode);
        break;
      case ACTION_TYPE.save:
        FocusScope.of(context).requestFocus(new FocusNode());
        if(widget.editCallback != null) {
          widget.editCallback(widget.textController.text);
        }
        setState(() {
          widget.icon = Icons.edit;
          widget.action = ACTION_TYPE.edit;
          widget.enabled = false;
        });
        break;
      case ACTION_TYPE.date_time:
        showModalBottomSheet(
            context: context,
            builder: (context) {
              return Container(
                height: 200,
                child:  CupertinoDatePicker(
                  onDateTimeChanged: (DateTime date) {
                    if(widget.getDateTimeObjectOnSelection != null) widget.getDateTimeObjectOnSelection(date);
                    _selectedTime = date;
                    setState(() {
                      var formatter = new DateFormat('yyyy-MM-dd h:mm a');
                      var tempTime = new DateFormat('yyyy-MM-dd h:mm');
                      tempTime.format(date);
                      print(tempTime);
                      widget.textController.text = formatter.format(date);
                    });
                  },
                  initialDateTime: _selectedTime,
                  mode: CupertinoDatePickerMode.dateAndTime,
                  use24hFormat: false,
                ),
              );
            });
        FocusScope.of(context).requestFocus(new FocusNode());
        break;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        height: AppDimensions.buttonHeight(),
        width: widget.width,
        child: new Stack(
            children: <Widget>[
              TextField(
                focusNode: focusNode,
                onTap: onTap,
                enabled: true,
                style: TextStyle(fontSize: 16.0, color: AppColors.TEXT_GREY_BLUISH),
                decoration: new InputDecoration(
                  contentPadding: EdgeInsets.fromLTRB(15.0, 15.0, 60.0, 15.0),
                  focusedBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: AppColors.TEXT_GREY, width: 1.0),
                  ),
                  enabledBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.white, width: 1.0),
                  ),
                  filled: true,
                  hintStyle: new TextStyle(color: AppColors.HINT_COLOR),
                  hintText: widget.hint,
                  fillColor: Colors.white,

                ),
                controller: widget.textController,
              ),
              Align(
                  alignment: Alignment.centerRight,
                  child: GestureDetector(
                    child: new Padding(
                        padding: EdgeInsets.only(right: 20.0),
                        child : widget.image == null ? Icon(
                          widget.icon ?? Icons.lock,
                          color: AppColors.TEXT_GREY,
                          size: 20.0,
                        ): Image.asset(widget.image, width: 18, height: 18,),
                    ),
                    onTap: onTap,
                  )
              )
            ]
        )
    );
  }
}

enum ACTION_TYPE {
  date,
  time,
  date_time,
  edit,
  save
}