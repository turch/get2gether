import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get2gether/util/AppColors.dart';
import 'package:get2gether/util/AppDimensions.dart';
import 'package:get2gether/util/Strings.dart';

class OrangeBorderTransparentButton extends StatefulWidget{

  Function callback;
  String text;
  Color colorText;
  bool loading;

  @override
  State<StatefulWidget> createState() {
    return OrangeBorderTransparentButtonState();
  }

  OrangeBorderTransparentButton(this.text, this.callback, {this.colorText = Colors.white, this.loading = false});


}

class OrangeBorderTransparentButtonState extends State<OrangeBorderTransparentButton> {
  @override
  Widget build(BuildContext context) {

    return Container(
      child: Material(

        child: new InkWell(
            onTap: (){ widget.callback();},
            child: Container(
              height: AppDimensions.buttonHeight(),
              width: AppDimensions.buttonWidth(MediaQuery.of(context).size),
              child: Center(
                child: widget.loading ? CircularProgressIndicator(): Text(widget.text, style: TextStyle(color: widget.colorText, fontSize: AppDimensions.buttonTextSize), textAlign: TextAlign.center,),),
              decoration: BoxDecoration(
                border: Border.all(color: AppColors.ORANGE),
                borderRadius: const BorderRadius.all(
                  const Radius.circular(5.0),
                ),
              ),
            )
        ),
        color: Colors.transparent,
        elevation: 0,
      ),
      color: Colors.transparent,
    );
  }
}
