import 'package:contacts_service/contacts_service.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:get2gether/models/Profile.dart';
import 'package:get2gether/resources/AppData.dart';
import 'package:get2gether/tabs/EventsTab.dart';
import 'package:get2gether/tabs/ProfileTab.dart';
import 'package:get2gether/tabs/SettingsTab.dart';
import 'package:get2gether/util/AppColors.dart';
import 'package:get2gether/util/AppDimensions.dart';
import 'package:get2gether/util/SharedPrefHelper.dart';
import 'package:permission_handler/permission_handler.dart';

import 'BottomNavBarWidget.dart';
import 'bloc/notifications/NotificationsBloc.dart';
import 'bloc/notifications/notifications_event.dart';
import 'models/FCMNotificationModel.dart';
import 'models/requests/NotificationsRequest.dart';
import 'tabs/NotificationsTab.dart';

class _Page {
  _Page({this.widget});
  final StatefulWidget widget;
}

class Home extends StatefulWidget {
  Home({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> with SingleTickerProviderStateMixin {
  var _currentIndex = 0;
  NotificationsBloc _notificationsBloc;
  List<_Page> _allPages;

  @override
  void initState() {
    final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();
    SharedPreferencesHelper.getValueByKey(SharedPreferenceKeys.NOTIFICATION).then((onValue){
      AppData.notificationSwitch = onValue;
    });
    _firebaseMessaging.requestNotificationPermissions();
    _firebaseMessaging.configure(
      onMessage: (Map<String, dynamic> message) async {
        print('on resume $message');
        FCMNotificationModel notification = _getNotificationModel(message);
        if(AppData.notificationSwitch) {
          showDialog(
            context: context,
            builder: (context) =>
                AlertDialog(
                  content: ListTile(
                    title: Text(_getNotificationTitle(notification)),
                    subtitle: Text(_getNotificationDescription(notification)),
                  ),
                  actions: <Widget>[
                    FlatButton(
                        child: Text('Ok'),
                        onPressed: () {
                          Navigator.of(context).pop();
                          _refreshAndNavigateToNotifications();
                        }
                    ),
                  ],
                ),
          );
        }
      },
      onResume: (Map<String, dynamic> message) async {
        print('on resume $message');
      },
      onLaunch: (Map<String, dynamic> message) async {
        print('on launch $message');
        myBackgroundMessageHandler(message);
      },);

    SharedPreferencesHelper.read(SharedPreferenceKeys.USER).then((map) {
      AppData.currentUser = Profile.fromJson(map);
      setState((){});
    });

    _notificationsBloc = NotificationsBloc();
    _allPages = <_Page>[
      _Page(widget: EventsTab()),
      _Page(widget: ProfileTab()),
      _Page(widget: NotificationsTab(_notificationsBloc)),
      _Page(widget: SettingsTab()
      ),
    ];
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  Widget build(BuildContext context) {
    Future.delayed(const Duration(milliseconds: 500), () {
      isPermissionGranted(context);
    });

    return Scaffold(
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.pushNamed(context, '/CreateEvent');
        },
        child: Icon(Icons.add, size: 30.0, color: Colors.white,),
        backgroundColor: AppColors.CYAN,
      ),
      body: AppData.currentUser == null ? Center(
        child: CircularProgressIndicator(),
      ) : IndexedStack(
        index: _currentIndex,
        children: _allPages.map<Widget>((_Page page) {
          return Container(
              key: ObjectKey(page.widget),
              child: page.widget);

        }).toList(),
      ),
      bottomNavigationBar: BottomAppBar(
          shape: null,

          color: Colors.transparent,
          child: Container(
              height: AppDimensions.bottomNavBarHeight,
              padding: EdgeInsets.symmetric(horizontal: 30.0),

              decoration: new BoxDecoration(
                  color: Colors.white,
                  borderRadius: new BorderRadius.only(
                      topLeft: const Radius.circular(30.0),
                      topRight: const Radius.circular(30.0))),
              child: BottomNavBarWidget(_tabChanged, selectedTab: _currentIndex,)
          )
      ),
    );
  }

  _tabChanged(int index) {
    setState(() {
      _currentIndex = index;
    });
  }

  _refreshAndNavigateToNotifications() {
    _tabChanged(2); //taking user to notifications tab
    _notificationsBloc.add(
        new FetchNotifications(new NotificationsRequest(20, 1)));
  }

  Future<dynamic> myBackgroundMessageHandler(Map<String, dynamic> message) {
    if (message.containsKey('data')) {
      final dynamic data = message['data'];
      FCMNotificationModel notification = _getNotificationModel(message);
      _tabChanged(2); //taking user to notifications tab
    }

    if (message.containsKey('notification')) {
      // Handle notification message
      final dynamic notification = message['notification'];
    }

    // Or do other work.
  }
  Future<void> isPermissionGranted(BuildContext context) async {
    var permission = await Permission.contacts.request();
    if (permission.isGranted) {
      ContactsService.getContacts().then((value) {
        AppData.contacts = value;
      });
    } else if (permission.isDenied) {
      DisplayAlertDialog(context, "Error","To Grant Contacts Permission in future goto Settings", (){ openAppSettings();});
    }
  }


  FCMNotificationModel _getNotificationModel(var message) {
    var jsonModel = message['data'];
    if(jsonModel == null)
      jsonModel = message;
    var map = new Map<String, dynamic>.from(jsonModel);
    var notification = FCMNotificationModel.fromJson(map);
    print(notification.toString());
    return notification;
  }



  String _getNotificationTitle(FCMNotificationModel notification) {
    if(notification.notificationType == NOTIFICATION_TYPE.event_accepted) {
      return "Invite Accepted";
    } else if(notification.notificationType == NOTIFICATION_TYPE.event_expired) {
      return "Event Expired";
    } else if(notification.notificationType.compareTo(NOTIFICATION_TYPE.event_invitation) == 0) {
      return "New Invite";
    } else if(notification.notificationType == NOTIFICATION_TYPE.event_paid) {
      return "Invite Accepted - Payment Done.";
    } else if(notification.notificationType == NOTIFICATION_TYPE.event_rejected) {
      return "Invited Rejected";
    }
  }

  String _getNotificationDescription(FCMNotificationModel notification) {

    var userProfile = notification.profile;
    var event = notification.event;


    if(notification.notificationType == NOTIFICATION_TYPE.event_accepted.toString()) {
      return "${userProfile.firstName} has accepted your invite against " + event.title +".";
    } else if(notification.notificationType == NOTIFICATION_TYPE.event_expired.toString()) {
      return event.title + " is expried.";
    } else if(notification.notificationType == NOTIFICATION_TYPE.event_invitation.toString()) {
      return "You are invited to a new event '" + event.title +"' by " + userProfile.firstName?? ""+"." ;
    } else if(notification.notificationType == NOTIFICATION_TYPE.event_paid.toString()) {
      return "${userProfile.firstName} has paid against your invite for '" + event.title +"'.";
    } else if(notification.notificationType == NOTIFICATION_TYPE.event_rejected.toString()) {
      return "${userProfile.firstName} is not interested to join '" + event.title +"'.";
    }
  }


}
class PlaceholderWidget extends StatefulWidget{
  final String text;

  PlaceholderWidget(this.text);


  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return null;
  }
}

void DisplayAlertDialog(BuildContext context, String title,String message, Function openSettingsCallback){
  showDialog(
    context: context,
    builder: (BuildContext context) {
      return AlertDialog(
        title: Text(title),
        content: Text(message),
        actions: [
          FlatButton(
            child: Text("Open Settings"),
            onPressed: () {openSettingsCallback();},
          ),
          FlatButton(
            child: Text("OK"),
            onPressed: () {Navigator.pop(context);},
          ),
        ],
      );
    },
  );
}

