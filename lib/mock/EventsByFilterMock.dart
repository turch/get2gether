import 'dart:convert';
import 'package:intl/intl.dart';
import 'package:dio/dio.dart';
import 'package:mockito/mockito.dart';

class DioAdapterMock extends Mock implements HttpClientAdapter {}

class EventsByFilterMock {
  String _type;
  DioAdapterMock dioAdapterMock;

  EventsByFilterMock(this._type) {
    final data = jsonEncode({
      'status': 1,
      'events': _type == 'date' ? dateData : monthData
    });

    final httpResponse = ResponseBody.fromString(
      data,
      200,
      headers: {
        Headers.contentTypeHeader: [Headers.jsonContentType],
      },
    );

    dioAdapterMock = DioAdapterMock();

    when(dioAdapterMock.fetch(any, any, any))
        .thenAnswer((_) async => httpResponse);
  }

  static final String date0 = DateFormat('yyyy-MM-dd').format(DateTime.now());
  static final String date1 = DateFormat('yyyy-MM-dd').format(DateTime.now().add(new Duration(days: 1)));
  static final String date2 = DateFormat('yyyy-MM-dd').format(DateTime.now().add(new Duration(days: 2)));

  static final dateData = [{
    'title': 'test',
    'short_description': 'wat',
    'date': date0,
    'time': '06:00:00',
    'address': "Denver or Something",
    'created_at': '2020-05-21T09:08:18',
    'admin_id': 119,
    'invitations': [],
    'acceptances': [],
    'deadline': '2020-05-25T09:08:18',
    'notification_time': '2020-05-25T09:08:18',
  },{
    'title': 'test',
    'short_description': 'wat',
    'date': date0,
    'time': '08:00:00',
    'address': "Denver or Something",
    'created_at': '2020-05-21T09:08:18',
    'invitations': [],
    'acceptances': [],
    'deadline': '2020-05-25T09:08:18',
    'notification_time': '2020-05-25T09:08:18',
  }];

  static final monthData = [{
    'title': 'test',
    'short_description': 'wat',
    'date': date0,
    'time': '06:00:00',
    'address': "Denver or Something",
    'created_at': '2020-05-21T09:08:18',
    'admin_id': 119,
    'invitations': [],
    'acceptances': [],
    'deadline': '2020-05-25T09:08:18',
    'notification_time': '2020-05-25T09:08:18',
  },{
    'title': 'test',
    'short_description': 'wat',
    'date': date1,
    'time': '07:00:00',
    'address': "Denver or Something",
    'created_at': '2020-05-21T09:08:18',
    'admin_id': 119,
    'invitations': [],
    'acceptances': [],
    'deadline': '2020-05-25T09:08:18',
    'notification_time': '2020-05-25T09:08:18',
  },{
    'title': 'test',
    'short_description': 'wat',
    'date': date2,
    'time': '06:00:00',
    'address': "Denver or Something",
    'created_at': '2020-05-21T09:08:18',
    'invitations': [],
    'acceptances': [],
    'deadline': '2020-05-25T09:08:18',
    'notification_time': '2020-05-25T09:08:18',
  },{
    'title': 'test',
    'short_description': 'wat',
    'date': date2,
    'time': '08:00:00',
    'address': "Denver or Something",
    'created_at': '2020-05-21T09:08:18',
    'invitations': [],
    'acceptances': [],
    'deadline': '2020-05-25T09:08:18',
    'notification_time': '2020-05-25T09:08:18',
  }];

}