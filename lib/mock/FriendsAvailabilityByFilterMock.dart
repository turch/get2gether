import 'dart:convert';
import 'package:intl/intl.dart';
import 'package:dio/dio.dart';
import 'package:mockito/mockito.dart';

class DioAdapterMock extends Mock implements HttpClientAdapter {}

class FriendsAvailabilityByFilterMock {
  String _type;
  int _friendId;
  DioAdapterMock dioAdapterMock;

  FriendsAvailabilityByFilterMock(this._type, this._friendId) {
    var dateCopy = List.from(dateData);
    var monthCopy = List.from(monthData);

    if(_friendId != null){
      dateCopy.removeAt(1);
      monthCopy.removeAt(4);
    }

    final data = jsonEncode({
      'status': 1,
      'availability_slots': _type == 'date' ? dateCopy : monthCopy,
    });

    final httpResponse = ResponseBody.fromString(
      data,
      200,
      headers: {
        Headers.contentTypeHeader: [Headers.jsonContentType],
      },
    );

    dioAdapterMock = DioAdapterMock();

    when(dioAdapterMock.fetch(any, any, any))
        .thenAnswer((test) async {
          return httpResponse;
        });
  }

  static final String date0 = DateFormat('yyyy-MM-dd').format(DateTime.now());
  static final String date1 = DateFormat('yyyy-MM-dd').format(DateTime.now().add(new Duration(days: 1)));
  static final String date2 = DateFormat('yyyy-MM-dd').format(DateTime.now().add(new Duration(days: 2)));

  static final dateData = [{
    'date': date0,
    'start_time': '02:00:00',
    'end_time': '04:00:00',
    'created_at': '2020-05-21T09:08:18',
    'owner': 'Nathan',

  },{
    'date': date0,
    'start_time': '05:00:00',
    'end_time': '06:00:00',
    'created_at': '2020-05-21T09:08:18',
    'owner': 'Joe',
  },{
    'date': date0,
    'start_time': '07:00:00',
    'end_time': '08:00:00',
    'created_at': '2020-05-21T09:08:18',
    'owner': 'Nathan',
  }];

  static final monthData = [{
    'date': date0,
    'start_time': '02:00:00',
    'end_time': '08:00:00',
    'created_at': '2020-05-21T09:08:18',
    'owner': 'Nathan',
  },{
    'date': date0,
    'start_time': '02:00:00',
    'end_time': '03:00:00',
    'created_at': '2020-05-21T09:08:18',
    'owner': 'Nathan',
  },{
    'date': date0,
    'start_time': '03:00:00',
    'end_time': '04:00:00',
    'created_at': '2020-05-21T09:08:18',
    'owner': 'Nathan',
  },{
    'date': date1,
    'start_time': '04:00:00',
    'end_time': '05:00:00',
    'created_at': '2020-05-21T09:08:18',
    'owner': 'Nathan',
  },{
    'date': date1,
    'start_time': '05:00:00',
    'end_time': '06:00:00',
    'created_at': '2020-05-21T09:08:18',
    'owner': 'Nathan',
  },{
    'date': date2,
    'start_time': '06:00:00',
    'end_time': '08:00:00',
    'created_at': '2020-05-21T09:08:18',
    'owner': 'Nathan',
  },{
    'date': date2,
    'start_time': '06:00:00',
    'end_time': '08:00:00',
    'created_at': '2020-05-21T09:08:18',
    'owner': 'Nathan',
  }];
}