import 'dart:convert';
import 'package:dio/dio.dart';
import 'package:mockito/mockito.dart';

class DioAdapterMock extends Mock implements HttpClientAdapter {}

class UserEventsMock {
  DioAdapterMock dioAdapterMock;

  UserEventsMock() {
    final data = jsonEncode({
      'status': 1,
      'events': monthData
    });

    final httpResponse = ResponseBody.fromString(
      data,
      200,
      headers: {
        Headers.contentTypeHeader: [Headers.jsonContentType],
      },
    );

    dioAdapterMock = DioAdapterMock();

    when(dioAdapterMock.fetch(any, any, any))
        .thenAnswer((_) async => httpResponse);
  }

  static final monthData = [{
    'title': 'test',
    'short_description': 'wat',
    'date': '2020-05-22',
    'time': '06:00:00',
    'address': "Denver or Something",
    'created_at': '2020-05-21T09:08:18',
    'admin_id': 119,
    'invitations': [{
      'first_name': 'John',
      'last_name': 'Smith',
      'email': 'john.smith@example.com',
      'phone': '(123) 4567890)',
    },{
      'name': 'Sally Something',
      'phone': '(123) 4567890)',
      'email': 'john.smith@example.com',
    }],
    'acceptances': [{
      'name': 'Bob Smith',
      'email': 'bob.smith@example.com',
      'date_time': {
        'date': '2020-05-22',
        'time': '06:00:00',
      },
    },{
      'name': 'Sally Something',
      'phone': '(123) 4567890)',
      'country_code': '+1',
      'date_time': {
        'date': '2020-05-22',
        'time': '06:00:00',
      },
    }],
    'deadline': '2020-05-25T09:08:18',
    'notification_time': '2020-05-25T09:08:18',
  },{
    'title': 'test',
    'short_description': 'wat',
    'date': '2020-05-22',
    'time': '07:00:00',
    'address': "Denver or Something",
    'created_at': '2020-05-21T09:08:18',
    'admin_id': 119,
    'invitations': [],
    'acceptances': [],
    'deadline': '2020-05-25T09:08:18',
    'notification_time': '2020-05-25T09:08:18',
  },{
    'title': 'test',
    'short_description': 'wat',
    'date': '2020-05-24',
    'time': '06:00:00',
    'address': "Denver or Something",
    'created_at': '2020-05-21T09:08:18',
    'invitations': [],
    'acceptances': [],
    'deadline': '2020-05-25T09:08:18',
    'notification_time': '2020-05-25T09:08:18',
  },{
    'title': 'test',
    'short_description': 'wat',
    'date': '2020-05-24',
    'time': '08:00:00',
    'address': "Denver or Something",
    'created_at': '2020-05-21T09:08:18',
    'invitations': [],
    'acceptances': [],
    'deadline': '2020-05-25T09:08:18',
    'notification_time': '2020-05-25T09:08:18',
  }];

}