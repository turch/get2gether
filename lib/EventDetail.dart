import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get2gether/EventCell.dart';
import 'package:get2gether/bloc/eventDetail/eventDetailBloc.dart';
import 'package:get2gether/bloc/eventDetail/eventDetail_state.dart';
import 'package:get2gether/bloc/eventDetail/eventDetail_event.dart';
import 'package:get2gether/models/Event.dart';
import 'package:get2gether/util/AppColors.dart';

class EventDetail extends StatefulWidget {
  EventDetail();

  @override
  _EventDetailState createState() => _EventDetailState();
}

class _EventDetailState extends State<EventDetail> {
  EventDetailBloc _eventDetailBloc;

  @override
  void initState() {
    _eventDetailBloc = new EventDetailBloc();
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  Widget build(BuildContext context) {
    Event event = ModalRoute.of(context).settings.arguments;
    _eventDetailBloc.add(new EventDetailEvent(event.id.toString()));
    return Scaffold(
        backgroundColor: AppColors.MAIN_BG_GREY,
        appBar: AppBar(
            elevation: 1.0,
            backgroundColor: AppColors.CYAN,
            centerTitle: true,
            title: Text(
              "Event Detail",
              style: TextStyle(color: Colors.white),
            ),
            leading: IconButton(
              icon: Icon(
                Icons.arrow_back,
                color: Colors.white,
              ),
              onPressed: () => Navigator.pop(context, false),
            )),
        body: BlocBuilder(
          bloc: _eventDetailBloc,
          builder: (BuildContext context, EventDetailBlocState state) {
            if (state is PrevModelDisplayed || state is NewModelFetching)  {
              return getEventDetailCell(event);
            }
            if (state is NewModelFetched) {
              return getEventDetailCell(state.eventDetailModel.event);
            } else {
              return Container();
            }
          },
        ));
  }

  Widget getEventDetailCell(Event event) {
    return SingleChildScrollView(
      child: EventsCell(event),
    );
  }
}
