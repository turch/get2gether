import 'package:contacts_service/contacts_service.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_google_places/flutter_google_places.dart';
import 'package:flutter_conditional_rendering/flutter_conditional_rendering.dart';
import 'package:get2gether/models/DateTimePair.dart';
import 'package:tuple/tuple.dart';
import 'package:get2gether/util/EventTypes.dart';
import 'package:get2gether/EventCell.dart';
import 'package:get2gether/SelectContacts.dart';
import 'package:get2gether/UIComponents/AppButton.dart';
import 'package:get2gether/UIComponents/FormTextField.dart';
import 'package:get2gether/bloc/createEvent/ceateEventBloc.dart';
import 'package:get2gether/bloc/createEvent/createevent_event.dart';
import 'package:get2gether/models/requests/EditEventRequest.dart';
import 'package:get2gether/bloc/eventsByFilter/EventsByFilterBloc.dart';
import 'package:get2gether/resources/AppData.dart';
import 'package:get2gether/util/AppColors.dart';
import 'package:get2gether/util/Constants.dart';
import 'package:get2gether/util/GeneralUtil.dart';
import 'package:google_maps_webservice/places.dart';
import 'package:intl/intl.dart';

import 'UIComponents/FormTextArea.dart';
import 'UIComponents/FormTextControlWithAction.dart';
import 'bloc/createEvent/createevent_state.dart';
import 'models/Event.dart';
import 'models/Invitation.dart';
import 'models/requests/CreateEventRequest.dart';


class CreateEvent extends StatefulWidget {

  @override
  _CreateEventState createState() => _CreateEventState();
}

class _CreateEventState extends State<CreateEvent>{

  DateTime _currentDate;
  DateTime eventNotification, eventDeadline;
  GoogleMapsPlaces _places = GoogleMapsPlaces(apiKey: ThirdParties.GoogleAPIKey);
  bool paymentSwitch = false;
  CreateEventBloc _createEventBloc;
  EventsByFilterBloc _eventsByFilterBloc;
  TextEditingController _titleController = TextEditingController();
  TextEditingController _descriptionController = TextEditingController();
  List<TextEditingController> _dateController = [TextEditingController()];
  List<TextEditingController> _timeController = [TextEditingController()];
  TextEditingController _rsvpController = TextEditingController();
  TextEditingController _notificationController = TextEditingController();
  TextEditingController _placesController = TextEditingController();
  TextEditingController _friendsController = TextEditingController();
  TextEditingController _amountController = TextEditingController();
  TextEditingController _paymentEmailController = TextEditingController();
  String _eventType;
  double _latitude;
  double _longitude;
  List<Contact> _selectedContacts;
  Event _eventModel;
  DateTime _selectedDate;

  List<Tuple2<DateTime, DateTime>> _dateTimeList = [Tuple2<DateTime, DateTime>(null, null)];

  @override
  void initState() {
    super.initState();
    _eventsByFilterBloc = BlocProvider.of<EventsByFilterBloc>(context);
  }

  @override
  void dispose() {
    super.dispose();
  }

  Future<bool> _onWillPop() async {
    if(_eventModel != null) {
      Navigator.pop(context, _eventModel);
    } else {
      Navigator.pop(context, false);
    }
    return false;
  }

  Widget build(BuildContext context) {
    final arguments = ModalRoute.of(context).settings.arguments;

    if (arguments is DateTime) {
      _selectedDate = arguments;
      _dateTimeList[0] = Tuple2<DateTime, DateTime>(arguments, _dateTimeList[0].item2);
    }

    if(_eventModel == null) {
      if (arguments is Event)
        _eventModel = ModalRoute.of(context).settings.arguments;
    }
    if(_eventModel != null) {
      if(_eventModel.paymentRequired == 1) paymentSwitch = true;
      _eventType = _eventModel.eventType;
      _latitude = _eventModel.latitude;
      _longitude = _eventModel.longitude;
      eventDeadline = DateTime.parse(_eventModel.deadline);
      eventNotification = DateTime.parse(_eventModel.notificationTime);

      _dateTimeList.clear();
      _eventModel.dateTimeList.forEach((dateTime) {
        _dateTimeList.add(Tuple2<DateTime, DateTime>(DateTime.parse(dateTime.date), DateTime.parse('1996-07-20 ' + dateTime.time)));
        _dateController.add(TextEditingController());
        _timeController.add(TextEditingController());
      });

      var a = 0;
    }

    return new WillPopScope(
        onWillPop: _onWillPop,
        child: BlocProvider<CreateEventBloc>(
            create: (BuildContext context) {
              _createEventBloc = CreateEventBloc();
              return _createEventBloc;
            },
            child: BlocListener<CreateEventBloc, CreateEventBlocState>(
                listener: (context, state) {
                  if (state is CreateEventSuccess) {
                    Navigator.pop(context, false);
                  } else if(state is EditEventSuccess) {
                    Navigator.pop(context, state.editEventResponse.event);
                  }
                },
                child: BlocBuilder<CreateEventBloc, CreateEventBlocState>(
                    bloc: _createEventBloc,
                    builder: (context, state) {
                      return Scaffold(
                          backgroundColor: AppColors.MAIN_BG_GREY,
                          appBar: AppBar(
                              elevation: 1.0,
                              backgroundColor: AppColors.CYAN,
                              centerTitle: true,
                              title: Text(
                                _eventModel != null ? "Update Event" : "Create Event", style: TextStyle(color: Colors.white),),
                              leading: IconButton(
                                icon: Icon(Icons.arrow_back, color: Colors.white,),
                                onPressed: _onWillPop,
                              )
                          ),
                          body: SingleChildScrollView(
                              padding: EdgeInsets.all(20.0),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[

                                  //Section1
                                  Text("Add title and short description for your Event",
                                    style: TextStyle(color: AppColors.TEXT_BLACK,
                                        fontSize: 23.0,
                                        fontFamily: 'Avenir'), textAlign: TextAlign.center,),
                                  SizedBox(height: 15.0,),
                                  Text("Add event title", style: TextStyle(
                                      color: AppColors.TEXT_GREY_BLUISH,
                                      fontSize: 18.0,
                                      fontFamily: 'Avenir'),),
                                  SizedBox(height: 10.0,),
                                  FormTextField("Type meeting title here…", MediaQuery
                                      .of(context)
                                      .size
                                      .width, textController: _titleController, text: _eventModel != null? _eventModel.title : "",),
                                  SizedBox(height: 15.0,),
                                  Text("Add short description", style: TextStyle(
                                      color: AppColors.TEXT_GREY_BLUISH,
                                      fontSize: 18.0,
                                      fontFamily: 'Avenir'),),
                                  SizedBox(height: 10.0,),
                                  FormTextArea("Type meeting description here…", MediaQuery
                                      .of(context)
                                      .size
                                      .width, textController: _descriptionController, text: _eventModel != null? _eventModel.shortDescription : ""),
                                  SizedBox(height: 15.0,),

                                  //Section2
                                  Text("Choose time and date for your Event",
                                    style: TextStyle(color: AppColors.TEXT_BLACK,
                                        fontSize: 23.0,
                                        fontFamily: 'Avenir'),textAlign: TextAlign.center),
                                  SizedBox(height: 15.0,),
                                  AppButton("View Availability", _friendsAvailability, color: AppColors.CYAN, loading: state is CreateEventInProgress,),
                                  SizedBox(height: 20.0,),
                                  Text("Choose event type", style: TextStyle(
                                      color: AppColors.TEXT_GREY_BLUISH,
                                      fontSize: 18.0,
                                      fontFamily: 'Avenir'),),
                                  SizedBox(height: 10.0,),
                                  Container(
                                    color: Colors.white,
                                    padding: EdgeInsets.only(left: 15.0, right: 15.0),
                                    child: DropdownButton<String>(
                                      isExpanded: true,
                                      value: _eventType,
                                      icon: Icon(Icons.arrow_downward),
                                      underline: Container(height: 0),
                                      hint: Text("Pick Event Type", style: TextStyle(fontSize: 16.0, color: AppColors.TEXT_GREY_BLUISH)),
                                      iconSize: 24,
                                      elevation: 16,
                                      style: TextStyle(fontSize: 16.0, color: AppColors.TEXT_GREY_BLUISH),
                                      onChanged: (String newValue) {
                                        setState(() {
                                          _eventType = newValue;
                                        });
                                      },
                                      items: <String>[EventTypes.singleDayEvent, EventTypes.multiDayEvent, EventTypes.flexibleEvent].map<DropdownMenuItem<String>>((String value) {
                                        return DropdownMenuItem<String>(
                                          value: value,
                                          child: Text(value),
                                        );
                                      }).toList(),
                                    ),
                                  ),
                                  SizedBox(height: 20.0,),
                                  buildMultiDateInput(),
                                  Text("Deadline (RSVP)", style: TextStyle(
                                      color: AppColors.TEXT_GREY_BLUISH,
                                      fontSize: 18.0,
                                      fontFamily: 'Avenir'),),
                                  SizedBox(height: 10.0,),
                                  FormTextControlWithAction("", MediaQuery
                                      .of(context)
                                      .size
                                      .width, 'assets/images/clock.png',
                                    text: _eventModel != null? getEventDeadline() : "Pick Time",
                                    action: ACTION_TYPE.date_time, textController: _rsvpController, enabled: false, getDateTimeObjectOnSelection: (dateTime){
                                      eventDeadline = dateTime;
                                    },),
                                  SizedBox(height: 10.0,),
                                  Text("Choose notification time", style: TextStyle(
                                      color: AppColors.TEXT_GREY_BLUISH,
                                      fontSize: 18.0,
                                      fontFamily: 'Avenir'),),
                                  SizedBox(height: 10.0,),
                                  FormTextControlWithAction("", MediaQuery
                                      .of(context)
                                      .size
                                      .width, 'assets/images/notification.png',
                                    text: _eventModel != null? getEventNotificationTime() : "Add Notification",
                                    action: ACTION_TYPE.date_time, textController: _notificationController, enabled: false, getDateTimeObjectOnSelection: (dateTime){
                                      eventNotification = dateTime;
                                    },),
                                  SizedBox(height: 15.0,),

                                  //Section3
                                  Text(
                                    "Invite friends and find a great place for your Event",
                                    style: TextStyle(color: AppColors.TEXT_BLACK,
                                        fontSize: 23.0,
                                        fontFamily: 'Avenir'),textAlign: TextAlign.center),
                                  SizedBox(height: 15.0,),
                                  Text("Invite people", style: TextStyle(
                                      color: AppColors.TEXT_GREY_BLUISH,
                                      fontSize: 18.0,
                                      fontFamily: 'Avenir'),),
                                  SizedBox(height: 10.0,),


                                  GestureDetector(
                                    child: Container(
                                        color: Colors.transparent,
                                        child: IgnorePointer(
                                          child:FormTextControlWithAction(
                                            "Select Friends to Invite", MediaQuery
                                              .of(context)
                                              .size
                                              .width, 'assets/images/add_friend.png',
                                            textController: _friendsController,
                                            text:  _eventModel != null ? mapSelectedContacts() : "Add Contact",),
                                        )
                                    ),
                                    onTap: () async {
                                      var result = await Navigator.of(context).push(
                                          MaterialPageRoute(builder: (context) => new SelectContacts(_selectedContacts, event: _eventModel,))
                                      );
                                      if(result is Event) {
                                        _eventModel = result;
                                        setState(() {
                                          _friendsController.text = mapSelectedContacts();
                                        });
                                      }
                                      else {
                                        _selectedContacts = result;
                                        setState(() {
                                          _friendsController.text = "${_selectedContacts.length} Contacts";
                                        });
                                      }

                                    },
                                  ),


                                  SizedBox(height: 10.0,),
                                  Text("Find a place", style: TextStyle(
                                      color: AppColors.TEXT_GREY_BLUISH,
                                      fontSize: 18.0,
                                      fontFamily: 'Avenir'),),
                                  SizedBox(height: 10.0,),
                                  GestureDetector(
                                    child: Container(
                                      color: Colors.transparent,
                                      child: IgnorePointer(
                                        child: FormTextControlWithAction("Search your place…", MediaQuery
                                            .of(context)
                                            .size
                                            .width, 'assets/images/google.png', textController: _placesController,
                                          text: _eventModel != null ? _eventModel.address : "",),
                                      ),
                                    ),
                                    onTap: _onPlacesTextChanged,
                                  ),

                                  SizedBox(height: 10.0,),
                                  Text("Make a Payment", style: TextStyle(
                                      color: AppColors.TEXT_GREY_BLUISH,
                                      fontSize: 18.0,
                                      fontFamily: 'Avenir'),),
                                  SizedBox(height: 10.0,),
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: <Widget>[
                                      Image.asset(
                                        'assets/images/paypal.png', fit: BoxFit.fill,),
                                      Switch(value: paymentSwitch, onChanged: (newVal) {
                                        setState(() {
                                          paymentSwitch = newVal;
                                        });
                                      })
                                    ],
                                  ),
                                  paymentSwitch ? Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: <Widget>[

                                      Text("Add per head participation amount",
                                        style: TextStyle(color: AppColors.TEXT_GREY_BLUISH,
                                            fontSize: 18.0,
                                            fontFamily: 'Avenir'),),
                                      SizedBox(height: 10.0,),
                                      FormTextField("Mention amount here…", MediaQuery
                                          .of(context)
                                          .size
                                          .width, inputType: TextInputType.number, textController: _amountController,
                                        text: _eventModel != null ? _eventModel.perHeadContribution.toString() : "",),
                                      SizedBox(height: 15.0,),
                                      Text("Mention your PayPal email address",
                                        style: TextStyle(color: AppColors.TEXT_GREY_BLUISH,
                                            fontSize: 18.0,
                                            fontFamily: 'Avenir'),),
                                      SizedBox(height: 10.0,),
                                      FormTextField("Mention email...", MediaQuery
                                          .of(context)
                                          .size
                                          .width, inputType: TextInputType.emailAddress, textController: _paymentEmailController,
                                        text: _eventModel != null ? _eventModel.paymentEmail.toString() : AppData.currentUser.email,),
                                      SizedBox(height: 15.0,),
                                    ],
                                  ) : Container(),

                                  state is CreateEventError ? Column(
                                      children: <Widget>[
                                        SizedBox(height: 20.0,),
                                        Center(child:
                                        Text(state.error, style: TextStyle(
                                            color: Colors.red,
                                            fontFamily: 'Avenir'
                                        )
                                        )

                                        )
                                      ])
                                      : Container(),
                                  AppButton(_eventModel != null? "UPDATE EVENT" : "CREATE EVENT", _eventModel != null? _eventUpdatePressed : _eventCreatePressed, color: AppColors.CYAN, loading: state is CreateEventInProgress,),


                                ],
                              )
                          )
                      );
                    }
                )
            )
        )
    );
  }

  Widget buildMultiDateInput() {
    return _eventType == null ? Container() :
      Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(_eventType != EventTypes.singleDayEvent ? "Add event dates" : "Pick event date", style: TextStyle(
              color: AppColors.TEXT_GREY_BLUISH,
              fontSize: 18.0,
              fontFamily: 'Avenir'),),
          SizedBox(height: 10.0,),
          Builder(
            builder: (context) {
              var children = List<Widget>();

              int totalLength = _eventType != EventTypes.singleDayEvent ? _dateTimeList.length : 1;

              for(int i = 0; i < totalLength; ++i) {
                children.addAll( <Widget>[
                  _eventType != EventTypes.singleDayEvent ? Container(
                    width: MediaQuery.of(context).size.width,
                    height: 26.0,
                    child: Stack(
                      children: [
                        Center(
                          child: Text("Date " + (i + 1).toString(), style: TextStyle(
                            color: AppColors.TEXT_GREY_BLUISH,
                            fontSize: 16.0,
                            fontFamily: 'Avenir'),)),
                        Positioned(
                            right: 20.0,
                            top: 4.0,
                            child: GestureDetector(
                              child: Image.asset("assets/images/delete.png", height: 18.0, width: 18.0,),
                              onTap: (){
                                _deleteDate(i);
                              },
                            )

                        )
                      ]),
                  )
                  : Container(),
                  SizedBox(height: 10.0,),
                  FormTextControlWithAction("", MediaQuery
                      .of(context)
                      .size
                      .width, 'assets/images/calendar.png',
                    text: _dateTimeList[i].item1 != null
                        ?  _dateTimeList[i].item1.year.toString() + "-" + _dateTimeList[i].item1.month.toString() + "-" + _dateTimeList[i].item1.day.toString()
                        : (
                          _selectedDate != null
                          ? _selectedDate.year.toString() + "-" + _selectedDate.month.toString() + "-" + _selectedDate.day.toString() : "Pick Date"
                        ),
                    action: ACTION_TYPE.date, textController: _dateController[i], enabled: false, getDateTimeObjectOnSelection: (dateTime) {
                        _dateTimeList[i] = Tuple2(dateTime, _dateTimeList[i].item2);
                      }),
                  SizedBox(height: 10.0,),
                  FormTextControlWithAction("", MediaQuery
                      .of(context)
                      .size
                      .width, 'assets/images/clock.png',
                      text: _dateTimeList[i].item2 != null? getEventTime(i) : "Pick Time",
                      action: ACTION_TYPE.time, textController: _timeController[i], enabled: false, getDateTimeObjectOnSelection: (dateTime) {
                        _dateTimeList[i] = Tuple2(_dateTimeList[i].item1, dateTime);
                      }),
                    SizedBox(height: 10.0,),
                ]);
              };

              return Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: children,
              );
            }
          ),
          SizedBox(height: 10.0,),
          _eventType != EventTypes.singleDayEvent ? AppButton("Add Date", _addDate, color: AppColors.CYAN) : Container(),
          _eventType != EventTypes.singleDayEvent ? SizedBox(height: 20.0,) : Container(),
        ],
      );
  }

  String getEventNotificationTime(){
    var formatter = DateFormat('yyyy-MM-dd hh:mm a');
    return formatter.format(eventNotification);
  }

  String getEventTime(int index) {
    var formatter = DateFormat('hh:mm a');
    return formatter.format(_dateTimeList[index].item2);
  }

  String getEventDeadline(){
    var formatter = DateFormat('yyyy-MM-dd hh:mm a');
    return formatter.format(eventDeadline);
  }

  List<DateTimePair> getEventDateTimeList() {
    var ret = List<DateTimePair>();

    int dateListLength = _eventType == EventTypes.singleDayEvent ? 1 : _dateTimeList.length;

    for (int i = 0; i < dateListLength; ++i) {
      ret.add(DateTimePair(DateFormat('yyyy-MM-dd').format(_dateTimeList[i].item1), DateFormat("HH:mm").format(_dateTimeList[i].item2)));
    }

    return ret;
  }

  Future<Null> displayPrediction(Prediction p) async {
    if (p != null) {
      PlacesDetailsResponse detail = await _places.getDetailsByPlaceId(p.placeId);

      var placeId = p.placeId;
      _latitude = detail.result.geometry.location.lat;
      _longitude = detail.result.geometry.location.lng;
      setState(() {
        _placesController.text = detail.result.formattedAddress;
      });
    }
  }

  void _eventUpdatePressed() {
    FocusScope.of(context).requestFocus(FocusNode());

    var formatter = DateFormat('yyyy-MM-dd HH:mm');
    String notificationDateTime =  formatter.format(eventNotification);
    String rsvpDateTime =  formatter.format(eventDeadline);


    var requestObj = new EditEventRequest(_eventModel.id, _titleController.text, _descriptionController.text,
        getEventDateTimeList(), _placesController.text, rsvpDateTime, notificationDateTime,
        int.parse(_amountController.text.isEmpty ? "-1" :_amountController.text), AppData.currentUser.id, paymentSwitch ? 1 : 0, _latitude,
        _longitude, _paymentEmailController.text);

    var eventObj = new EditEventFormSubmitted(requestObj);
    _createEventBloc.add(eventObj);
  }

  void _friendsAvailability() {
    Navigator.pushNamed(context, '/FriendsAvailability');
  }

  void _addDate() {
    setState(() {
      _dateTimeList.add(Tuple2<DateTime, DateTime>(null, null));
      _timeController.add(TextEditingController());
      _dateController.add(TextEditingController());
    });
  }

  void _deleteDate(int index) {
    setState(() {
      _dateTimeList.removeAt(index);
      _timeController.removeAt(index);
      _dateController.removeAt(index);
    });
  }

  void _eventCreatePressed() {
    if (_eventType == EventTypes.singleDayEvent) {
      if (_dateTimeList[0].item1 == null || _dateTimeList[0].item2 == null) {
        showErrorMessage("Fields Missing", "Please pick event date and time");
        return;
      }
    } else {
      for (int i = 0; i < _dateTimeList.length; ++i) {
        if (_dateTimeList[i].item1 == null || _dateTimeList[i].item2 == null) {
          showErrorMessage("Fields Missing", "Please fill in all event dates and times");
          return;
        }
      }
    }

    var formatter = DateFormat('yyyy-MM-dd HH:mm');
    if(eventNotification == null) {
      showErrorMessage("Fields Missing", "Please pick Notification Date and Time");
      return;
    }

    String notificationDateTime =  formatter.format(eventNotification);

    if(eventDeadline == null) {
      showErrorMessage("Fields Missing", "Please pick event deadline");
      return;
    }
    String rsvpDateTime =  formatter.format(eventDeadline);
    FocusScope.of(context).requestFocus(FocusNode());
    var requestObj = new CreateEventRequest(_titleController.text, _descriptionController.text, _eventType,
        getEventDateTimeList(), _placesController.text, rsvpDateTime, notificationDateTime,
        int.parse(_amountController.text.isEmpty ? "-1" :_amountController.text), AppData.currentUser.id, paymentSwitch ? 1 : 0, _latitude,
        _longitude, _paymentEmailController.text, getInvites());

    var eventObj = new CreateEventFormSubmitted(requestObj);
    _createEventBloc.add(eventObj);
  }

  showErrorMessage(String title, String error) {
    showDialog(
      context: context,
      builder: (context) =>
          AlertDialog(
            content: ListTile(
              title: Text(title),
              subtitle: Text(error),
            ),
            actions: <Widget>[
              FlatButton(
                  child: Text('Ok'),
                  onPressed: () {
                    Navigator.of(context).pop();
                  }
              ),
            ],
          ),
    );
  }

  List<Invitation> getInvites() {
    List<Invitation> invitations = new List();
    if(_selectedContacts != null) {
      for (int i = 0; i < _selectedContacts.length; i++) {
        Contact contact = _selectedContacts[i];
        var type;
        var invite;

        if(contact.phones != null && contact.phones.single != null) {
          type = INVITATION_TYPE.Phone;
          List<String> contactTokens = GeneralUtil.getCountryCode(context, contact.phones.single.value);
          invite = new Invitation(type, contact.displayName, null, contactTokens[1], contactTokens[0]);
          contact.phones.single.value = contactTokens[0] + contactTokens[1];
        }
        else if(contact.emails != null && contact.emails.single != null) {
          type = INVITATION_TYPE.Email;
          invite = new Invitation(type, contact.displayName, contact.emails.single.value, null, null);
        }
        if(invite != null)
          invitations.add(invite);
      }
    }
    return invitations;
  }

  void _onPlacesTextChanged() async{
    FocusScope.of(context).requestFocus(FocusNode());
    Prediction p = await PlacesAutocomplete.show(
        context: context,
        apiKey: ThirdParties.GoogleAPIKey,
        mode: Mode.overlay, // Mode.fullscreen
        language: "en",
        components: [new Component(Component.country, "us")]);
    displayPrediction(p);
  }

  String mapSelectedContacts() {
    _selectedContacts = new List();
    if(_eventModel != null && _eventModel.invitations != null && _eventModel.invitations.length > 0) {
      for(int i = 0; i < _eventModel.invitations.length ; i++) {
        var invitationModel = _eventModel.invitations[i];
        String name = "";
        name = invitationModel.firstName != null ? invitationModel.firstName : "";
        name += invitationModel.lastName != null ? invitationModel.lastName : "";
        Iterable<Item> emails = [new Item(label: "email", value: invitationModel.email ?? "")];
        Iterable<Item> phones = null;
        if(invitationModel.countryCode != null && invitationModel.phone != null)
          phones = [new Item(label: "phone", value: invitationModel.countryCode+invitationModel.phone+"")];
        _selectedContacts.add(new Contact(displayName: name, phones: phones, emails: emails, suffix: invitationModel.id.toString()));
      }
      return "${_eventModel.invitations.length} Contacts";
    }
    return "";
  }

}