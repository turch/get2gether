import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get2gether/util/AppColors.dart';

import 'FriendCell.dart';
import 'models/Event.dart';

class AllInvitedFriends extends StatefulWidget {

  @override
  _AllInvitedFriendsState createState() => _AllInvitedFriendsState();
}

class _AllInvitedFriendsState extends State<AllInvitedFriends>{

  DateTime _currentDate;
  bool paymentSwitch = false;

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  Widget build(BuildContext context) {
    final Event event = ModalRoute.of(context).settings.arguments;

    return Scaffold(
        backgroundColor: AppColors.MAIN_BG_GREY,
        appBar: AppBar(
            elevation: 1.0,
            backgroundColor: AppColors.CYAN,
            centerTitle: true,
            title: Text("Invited Friends", style: TextStyle(color: Colors.white),),
            leading: IconButton(icon:Icon(Icons.arrow_back, color: Colors.white,),
              onPressed:() => Navigator.pop(context, false),
            )
        ),
        body: ListView.builder(
            padding: EdgeInsets.all(20.0),
            itemCount: event.invitations.length,
            itemBuilder: (BuildContext ctxt, int index) {
              var invitation = event.invitations[index];
              return FriendCell(invitation);
            }
        ),

    );


  }
}