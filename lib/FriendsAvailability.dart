import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:contacts_service/contacts_service.dart';
import 'package:tuple/tuple.dart';
import 'package:get2gether/AvailabilitySlotCell.dart';
import 'package:get2gether/bloc/friendsAvailabilityByFilter/FriendsAvailabilityByFilterBloc.dart';
import 'package:get2gether/bloc/friendsAvailabilityByFilter/friendsavailabilitybyfilter_event.dart';
import 'package:get2gether/bloc/friendsAvailabilityByFilter/friendsavailabilitybyfilter_state.dart';
import 'package:get2gether/models/AvailabilitySlot.dart';
import 'package:get2gether/models/requests/FriendsAvailabilityByFilterRequest.dart';
import 'package:get2gether/util/AppColors.dart';
import 'package:intl/intl.dart';
import 'package:table_calendar/table_calendar.dart';


class FriendsAvailability extends StatefulWidget {

  @override
  _FriendsAvailabilityState createState() => _FriendsAvailabilityState();
}

class _FriendsAvailabilityState extends State<FriendsAvailability>{
  List<String> _friends;
  DateTime _currentDate;
  FriendsAvailabilityByFilterBloc _friendsAvailabilityBloc;
  CalendarController _calendarController;
  Map<DateTime, List<AvailabilitySlot>> _availabilitySlotsForCalendarIndicators = new Map();
  Map<DateTime, List<dynamic>> _events = new Map();
  bool isCalenderMonthly = true;

  @override
  void initState() {
    _friendsAvailabilityBloc = new FriendsAvailabilityByFilterBloc();
    _currentDate = new DateTime.now();
    _calendarController = CalendarController();
    super.initState();
  }

  @override
  void dispose() {
    _calendarController.dispose();
    super.dispose();
  }

  void _onVisibleDaysChanged(DateTime first, DateTime last, CalendarFormat format) {
    if(format == CalendarFormat.month) {
      _friendsAvailabilityBloc.add(new FetchFriendsAvailabilityByMonth(new FriendsAvailabilityByFilterRequest("month", _calendarController.focusedDay.month.toString(), _calendarController.focusedDay.year.toString(), _friends)));
      isCalenderMonthly = true;
    } else {
      _fethAvailabilitySlotsForDay();
    }
  }

  Future<bool> _onWillPop() async {
    Navigator.pop(context, false);
    return false;
  }

  void _fethAvailabilitySlotsForDay() {
    var formatter = new DateFormat('yyyy-MM-dd');
    var stringDate = formatter.format(_currentDate);
    _friendsAvailabilityBloc.add(new FetchFriendsAvailabilityByDate(new FriendsAvailabilityByFilterRequest("date", stringDate, _currentDate.year.toString(), _friends)));
    isCalenderMonthly = false;
  }

  Widget _buildCalendarMarker(DateTime date, dynamic event) {
    if (event is AvailabilitySlot) {
      return Container(
        width: 10,
        height: 10,
        decoration: BoxDecoration(
          shape: BoxShape.circle,
          color: Colors.red,
        ),
      );
    }

    return Container(
      width: 10,
      height: 10,
      decoration: BoxDecoration(
        shape: BoxShape.circle,
        color: _calendarController.isSelected(date) ? Colors.white : _calendarController.isToday(date) ?  AppColors.ORANGE : AppColors.ORANGE,
      ),
    );
  }

  Widget build(BuildContext context) {
    final arguments = ModalRoute.of(context).settings.arguments;

    if (arguments is String) {
      _friends = [arguments];
    } else {
      _friends = List();
      ContactsService.getContacts().then((value) {
        value.forEach((contact) {
          _friends.add((contact.givenName != null ? contact.givenName : '') + ' ' + (contact.familyName != null ? contact.familyName : ''));
        });
        _friendsAvailabilityBloc.add(new FetchFriendsAvailabilityByMonth(new FriendsAvailabilityByFilterRequest("month", _currentDate.month.toString(), _currentDate.year.toString(), _friends)));
      });
    }

    return new WillPopScope(
        onWillPop: _onWillPop,
        child: Scaffold(
        backgroundColor: AppColors.MAIN_BG_GREY,
        appBar: AppBar(
            elevation: 1.0,
            backgroundColor: AppColors.CYAN,
            centerTitle: true,
            title: Text(
              "Friend's Availability", style: TextStyle(color: Colors.white),),
            leading: IconButton(
              icon: Icon(Icons.arrow_back, color: Colors.white,),
              onPressed: _onWillPop,
            )
        ),
        body: BlocProvider<FriendsAvailabilityByFilterBloc>(
            create: (BuildContext context) {
              return _friendsAvailabilityBloc;
            },
            child: BlocListener<FriendsAvailabilityByFilterBloc, FriendsAvailabilityByFilterBlocState>(
                bloc: _friendsAvailabilityBloc,
                listener: (context, state) {
                  if (state is FriendsAvailabilityFetchedByMonth) {
                      _populateAvailabilitySlotsFromResponse(state.friendsAvailabilityResponse.availabilitySlots);
                  }
                },
                child: BlocBuilder<FriendsAvailabilityByFilterBloc, FriendsAvailabilityByFilterBlocState>(
                  bloc: _friendsAvailabilityBloc,
                  builder: (context, state) {
                    return Stack(
                      children: <Widget>[
                        Container(
                          padding: EdgeInsets.only(top: 30, bottom: 10),
                          decoration: new BoxDecoration(
                              color: AppColors.CYAN,
                              borderRadius: new BorderRadius.only(
                                  bottomLeft: const Radius.circular(30.0),
                                  bottomRight: const Radius.circular(30.0)
                              )
                          ),
                          child: TableCalendar(
                            startingDayOfWeek: StartingDayOfWeek.monday,
                            calendarController: _calendarController,
                            initialSelectedDay: _currentDate,
                            initialCalendarFormat: CalendarFormat.month,
                            events: _availabilitySlotsForCalendarIndicators,
                            availableCalendarFormats: const {
                              CalendarFormat.month: 'Month',
                              CalendarFormat.week: 'Week',
                            },
                            onVisibleDaysChanged: _onVisibleDaysChanged,
                            onDaySelected: (date, events) {
                              _currentDate = date;
                              _calendarController.setCalendarFormat(CalendarFormat.week);
                              _fethAvailabilitySlotsForDay();
                            },

                            headerStyle: HeaderStyle(
                              titleTextBuilder: (date, locale) => DateFormat.yMMM(locale).format(date),
                              titleTextStyle: TextStyle(fontFamily: 'Lato', color: Colors.white, fontSize: 20.0),
                              formatButtonTextStyle: TextStyle().copyWith(color: Colors.white, fontFamily: 'Lato'),
                              formatButtonDecoration: BoxDecoration(
                                color: AppColors.ORANGE,
                                borderRadius: BorderRadius.circular(16.0),
                              ),
                              leftChevronIcon: Icon(Icons.chevron_left, color: Colors.white),
                              rightChevronIcon: Icon(Icons.chevron_right, color: Colors.white),
                            ),
                            daysOfWeekStyle:  DaysOfWeekStyle(
                                weekdayStyle: TextStyle(color: Colors.white, fontWeight: FontWeight.w600),
                                weekendStyle: TextStyle(color: Colors.white, fontWeight: FontWeight.w600)
                            ),
                            calendarStyle: CalendarStyle(
                              selectedColor: AppColors.ORANGE,
                              todayColor:  AppColors.TEXT_GREY_BLUISH,
                              weekdayStyle: TextStyle(color: Colors.white),
                              weekendStyle: TextStyle(color: Colors.white),
                              outsideStyle: TextStyle(color: Colors.white),
                              outsideWeekendStyle: TextStyle(color: Colors.white),
                              outsideHolidayStyle: TextStyle(color: Colors.white),
                            ),
                            builders: CalendarBuilders(
                              markersBuilder: (context, date, events, holidays) {
                                final children = <Widget>[];
                                final dots = <Widget>[];

                                events.forEach((element) {
                                  dots.add(_buildCalendarMarker(date, element));
                                });

                                children.add(
                                    Row(
                                      children: dots,
                                      mainAxisAlignment: MainAxisAlignment.center,
                                    )
                                );

                                return children;
                              },
                            ),
                          ),
                        ),
                        Container(
                            height:  isCalenderMonthly ? 0 : null,
                            margin: EdgeInsets.only(top: 170.0),
                            child: _getChildViewByState(state)
                        )
                      ],
                    );

                  },
                )
            )
        )
      )
    );

  }

  Widget _getChildViewByState(FriendsAvailabilityByFilterBlocState stateWithAvailabilitySlots) {
    if(stateWithAvailabilitySlots is FriendsAvailabilityFetched) {
      return BlocListener<FriendsAvailabilityByFilterBloc, FriendsAvailabilityByFilterBlocState>(
          bloc: _friendsAvailabilityBloc,
          listener: (context, state) {
            /*if (state is EventDeleted) {
              //_removeEventFromList(state.deletedEventId, stateWithAvailabilitySlots);
            }*/
          },
          child: BlocBuilder<FriendsAvailabilityByFilterBloc, FriendsAvailabilityByFilterBlocState>(
              bloc: _friendsAvailabilityBloc,
              builder: (context, state) {
                var children = List<Widget>();

                if (stateWithAvailabilitySlots.friendsAvailabilityResponse.availabilitySlots.length == 0) {
                  children.add(SizedBox(height: 10.0,));
                  children.add(
                    Center(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          SizedBox(height: 10.0,),
                          Text("No friends available on selected date."),
                        ],
                      )
                    )
                  );
                }

                stateWithAvailabilitySlots.friendsAvailabilityResponse.availabilitySlots.forEach((availabilitySlotModel) {
                  children.add(
                      AvailabilitySlotCell(
                          availabilitySlotModel,
                          deleteCallback: () {

                          }
                      )
                  );
                });

                children.add(SizedBox(height: 40.0,));

                return ListView(
                    children: children
                );
              }
          )
      );
    }
    else if(stateWithAvailabilitySlots is FriendsAvailabilityInProgress) {
      return Center(
        child: CircularProgressIndicator(),
      );
    }
    else if(stateWithAvailabilitySlots is FriendsAvailabilityError) {
      return Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              //Text(stateWithAvailabilitySlots.error),
            ],
          )
      );
    }

    return Container();
  }

  _populateAvailabilitySlotsFromResponse(List<AvailabilitySlot> availabilitySlots) {
    _availabilitySlotsForCalendarIndicators.clear();

    availabilitySlots.forEach((slot) {
      DateTime date = DateTime.parse(slot.date);

      if (_availabilitySlotsForCalendarIndicators[date] == null) {
        _availabilitySlotsForCalendarIndicators[date] = List();
      }

      _availabilitySlotsForCalendarIndicators[DateTime.parse(slot.date)].add(slot);
    });
  }
}