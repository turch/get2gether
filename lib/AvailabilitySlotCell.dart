import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get2gether/UIComponents/AppButton.dart';
import 'package:get2gether/UIComponents/OrangeBorderTransparentButton.dart';
import 'package:get2gether/bloc/updateInviteStatus/updateinvite_event.dart';
import 'package:get2gether/bloc/updateInviteStatus/updateinvite_state.dart';
import 'package:get2gether/models/requests/UpdateInviteRequest.dart';
import 'package:get2gether/resources/AppData.dart';
import 'package:get2gether/util/AppColors.dart';
import 'package:get2gether/util/AppDimensions.dart';
import 'package:get2gether/util/Constants.dart';
import 'package:intl/intl.dart';
import 'package:timeago/timeago.dart' as timeago;

import 'models/AvailabilitySlot.dart';

class AvailabilitySlotCell extends StatefulWidget {
  AvailabilitySlot availabilitySlotModel;
  Function deleteCallback;
  @override
  _AvailabilitySlotCellState createState() => _AvailabilitySlotCellState();

  AvailabilitySlotCell(this.availabilitySlotModel, {this.deleteCallback});
}

class _AvailabilitySlotCellState extends State<AvailabilitySlotCell>{

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        Card(
            color: Colors.white,
            margin: EdgeInsets.only(top: 15.0, left: 15.0, right: 15.0),
            child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  widget.availabilitySlotModel.owner != null ?
                  Padding(
                    padding: EdgeInsets.all(15.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(widget.availabilitySlotModel.owner, style: TextStyle(color: AppColors.TEXT_GREY_BLUISH, fontSize: 23.0, fontFamily: 'Avenir', )),
                      ],
                    ),
                  ) : Container(),
                  Padding(
                    padding: EdgeInsets.all(15.0),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Text(widget.availabilitySlotModel.startTime + ' - ' + widget.availabilitySlotModel.endTime, style: TextStyle(color: AppColors.TEXT_GREY_BLUISH, fontSize: 23.0, fontFamily: 'Avenir', )),
                      ],
                    ),
                  ),
                  widget.availabilitySlotModel.eventId != null ?
                  Padding(
                    padding: EdgeInsets.all(15.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        AppButton('Accept This Time and Date', _acceptAvailabilitySlot)
                      ],
                    ),
                  ) : Container(),
                ]
            )
        ),
        widget.availabilitySlotModel.owner == null && widget.availabilitySlotModel.eventId == null ?
        Positioned(
            right: 30.0,
            top: 35.0,
            child: GestureDetector(
              child: Image.asset("assets/images/menu.png", height: 18.0, width: 18.0,),
              onTap: (){
                _showOptionsBottomSheet(context);
              },
            )
        ): Container()
      ],
    );
  }

  void _showOptionsBottomSheet(context){
    showModalBottomSheet(
        context: context,
        builder: (BuildContext bc){
          return Container(
            child: new Wrap(
              children: <Widget>[
                new ListTile(
                  leading: Image.asset("assets/images/edit.png", height: 20.0, width: 20.0, fit: BoxFit.fill,),
                  title: new Text('Edit'),
                  onTap: () async {
                    Navigator.pop(context);
                    Navigator.pushNamed(context, '/CreateAvailabilitySlot', arguments: widget.availabilitySlotModel);
                  },
                ),
                new ListTile(
                    leading: Image.asset("assets/images/delete.png", height: 20.0, width: 20.0, fit: BoxFit.fill,),
                    title: new Text('Delete'),
                    onTap: () {/*
                      Navigator.pop(context);
                      if(isAcceptedByAnyUser())
                        _showActionBlockedDialog("Delete");
                      else
                      if(widget.deleteCallback != null) widget.deleteCallback();*/
                    }
                ),

              ],
            ),
          );
        }
    );
  }

  void _acceptAvailabilitySlot() {

  }
}