import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get2gether/UIComponents/AppButton.dart';
import 'package:get2gether/UIComponents/OrangeBorderTransparentButton.dart';
import 'package:get2gether/bloc/updateInviteStatus/updateInviteBloc.dart';
import 'package:get2gether/bloc/updateInviteStatus/updateinvite_event.dart';
import 'package:get2gether/bloc/updateInviteStatus/updateinvite_state.dart';
import 'package:get2gether/models/requests/UpdateInviteRequest.dart';
import 'package:get2gether/resources/AppData.dart';
import 'package:get2gether/util/AppColors.dart';
import 'package:get2gether/util/AppDimensions.dart';
import 'package:get2gether/util/Constants.dart';
import 'package:get2gether/models/Event.dart';
import 'package:intl/intl.dart';
import 'package:timeago/timeago.dart' as timeago;

import 'models/AvailabilitySlot.dart';

class EventTimeSlotCell extends StatefulWidget {
  String _eventTime;
  String _eventDate;
  Event _event;
  @override
  _EventTimeSlotCellState createState() => _EventTimeSlotCellState(_eventTime, _eventDate, _event);

  EventTimeSlotCell(String eventTime, eventDate, Event event) : super(key: ValueKey(eventTime)) {
    _eventTime = eventTime;
    _eventDate = eventDate;
    _event = event;
  }
}

class _EventTimeSlotCellState extends State<EventTimeSlotCell>{
  String _eventTime;
  String _eventDate;
  Event _event;
  UpdateInviteBloc _updateInviteBloc;

  _EventTimeSlotCellState(this._eventTime, this._eventDate, this._event);

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  Widget build(BuildContext context) {
    _updateInviteBloc = UpdateInviteBloc();

    return Stack(
      children: <Widget>[
        Card(
            color: Colors.white,
            margin: EdgeInsets.only(top: 15.0, left: 15.0, right: 15.0),
            child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.all(15.0),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Text(_eventTime, style: TextStyle(color: AppColors.TEXT_GREY_BLUISH, fontSize: 23.0, fontFamily: 'Avenir', )),
                      ],
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.all(15.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        AppButton('Accept This Time and Date', _acceptAvailabilitySlot)
                      ],
                    ),
                  )
                ]
            )
        ),
      ],
    );
  }

  void _acceptAvailabilitySlot() {
    _updateInviteBloc.add(new AcceptInvite(new UpdateInviteRequest(
        _event.id, AppData.currentUser.id, "accepted", _eventDate, _eventTime)));
  }
}